EESchema Schematic File Version 2
LIBS:CMCentralSensorSystem-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:CNY70
LIBS:Display162A
LIBS:rfm_12
LIBS:switch-misc
LIBS:philips-3
LIBS:battery
LIBS:batclipholder
LIBS:NXP_By_element14_Batch_1
LIBS:SparkFun-Sensors
LIBS:74xx-eu
LIBS:ftdichip
LIBS:ftdichip-4
LIBS:dp_devices
LIBS:EMA-406A
LIBS:EA_DOG_Dispalys
LIBS:display_ea_dogm132
LIBS:display_ea_dogm128
LIBS:display_ea_dog-m
LIBS:DOGS102
LIBS:XC3868
LIBS:z180
LIBS:_con-usb
LIBS:_con-usb_1
LIBS:_RFID
LIBS:OVC3860_XS3868
LIBS:LM2576
LIBS:LibB80C1500
LIBS:trafo
LIBS:usb_con-update
LIBS:u_mcu_atmel_atsam4e16c_tfbga-100
LIBS:u_mcu_atmel_atsame70n21_bga-100
LIBS:u_mcu_atmel_atsame70q21_lfbga144
LIBS:u_mcu_lpc2194-01_lqfp64
LIBS:lt1932
LIBS:pir
LIBS:bis00001
LIBS:CMCentralSensorSystem-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title "Central Sensor System -> Sensor Board"
Date "2017-04-30"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	11150 500  7100 500 
$EndSCHEMATC
