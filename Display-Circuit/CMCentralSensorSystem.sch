EESchema Schematic File Version 2
LIBS:CMCentralSensorSystem-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:CNY70
LIBS:Display162A
LIBS:rfm_12
LIBS:switch-misc
LIBS:philips-3
LIBS:battery
LIBS:batclipholder
LIBS:NXP_By_element14_Batch_1
LIBS:SparkFun-Sensors
LIBS:74xx-eu
LIBS:ftdichip
LIBS:ftdichip-4
LIBS:dp_devices
LIBS:EMA-406A
LIBS:EA_DOG_Dispalys
LIBS:display_ea_dogm132
LIBS:display_ea_dogm128
LIBS:display_ea_dog-m
LIBS:DOGS102
LIBS:XC3868
LIBS:z180
LIBS:_con-usb
LIBS:_con-usb_1
LIBS:_RFID
LIBS:OVC3860_XS3868
LIBS:LM2576
LIBS:LibB80C1500
LIBS:trafo
LIBS:usb_con-update
LIBS:u_mcu_atmel_atsam4e16c_tfbga-100
LIBS:u_mcu_atmel_atsame70n21_bga-100
LIBS:u_mcu_atmel_atsame70q21_lfbga144
LIBS:u_mcu_lpc2194-01_lqfp64
LIBS:lt1932
LIBS:pir
LIBS:bis00001
LIBS:CMCentralSensorSystem-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 3
Title "CM Central Sensor System"
Date "2017-02-19"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 17500 150  16350 10450
U 5905B356
F0 "CSS_Sensor_Board" 60
F1 "Central_Sensor_System_SensorBoard.sch" 60
$EndSheet
$Sheet
S 250  12750 16150 9500
U 590628FE
F0 "CSS_Display_Board" 60
F1 "Central_Sensor_System_Display_Board.sch" 60
$EndSheet
$EndSCHEMATC
