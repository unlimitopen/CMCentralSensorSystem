EESchema Schematic File Version 4
LIBS:CMCentralSensorSystem_ExtensionBoard-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 3
Title "CM Central Sensor System"
Date "2017-02-19"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 17500 150  16350 10450
U 5905B356
F0 "CSS_Sensor_Board" 60
F1 "Central_Sensor_System_SensorBoard.sch" 60
$EndSheet
$Sheet
S 250  12750 16150 9500
U 590628FE
F0 "CSS_Display_Board" 60
F1 "Central_Sensor_System_Display_Board.sch" 60
$EndSheet
$EndSCHEMATC
