﻿/*
 * CMLux.c
 *
 * Created: 26.01.2017 22:25:47
 *  Author: Christian Möllers
 */ 

#include <avr/io.h>
#include "../CMGlobal.h"

long convertAdcToLux(int adcValue)
{
	// depending of the Resistance used, you could measure better at dark or at bright conditions.
	// you could use a double circuit (using other LDR connected to analog pin 1) to have fun testing the sensors.
	// Change the value of Res0 depending of what you use in the circuit
	// the analog reading from the analog resistor divider
	
	// Resistance in the circuit of sensor 0 (KOhms)
	float Res=10.0;          
	
	// calculate the voltage
	float Vout = adcValue * 0.0048828125;   
	
	// calculate the Lux
	long lux = 500 / ( Res * ( ( 5 - Vout ) / Vout ) );        
	
	return lux;
}
