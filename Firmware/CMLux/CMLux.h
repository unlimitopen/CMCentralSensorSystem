﻿/*
 * CMLux.h
 *
 * Created: 26.01.2017 22:28:35
 *  Author: Christian Möllers
 */ 


#ifndef CMLUX_H_
#define CMLUX_H_


long convertAdcToLux(int adcValue);



#endif /* CMLUX_H_ */