/*
 * CMspi.c
 *
 * Created: 10.01.2015 19:54:32
 *  Author: c.moellers
 *
 *	Software architecture Informations
 *
 *  Following functions are implemented:
 *	- RFM12 Communication
 *	- DCF Module
 *
 *	Technical Informations
 *
 *  Information about Connecting the RFM12 Module
 *	VDD -> VCC
 *	SDI -> MOSI
 *	SCK -> SCK
 *	nSEL -> SS
 *	SDO -> MISO
 *	nIRQ -> INT0
 *	GND -> GND
 *	FSK/DATA/nFFS -> over R with 10kOhm to VCC
 *
 *  Information about Connecting the DCF Module
 *
 *	VDD -> VCC
 *	GND -> GND
 *	DATA -> PC5
 *	PON -> GND
 *	PON -> over R with 10kOhm to VCC
 *  DATA -> we do a filter on it
 */

#include <avr/io.h>
#include "CMGlobal.h"

#include "CMNodeDataHandle/CMNodeDataHandle.h"
#include "CMMasterNode/CMMasterNode.h"
#include "CMDS1307/CMDS1307.h"
#include "CMDcf/CMDcf.h"
#include "CMSpi/SPI.h"
#include "CMUart/CMUart0.h"
#include "CMRfm12/RFM12.h"
#include "CMBMP180/CMBMP180.h"
#include "CMLm75/CMLm75.h"
#include "CMAdcMessure/CMAdcMessure.h"
#include "CMMCP9808/CMMCP9808.h"
#include "CMPirSensor/CMPirSensor.h"
#include "CMDhtSensor/CMDhtSensor.h"
#include "CMAudioSensor/CMAudioSensor.h"
#include "CMLux/CMLux.h"
#include "CMHandy/CMHandy.h"

//#include <avr/interrupt.h>
//#include <util/delay.h>
//#include <stdlib.h>
//#include <string.h>

const char versionNumber[]="1008";
const char buildNumber[]="20181104";
const char nodeSNumber[10] = "16110000";

#define DateAndTimeSet	"Date and Time was set"
#define StartingCentralSensorSystem	"Starting System"
#define StartingInitialisize "Starting Initialisize"

#define startingMode			0
#define normalStatus			1
#define checkIfSMSavailable		2
#define setNodeConfig			3
#define readLocalSensors		4
#define viewAllNodes			5

// !!!!!  FIXMEE !!!!!! thats is horrible!!!! Please fix that 
#define masterNodeAddress		"9900"

volatile uint8_t TimeTick = FALSE;
volatile uint8_t receiveRFM12 = FALSE;
volatile uint8_t counter = 0;
volatile uint16_t sensorTimerCount = 0;
volatile uint16_t viewAllNodesTimerCount = 0;
volatile uint8_t state = normalStatus;
uint8_t securityState = FALSE;
uint16_t sensorTimerDefinition = 300;
volatile uint16_t tmpSensorCount = 0; 

//extern uint8_t sensorNodeQuery;
extern volatile uint8_t queryChange;
extern volatile uint16_t queryCounting;
extern uint8_t nodeType;

extern volatile uint8_t sec;
extern volatile uint8_t hour;
extern volatile uint8_t min;
extern volatile uint8_t day;
extern volatile uint8_t weekday;
extern volatile uint8_t month;
extern volatile uint8_t year;
extern volatile uint8_t tick;
extern volatile uint8_t high;
extern volatile uint8_t lowTick;
extern volatile uint8_t low;

extern char rxBuffer[DataSize];
extern char rxBuffer2[DataSize];
extern char *rxBufferPtr;
extern char *rxBuffer2Ptr;
extern uint8_t rxWritePosition;
extern uint8_t rxReadPosition;
volatile uint8_t rs232Counter;
extern volatile uint8_t rsUart0Success;

/**
	@brief	TIMER2_OVERFLOW_vector
	@param 	none
	@return	none
*/
ISR (INT1_vect)
{
	receiveRFM12 = TRUE;	
	RedLEDHigh;
}

/**
	@brief	TIMER2_OVERFLOW_vector
	@param 	none
	@return	none
*/
ISR (INT0_vect)
{
	/*
	// Very simple filter for noise!
	if (counter >=5)
	{

		counter = 0;
	}
	*/
	//receive = TRUE;
	//LedPinToggle;
	//_delay_ms(800);
	//counter++;
}

/**
	@brief	TIMER1_COMPAR_vector
	@param 	none
	@return	none
*/
ISR(TIMER1_COMPA_vect)
{
    sec++;

    if (sec >= 60)
    {
        min++;
		queryChange++;
        sec = 0;
        //tc = 0;
    }

    if (min >= 60)
    {
        hour++;
        min = 0;
    }

    if (hour >= 24)
    {
        hour = 0;
        day++;
    }

	// FIXME: Month correction
    if (day >= 31)
    {
        month++;
        day = 1;
    }

    if (month >= 13)
    {
        year++;
        month = 1;
    }
	
	tmpSensorCount++;
	
	TimeTick ^= 1;
	sensorTimerCount++;
	viewAllNodesTimerCount++;
	rs232Counter++;
	GreenLEDToggle;
}

/**
	@brief	TIMER2_OVERFLOW_vector for 10 ms
	@param 	none
	@return	none
*/
ISR (TIMER2_OVF_vect)
{
	TCNT2 = 183;

	// We use here a timer for the
	// dcf input signal.
	if (dcfReadingSignal)
	{
		tick = TRUE;
		high++;
		lowTick = FALSE;
	}
	else
	{
		tick = FALSE;
		lowTick = TRUE;
		low++;
	}
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setupInit(void)
{
	USART_init();
	
	#if (printDebugging == TRUE)
		USART_print("Starting Central Sensor System UART0 \n");
		USART_print("Version: ");
		USART_print(versionNumber);
		USART_print("\n");
		USART_print("  Build: ");
		USART_print(buildNumber);
		USART_print("\n");
	#endif
	
	initNodeProtocol();
	//USART_print("Starting initialisation \n");
	// Initialisize
	spiInitMaster();
	_delay_ms(250);
	// initalisize the rfm12 module
	rfm12Init();

	//USART1_init();
	//USART1_print("Starting Central System UART1 \n");

	//#define RedLEDOutput	DDRD |= (1<<RedLED)
	//#define RedLEDInput		DDRD &=~ (1<<RedLED)
	//#define RedLEDHigh		PORTD |= (1<<RedLED)
	//#define RedLEDLow		PORTD &=~ (1<<RedLED)
	
	//Define LED Output
	RedLEDOutput;
	GreenLEDOutput;
	
	// initialisize the bmp180 sensor
	bmp180Init();
	
	// initialisize the rtc 
	initDS1307();
	getTimeVariablesFromDS1307();
	
	pirDirection(TRUE);
	audioSensorDirection(FALSE);
	
	// Interrupt handling with INT0
	//interrupt0Input;
	//interrupt0High;

	// Interrupt handling with INT1
	interrupt1Input;

	EICRA |= (0<<ISC10)| (1<<ISC11);
	EIMSK &= (1<<INT1);
	//MCUCR |= (1 << ISC10);    // set INT0 to trigger on ANY logic change

	//EIFR |= (0<<INTF1);
	//MCUCR &= ~(1<<ISC00);
	//MCUCR |= (1<<ISC01); // | (1<<ISC00);
	//DDRD &= ~(1 << DDD2);     // Clear the PD2 pin
	// PD2 (PCINT0 pin) is now an input
	//PORTD |= (1 << PORTD2);    // turn On the Pull-up
	// PD2 is now an input with pull-up enabled
	//EICRA |= (1 << ISC00) | (1 << ISC00);    // set INT0 to trigger on ANY logic change
	//EIMSK |= (1 << INT0);     // Turns on INT0
	//DDRB |= (1<<PB0) | (1<<PB1);	//Set up PORTB.0 and PORTB.1 to output
	//EIFR = (1<<INTF0);						//Clear interrupt flags
	//EIMSK |= (1<<INT0);						//Enable external int0
	//EICRA |= (1<<ISC00);					//Set to logical state change interrupt
	//EICRA |= (1<<ISC01);					//Set to falling edge interrupt
	//EICRA |= (1<<ISC01); //(1<<ISC00);					//Set to rising edge interrupt

	// Timer0 definition
	//TCCR0 |= (1<<CS00) | (1<<CS01) | (1<<CS02);
	//TCCR0 |= (1<<CS00) | (1<<CS02);
	//TCCR0 |= (1<<CS00);
	//TCNT0 = 0xFF;

	// timer1 definition
	//OCR1A = 31249;
	//OCR1A =   19531;
	//OCR1A = 14135;
	OCR1A = 14440;
	TCNT1 = 0;
	// Timer1- Overflow Interrupt activation
	TCCR1B |=  (1<<WGM12) | (1<<CS10) | (1<<CS12);

	// Timer2 with Prescaler
	//TCCR2 |= (1<<CS21);
	//TCCR2B |= (1<<CS20) | (1<<CS22) | (1<<CS21);
	TCCR2B |= (1<<CS22) | (1<<CS20) | (1<<CS21);
	TCNT2 = 183;
	//Vorladewert = 2^16 - (Wunschzeit * Quarzfrequenz / Prescaler)
	//2^16 - (        1s           *       1000000 Hz /           64    ) = 49911

	// Timer1- Compare Match activation
	// Timer2- Overflow Interrupt activation
	TIMSK1 |= (1<<OCIE1A);
	TIMSK2 |= (1<<TOIE2);
}


/**
	@brief	Alarm function
	@param 	none
	@return	none
*/
void checkSituations(void)
{
	static uint8_t dcfAccess = FALSE;
	unsigned char rs232String[DataSize];
	uint8_t success = FALSE;
	
	memset((void *) rs232String, 0, DataSize);

	// make a dcf sync in the night
	if ( min == 0 && hour == 3 && dcfAccess == FALSE)
	{
		dcfAccess = TRUE;
		success = dcfSyncing();
	}

	// We must save the irq load.
	if (queryCounting >= 288 && hour == 0 && min == 0 && sec >= 0 && sec <= 25 )
	{
		queryCounting = 0;
		dcfAccess = FALSE;
	}
	
	// This is for the network nodes  this is a check how often a
	// node comes up with new informations! in 5 minutes we need
	// all nodes read once a time, if we have nodes that don't come up
	// we need to delete them from node struct table or poll the node
	// to come up and give us the informations
	if (queryChange >= 5)
	{
		queryCounting++;
		queryChange = 0;
	}
	
	// needed for reading the local sensors
	if (sensorTimerCount >= sensorTimerDefinition)
	{
		state = readLocalSensors;
	}
	
	// needed for reading the local for security situations
	if (tmpSensorCount >= 1 && securityState == TRUE)
	{
		state = readLocalSensors;
	}
}

void zahlzeigen(uint8_t *zahl, uint8_t *neuzahl)
{
	uint8_t i = 0;
	
	for (i=0; i<6;i++)
	{
		*neuzahl++ = *zahl++;
	}
}

/**
	@brief	main
	@param 	none
	@return	none
*/
int main (void)
{
	cli();
	uint8_t rfm12Success = FALSE;

	setupInit();
	
	//success = initialisizeHandyConnection();
	// here we can check if the rfm12 is ready!
	rfm12Success = rfm12PORCheckCheck();

	// RFM12 Handling definitions for RFM12 Communication
	//rfm12CommunicationDirection(RXMode);

	//rfm12Wakeup();
	//_delay_ms(500);
	//rfm12CommunicationDirection(RXMode);
	rfm12CommunicationDirection(RXIntMode);
	
	//if( (TIFR0 = TIFR0 & ~(1<<TOV1)) & (1<<TOV0) );
	//EIFR |= (1<<INTF1);
		
	// In the first start, we need to delete
	// the garbage in the node struct table
	deleteAllNodeAdress();

	// define masternodeAddress
	defineMasterNodeAddress(masterNodeAddress, nodeSNumber);

	//initT6963();

	//EIFR |= (1<<INTF1);
	//EIMSK &=~ (1<<INT1); // no more interrupts
	//EIMSK |= (1<<INT1); // no more interrupts

	// DCF Handling
	//dcfSyncing();
	
	//uint8_t check = FALSE;
	unsigned char nodeAddress[5] = "0";

	unsigned char dataInformation[DataSize];
	dataInformation[0] = '\0';

	unsigned char *dataInformationPtr;
	dataInformationPtr = dataInformation;

	rxBufferPtr = rxBuffer;
	rxBuffer2Ptr = rxBuffer2;
	// set the pointer information

	//Variable Declaration
	//char data[5];
	
	uint8_t success = FALSE;
	//uint8_t i = 0;
	//uint8_t status = FALSE;
	//uint8_t sendingQuery = 0;
	//uint16_t sendingStopCounter = 0;
	//uint8_t sendStatus = 0;
	//uint8_t sendHealth = 0;

	float feuchtigkeit = 0.0;
	float spannung = 0.0;
	float LM75Temperatur = 0.0;
	float airpressure = 0.0;

	float dht11HumityLow = 0;
	float dht11temperaturLow = 0.0;
	float *dht11HumityLowPtr = &dht11HumityLow;
	float *dht11temperaturLowPtr = &dht11temperaturLow;
	
	float battery = 0.0;
	int lightintense = 0;
	int audioSense = 0;
	int gassensor = 0;
	uint8_t pirSensor = 0;
	int helperInt = 0;
	long helpLong = 0;
	float temp = 0;
	float press = 0;
	float altitude = 0.0;
	char infoPtr[DataSize];
	char StringPtr[DataSize];
	int crcCheck = 0;
	uint8_t sensorNodeQuery = FALSE;
	
	#if (printDebugging == TRUE)
	
		if (rfm12Success == TRUE)
		{
			USART_print("RFM12 Ready \n");	
		}
		else
		{
			USART_print("RFM12 not Ready \n");
		}
	#endif
	
	// INSERTING: If the masternode comes up with "1246" and we become from css a 1245 with date and time: YY-MM-DD hh:mm:ss 
	
	sensorTimerCount = 300;
	state = readLocalSensors;
	EIMSK |= (1<<INT1);
	
	// Enable Interrupts!
	sei();
	
	while(1)
	{	
		switch(state)
		{
			//case startingMode:
			//{
				//getRsDataConfig(StringPtr, getRsDataGetTimeSetup);
//
				//if (success == TRUE)
				//{
					//state = normalStatus;	
				//}
				//break;
			//}
			
			case normalStatus:
			{
				// the normal status is really interesting 
				if (rfm12Success == TRUE);
				{
					if (receiveRFM12 == TRUE)
					{
						receiveRFM12 = FALSE;
						RedLEDLow;
						sensorNodeQuery = receiveNode(nodeAddress);
					}
					
					if ( sensorNodeQuery == TRUE)
					{
						tailInformation("",dataInformationPtr, nodeAddress);

						readAlarmManagement(nodeAddress);
						sensorNodeQuery = FALSE;
					}
				}
				
				//if (rsUart0Success == TRUE)
				//{
					//getRsDataConfig(StringPtr, 1);
					//rsUart0Success = FALSE;
				//}
				break;
			}

			//case checkIfSMSavailable:
			//{
				//if (TimeTick == TRUE)
				//{
					//TimeTick = FALSE;
					//USART1_print("Check if sms is available:\n");
					//success = workWithHandy();
					//if (success == TRUE)
					//{
						//USART1_print("sms is available:\n");
						//success = FALSE;
					//}
				//}
				//break;
			//}
			
			case setNodeConfig:
			{

				break;
			}
			
			case readLocalSensors:
			{
				// At this point we switch to read the local
				// sensors, these sensors are the principle for the
				// CSS, because these values are reached by itself.
				memset(infoPtr, 0, DataSize);
				memset(StringPtr, 0, DataSize);
				
				// ########### Node Management 
				strcat(StringPtr, "N#:");
				strcat(StringPtr, "9900");
				strcat(StringPtr, ";");
				crcCheck += makeCrcCheckSum("9900");
					
				// ########### Serial Management 
				strcat(StringPtr, "S#:");
				strcat(StringPtr, nodeSNumber);
				strcat(StringPtr, ";");
				crcCheck += makeCrcCheckSum(nodeSNumber);
				
				// ########### Type Management 
				strcat(StringPtr, "T#:");
				itoa(nodeType, infoPtr, 10);
				strcat(StringPtr, infoPtr);
				strcat(StringPtr, ";");
				crcCheck += makeCrcCheckSum(infoPtr);
				
				// ########### Security State Management
				strcat(StringPtr, "I#:");
				itoa(securityState, infoPtr, 10);
				strcat(StringPtr, infoPtr);
				strcat(StringPtr, ";");
				crcCheck += makeCrcCheckSum(infoPtr);
								
				// ########### Version Management
				strcat(StringPtr, "V#:");
				strcat(StringPtr, versionNumber);
				strcat(StringPtr, ";");
				crcCheck += makeCrcCheckSum(versionNumber);
				
				if (sensorTimerCount >= sensorTimerDefinition)
				{
					// ########### Temparatur1 Management 
					strcat(StringPtr, "T1:");
					LM75Temperatur = readLM75TemparaturSensor();
					dtostrf(LM75Temperatur, 5,2, infoPtr);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
					
					// ########### Temparatur2 Management
					strcat(StringPtr, "T2:");
					readDhtSensor(&dht11HumityLow, &dht11temperaturLow);
					dtostrf(dht11temperaturLow, 5,2, infoPtr);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
						
					// ########### Temparatur3 Management 
					strcat(StringPtr, "T3:");
					helperInt = getBMP180CalcTemp();
					temp = helperInt / 10;
					dtostrf(temp, 2,2, infoPtr);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);

					// ###########
					strcat(StringPtr, "A1:");
					helpLong = getBMP180CalcPress();
					press = helpLong / 100;
					dtostrf(press, 5,2, infoPtr);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
						
					// ########### Humidity Management
					strcat(StringPtr, "H1:");
					dtostrf(dht11HumityLow, 5,2, infoPtr);
					//itoa(dht11HumityLow, infoPtr,10);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
						
					//// ###########
					//altitude = getBMP180altitude(press);//
					//dtostrf(altitude, 4,2,infoPtr);
					//strcat(StringPtr, infoPtr);
					//strcat(StringPtr, ";");
					//
					//// ###########
					//getBMP180Altimetry(press, alti);
					//strcat(StringPtr, alti);
					//strcat(StringPtr, ";");
				}
				
				if (sensorTimerCount >= sensorTimerDefinition || securityState == TRUE)
				{
					// ########### Motion Management
					pirSensor = getPirMotionSensor();
					strcat(StringPtr, "M1:");
					itoa(pirSensor, infoPtr, 10);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
				
					// ########### Light Management
					lightintense = adcMessure(5);
					strcat(StringPtr, "L1:");
					ltoa(convertAdcToLux(lightintense), infoPtr, 10);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
				
					// ########### Audio Management
					audioSense = getAudioNoise(7);
					strcat(StringPtr, "A2:");
					itoa(audioSense, infoPtr, 10);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
				
					// ########### GasSensor Management
					gassensor = adcMessure(6);
					strcat(StringPtr, "G1:");
					itoa(gassensor, infoPtr, 10);
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
					 
					// ########### BATTERY Management
					strcat(StringPtr, "B1:");
					battery = (5.000 * adcMessure(1) ) / 1023;
					dtostrf( battery, 2, 2, infoPtr );
					strcat(StringPtr, infoPtr);
					strcat(StringPtr, ";");
					crcCheck += makeCrcCheckSum(infoPtr);
				}
					
				// ########### CRC Management
				strcat(StringPtr, "CR:");
				itoa(crcCheck, infoPtr,10);
				strcat(StringPtr, infoPtr);
				crcCheck = 0;
				
				#if (printDebugging == TRUE)
					USART_print(StringPtr);
					USART_print("\n");
				#endif
				
				makeDataUpdate(StringPtr);
				tailInformation("", dataInformation, "9900");
				
				securityState = FALSE;
				tmpSensorCount = 0;
				sensorTimerCount = 0;
				state = normalStatus;
				
				break;
			}
			
			case viewAllNodes:
			{
				viewNodeAdress();
				viewAllNodesTimerCount = 0;
				state = normalStatus;
				break;
			}
		}
		checkSituations();
	}
	return 0;
}

