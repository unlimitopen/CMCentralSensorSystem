﻿/*
 * CMMCP9808.c
 *
 * Created: 23.07.2016 18:20:30
 *  Author: Christian Möllers
 */ 

 #include <avr/io.h>
 #include "CMMCP9808.h"
 #include "../CMTwi/CMTwi.h"
 #include "../CMAdcMessure/CMAdcMessure.h"
 #include "../CMGlobal.h"

 #define twWrite			0
 #define twRead				1

 #define AddressByte		0x30

 int getMCP9808Temperatur(void)
 {
	uint8_t UpperByte = 0;
	uint8_t LowerByte = 0;
	int Temperature = 0;
	
	i2cInit();
	i2cStart(AddressByte | twWrite); // send START command
	i2cSend(0x05);
	i2cStart(AddressByte | twRead); // READ Command (see Section 4.1.4 “Address Byte”) //also, make sure bit 0 is Set ‘1’
	UpperByte = i2cReceiveAck(); // READ 8 bits  //and Send ACK bit
	LowerByte = i2cReceiveNack(); // READ 8 bits //and Send NAK bit
	i2cStop(); // send STOP command
	//Convert the temperature data
	//First Check flag bits
	if ((UpperByte & 0x80) == 0x80)
	{ 
		//TA ³ TCRIT
	}
	
	if ((UpperByte & 0x40) == 0x40)
	{ 
		//TA > TUPPER
	}

	if ((UpperByte & 0x20) == 0x20)
	{
		//TA < TLOWER
	}

	UpperByte = UpperByte & 0x1F; //Clear flag bits

	if ((UpperByte & 0x10) == 0x10)
	{ 
		//TA < 0°C
		UpperByte = UpperByte & 0x0F; //Clear SIGN
		Temperature = 256 - (UpperByte * 16 + LowerByte / 16);
	}
	else
	{
		//TA ³ 0°C
		Temperature = (UpperByte * 16 + LowerByte / 16); //Temperature = Ambient Temperature (
	}
	
	return Temperature;
}
