﻿/*
 * CMBMP180.h
 *
 * Created: 23.07.2016 15:50:51
 *  Author: Christian Möllers
 */ 


#ifndef CMBMP180_H_
#define CMBMP180_H_

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#define twWrite 0
#define twRead 1
#endif

// define the altitude 
#define ALTITUDEMETER	60.00

void bmp180Init(void);
void getBMP180EEpromValues(void);
unsigned int getBMP180TempValue(void);
unsigned long getBMP180PressValue(void);
short getBMP180CalcTemp(void);
char bmp180CheckSensor(void);
long getBMP180CalcPress(void);
float getBMP180altitude(long pressure);
void bmp180ResetSensor(void);
void getBMP180Altimetry(long pressure, char *altimetry);

#endif /* CMBMP180_H_ */