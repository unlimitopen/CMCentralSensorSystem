﻿/*
* CMBMP180.c
*
* Created: 23.07.2016 15:50:32
*  Author: Christian Möllers
*/ 

#include "CMBMP180.h"

#include <avr/io.h>
#include "../CMGlobal.h"
#include "../CMTwi/CMTwi.h"
#include <util/delay.h>
#include <math.h>
#include <string.h>

#define BMP180EEPROMREADADDRESS			0xEE
#define testing							0

// (00b: single, 01b: 2 times, 10b: 4 times, 11b: 8 times).

const unsigned char oss = 0;

int bmp180AC1 = 0;
int bmp180AC2 = 0;
int bmp180AC3 = 0;
unsigned int bmp180AC4 = 0;
unsigned int bmp180AC5 = 0;
unsigned int bmp180AC6 = 0;
int bmp180B1 = 0;
int bmp180B2 = 0;
int bmp180MB = 0;
int bmp180MC = 0;
int bmp180MD = 0;
long bmp180B5;

short temperature;
long pressure;

void getBMP180EEpromValues(void)
{
	bmp180AC1 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xAA);
	bmp180AC2 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xAC);
	bmp180AC3 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xAE);
	bmp180AC4 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xB0);
	bmp180AC5 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xB2);
	bmp180AC6 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xB4);
	bmp180B1 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xB6);
	bmp180B2 = i2cReadInt(BMP180EEPROMREADADDRESS, 0xB8);
	bmp180MB = i2cReadInt(BMP180EEPROMREADADDRESS, 0xBA);
	bmp180MC = i2cReadInt(BMP180EEPROMREADADDRESS, 0xBC);
	bmp180MD = i2cReadInt(BMP180EEPROMREADADDRESS, 0xBE);
}

void getBMP180Altimetry(long pressure, char *altimetry)
{
	const float p0 = 101325;     // Pressure at sea level (Pa)
	const float currentAltitude = ALTITUDEMETER; // current altitude in METERS
	const float ePressure = p0 * pow((1-currentAltitude/44330), 5.255);  // expected pressure (in Pa) at altitude
	float weatherDiff;

	// Add this into loop(), after you've calculated the pressure
	weatherDiff = pressure - ePressure;
	if(weatherDiff > 250)
	{
		strcat(altimetry, "SUN");
	}
	else if ((weatherDiff <= 250) || (weatherDiff >= -250))
	{
		strcat(altimetry, "Cloudy");
	}
	else if (weatherDiff > -250)
	{
		strcat(altimetry, "Rain");
	}
 }

void bmp180ResetSensor(void)
{
	i2cStart(BMP180EEPROMREADADDRESS | twWrite);
	i2cSend(0xE0);
	i2cSend(0xB6);
	i2cStop();
	_delay_ms(5);
}

unsigned int getBMP180TempValue(void)
{
	unsigned int bmp180UT;

	//write 0x2E into reg 0xF4, wait 4.5ms
	//read reg 0xF6 (MSB), 0xF7 (LSB)
	//UT = MSB << 8 + LSB
	i2cStart(BMP180EEPROMREADADDRESS | twWrite);
	i2cSend(0xF4);
	i2cSend(0x2E);
	i2cStop();
	_delay_ms(5);
	i2cStart(BMP180EEPROMREADADDRESS | twWrite);	
	i2cSend(0xF6);
	i2cStart(BMP180EEPROMREADADDRESS | twRead);
	bmp180UT = i2cReceiveAck() << 8;
	bmp180UT |= i2cReceiveNack();
	i2cStop();

	return bmp180UT;
}

short getBMP180CalcTemp(void)
{ 
	short temp = 0;
	long bmp180X1, bmp180X2;
	unsigned int bmp180UT = 0;
	
	bmp180UT = getBMP180TempValue();

	bmp180X1 = ( (long) bmp180UT - (long) bmp180AC6) * (long) bmp180AC5 >> 15;
	bmp180X2 = ( (long) bmp180MC << 11 ) / (bmp180X1 + bmp180MD);
	bmp180B5 = bmp180X1 + bmp180X2;
	temp = (bmp180B5 + 8) >> 4;

	return temp;
}

float getBMP180altitude(long pressure)
{
	float altitude = 0.0;
	const float p0 = 101325;     // Pressure at sea level (Pa)
	// Add this into loop(), after you've calculated the pressure
	altitude = (float)44330 * (1 - pow(((float) pressure/p0), 0.190295));

	return altitude;
}

unsigned long getBMP180PressValue(void)
{	
	unsigned char MSB = 0;
	unsigned char LSB = 0;
	unsigned char XLSB = 0;
	unsigned long bmp180UP = 0;

	//write 0x34+(oss<<6) into reg 0xF4, wait
	//read reg 0xF6 (MSB), 0xF7 (LSB), 0xF8 (XLSB)
	//UP = (MSB<<16 + LSB<<8 + XLSB) >> (8-oss)
	i2cStart(BMP180EEPROMREADADDRESS | twWrite);
	i2cSend(0xF4);
	i2cSend(0x34 + (oss << 6));
	i2cStop();
	_delay_ms(5);

	MSB = i2cReadChar(BMP180EEPROMREADADDRESS, 0xF6);
	LSB = i2cReadChar(BMP180EEPROMREADADDRESS, 0xF7);
	XLSB = i2cReadChar(BMP180EEPROMREADADDRESS, 0xF8);
	
	bmp180UP = (((unsigned long) MSB << 16) | ((unsigned long) LSB << 8) | (unsigned long) XLSB) >> (8-oss);

	/*	
	i2cStart(0xEE | twWrite);
	i2cSend(0xF6);
	i2cStart(0xEE | twRead);
	bmp180UP = i2cReceiveAck()<<16;
	bmp180UP |= (i2cReceiveAck()) << 8;
	bmp180UP |= (i2cReceiveNack()) >> (8-oss) ;
	
	//bmp180UP = (((unsigned long) MSB << 16) | ((unsigned long) LSB << 8) | (unsigned long) XLSB) >> (8-oss);
	//bmp180UP |= ((unsigned long) XLSB) >> (8-oss);
	//bmp180UP = (( MSB << 16 ) | ( LSB << 8) | ( XLSB ) >> (8-oss) );
	//(((unsigned long) msb << 16) | ((unsigned long) lsb << 8) | (unsigned long) xlsb) >> (8-OSS);
	*/
	i2cStop();

	return bmp180UP;
}

long getBMP180CalcPress(void)
{
	long bmp180X1, bmp180X2, bmp180X3, bmp180B3, bmp180B6, press;
	unsigned long bmp180B4, bmp180B7;

	unsigned long bmp180UP = getBMP180PressValue();

	bmp180B6 = bmp180B5 - 4000;
	// Calculate B3
	bmp180X1 = (bmp180B2 * (bmp180B6 * bmp180B6)>>12)>>11;
	bmp180X2 = (bmp180AC2 * bmp180B6)>>11;
	bmp180X3 = bmp180X1 + bmp180X2;
	bmp180B3 = (((((long)bmp180AC1)*4 + bmp180X3)<<oss) + 2)>>2;
	
	// Calculate B4
	bmp180X1 = (bmp180AC3 * bmp180B6)>>13;
	bmp180X2 = (bmp180B1 * ((bmp180B6 * bmp180B6)>>12))>>16;
	bmp180X3 = ((bmp180X1 + bmp180X2) + 2)>>2;
	bmp180B4 = (bmp180AC4 * (unsigned long)(bmp180X3 + 32768))>>15;
	
	bmp180B7 = ((unsigned long)(bmp180UP - bmp180B3) * (50000>>oss));
	if (bmp180B7 < 0x80000000)
	{
		press = (bmp180B7<<1)/bmp180B4;
	}
	else
	{
		press = (bmp180B7/bmp180B4)<<1;
	}
	
	bmp180X1 = (press>>8) * (press>>8);
	bmp180X1 = (bmp180X1 * 3038)>>16;
	bmp180X2 = (-7357 * press)>>16;
	press += (bmp180X1 + bmp180X2 + 3791)>>4;

	return press;
}


void Error(int errcode, char* fmt, ...)
{
	 //=> "error(ERRCODE): ..."
	 
	//printf("CODE: %i MESSAGE: %s \n", errcode, fmt);
	 
}

char bmp180CheckSensor(void)
{
	char success = FALSE;

	if ( i2cReadChar(BMP180EEPROMREADADDRESS, 0xD0) == 0x55)
	{
		success = TRUE;
	}

	return success;
}

void bmp180Init(void)
{	
	i2cInit();
	// Check if ic is online
	if ( bmp180CheckSensor() )
	{
		if ( testing == 1)
		{
			bmp180AC1 = 408;
			bmp180AC2 = -72;
			bmp180AC3 = -14383;
			bmp180AC4 = 32741;
			bmp180AC5 = 32757;
			bmp180AC6 = 23153;
		
			bmp180B1 = 6190;
			bmp180B2 = 4;
		
			bmp180MB = -32767;
			bmp180MC = -8711;
			bmp180MD = 2868;
		
			//bmp180UT = 27898;
			//bmp180UP = 23843;
		}
		else
		{
			getBMP180EEpromValues();
		}
	}
}

