
#include <avr/io.h>
#include "pduConvert.h"
#include "../CMUart/CMUart0.h"

static const unsigned char mask[] =
{
    0001,   /* 00000001 */
    0003,   /* 00000011 */
    0007,   /* 00000111 */
    0017,   /* 00001111 */
    0037,   /* 00011111 */
    0077,   /* 00111111 */
    0177,   /* 01111111 */
};

void ascii2pdu (char *sept, const unsigned char *text)
{
    int pos = 0;
    do
    {
        if (*text & 0x80)
        {
            printf ("Bit 8 ist gesetzt \n");
            break;
        }

        *sept = *text >> pos;
        *sept |= (*(text+1) & mask[pos]) << (7 - pos);

        if (pos != 7) /* nach 7 Zeichen haben wir eins eingespart */
        sept++;

        pos = (pos + 1) % 8;

    }while (*text++);
    sept = '\0';
}


void pdu2ascii (unsigned char *text, unsigned char *sept)
{
    unsigned int pos = 0;
    do
    {
        *sept = *text;

        if (pos >0)
        {
            *sept = *text++ >> (8-pos);
        }
        
        *sept |= (*text << pos);
        *sept &= 0x7F;
        
        if (pos != 8) 
        sept++;

        pos = (pos + 1) % 8;

    }while (*text != '\0');
    sept = '\0';
}


