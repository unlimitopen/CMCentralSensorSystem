/*
 * IncFile1.h
 *
 * Created: 16.02.2015 11:09:49
 *  Author: c.moellers
 */

#ifndef SPI_H_
#define SPI_H_

#define MOSI_PIN			PB5
#define MISO_PIN			PB6
#define SCK_PIN				PB7
//#define SS_PIN				PB2

void spiInitMaster();
void spiInitSlave();
unsigned char spiTrasmitData(unsigned char data);
unsigned char spiReceiveData();

#endif /* INCFILE1_H_ */