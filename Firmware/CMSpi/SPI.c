/*
 * CFile1.c
 *
 * Created: 16.02.2015 11:09:35
 *  Author: c.moellers
 */ 

#include <avr/io.h>
#include "SPI.h"

#include "../CMGlobal.h"

void spiInitMaster()
{
	DDRB |= (1<<SCK_PIN)|(1<<MOSI_PIN); // SCK, MOSI as outputs
	DDRB &= ~(1<<MISO_PIN); // MISO as input

	SPCR |= (1<<MSTR); // Set as Master
	SPCR |= (1<<SPR0)|(1<<SPR1);       // divide clock by 128
	//SPCR |= (1<<SPR0)|(1<<SPI2X);       // divide clock by 8
	//SPCR |= (1<<SPR0)|(1<<SPI2X);       // divide clock by 8
	SPCR |= (1<<SPE);  // Enable SPI
}

void spiInitSlave()
{
	DDRB &= ~((MOSI_PIN)|(SCK_PIN)); // SCK, MOSI SS as inputs
	DDRB |= (MISO_PIN); // MISO as output

	SPCR &= ~(1<<MSTR);                // Set as slave
	SPCR |= (1<<SPR0)|(1<<SPR1);       // divide clock by 128
	//SPCR |= (1<<SPR0)|(1<<SPI2X);       // divide clock by 8
	//SPCR |= (1<<SPR0)|(1<<SPI2X);       // divide clock by 8
	SPCR |= (1<<SPE);                  // Enable SPI
}

unsigned char spiTrasmitData(unsigned char data)
{
	SPDR = data;
	while (!(SPSR & (1<<SPIF)));
	data = SPDR;
	return data;
}

unsigned char spiReceiveData()
{
	unsigned char data = '\0';
	
	while(!(SPSR & (1<<SPIF)));    // wait until all data is received
	data = SPDR;                   // hurray, we now have our data
	
	return data;
}

