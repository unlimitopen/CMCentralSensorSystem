/*
 * CMOneWire.c
 *
 * Created: 14.03.2015 13:24:21
 *  Author: Christian M�llers
 */ 

#include <avr/io.h>
#include <string.h>
#include "CMGlobal.h"
#include <util/delay.h>

// 1Wire Sensor
#define DS18S20_PIN	        PB2
#define DS18S20_REG			DDRB
#define DS18S20_PORT		PORTB
#define DS18S20_PINB		PINB
#define DS18S20_OUTPUT      (DS18S20_REG |= (1<<DS18S20_PIN))
#define DS18S20_INPUT       (DS18S20_REG &=~ (1<<DS18S20_PIN))
#define DS18S20_HI          (DS18S20_PORT |= (1<<DS18S20_PIN))
#define DS18S20_LOW         (DS18S20_PORT &=~ (1<<DS18S20_PIN))
#define DS18S20_TOG         (DS18S20_PORT ^= (1<<DS18S20_PIN))
#define DS18S20_ENABLE		(DS18S20_PINB &(1<<DS18S20_PIN))
#define DS18S20_DISABLE		(!(DS18S20_PINB &(1<<DS18S20_PIN)))

// 1Wire LED
#define DS18S20_LED_PIN		PD7
#define DS18S20_LED_OUTPUT  (DDRD |= (1<<DS18S20_LED_PIN))
#define DS18S20_LED_INPUT   (DDRD &=~ (1<<DS18S20_LED_PIN))
#define DS18S20_LED_HI      (PORTD |= (1<<DS18S20_LED_PIN))
#define DS18S20_LED_LOW     (PORTD &=~ (1<<DS18S20_LED_PIN))
#define DS18S20_LED_TOG     (PORTD ^= (1<<DS18S20_LED_PIN))

// global variables
volatile uint8_t temp;          // temp for 1 wire

// write bit
// Man kann kein einzelndes Bit zur�ckgeben, deshalb uint8_t
void wr_bit(int byte) 
{
	//DDRC|=(1<<PC5); //Bus auf low ziehen
	DS18S20_OUTPUT;
	//DS18S20_INPUT();
	//PORTC &=~ (1<<PC5);
	_delay_us(1);

	if (byte)
	{
		DS18S20_HI;
		//PORTC |= (1<<PC5);
		_delay_us(1);
	}
	_delay_us(110);
	DS18S20_INPUT;
	//DDRC &=~ (1<<PC5);
	DS18S20_LOW;
	//PORTC &=~ (1<<PC5);
}

// write byte
void wr_byte(uint8_t byte)
{
	uint8_t i;
	_delay_us(5);

	for(i=0;i<8;i++) //ein Byte sind 8 Bit
	{
		if (byte & (1<<i))
		wr_bit(1);
		else
		wr_bit(0);
	}
	_delay_us(120);
}

// read bit
// Man kann kein einzelndes Bit zur�ckgeben, deshalb uint8_t
int rd_bit(void) 
{
	//DDRC|=(1<<PC5); //Bus auf low ziehen
	DS18S20_OUTPUT;
	DS18S20_LOW;
	//PORTC &=~ (1<<PC5);
	_delay_us(1); //mindestens 1�s warten
	DS18S20_INPUT;
	//DDRC&=~(1<<PC5); //Bus wieder loslassen
	_delay_us(12); //zur Sicherheit, ein asm("nop") w�rde wohl auch reichen...

	//Bus abfragen und Wert speichern
	if(DS18S20_ENABLE) 
	{
		//bit=1;
		return 1;
	}
	else
	{
		//bit=0;
		return 0;
	}
}

// read byte
int rd_byte(void)
{	uint8_t byte=0;
	int i;
	for(i=0;i<8;i++) //ein Byte sind 8 Bit
	{
		if (rd_bit ())
		{
			byte |= (1<<i);
		}
		_delay_us(120);
	}
	return byte; //und Ergebnis zur�ckgeben
}

// Reset and presence together
int reset(void)
{
	//DDRC |= (1<<PC5);
	DS18S20_OUTPUT;
	DS18S20_LOW;
	//PORTC &= ~(1<<PC5);
	_delay_us(480);
	//DDRC &=~ (1<<PC5);
	DS18S20_INPUT;
	_delay_us(80);

	// check if pin pc5 will
	// be activate
	if (DS18S20_DISABLE)
	{
		_delay_us(450);
		return 1;
	}
	else
	{
		return 0;
	}
}

void readOneWireSensor(unsigned char * neutemp2)
{
	DS18S20_LED_OUTPUT;
	float ftemp2;
	//const char neutemp2;
	//char neutemp2[9];
	uint8_t wert[9];
	uint8_t returncode = 0;
	uint8_t i;
	// 1. Reset
	returncode = reset();
	// pruefe returncode
	if (returncode == 1)
	{
		// If one DS18S20 reachable
		// the DS18S20 RED LED will be
		// deactivated
		DS18S20_LED_LOW;

		// schreibe SKIP ROM
		wr_byte(0xcc);

		//Read ROM!
		//wr_byte(0xff);

		// schreibe CONVER_T
		wr_byte(0x44);

		// warte 750 ms
		_delay_ms(750);

		// erneuter Reset
		reset();

		// schreibe, SKIP ROM
		wr_byte(0xcc);
		
		// schreibe READ ROM
		wr_byte(0xbe);
		
		// lese byte ein wert

		for (i=0;i<9;i++)
		{
			wert[i] = rd_byte();
		}

		if ( wert[0] == 0)
		{
			ftemp2 = (wert[1] * 100) / 2;
			//ftemp2 = wert[1]/2;
			//ftemp2 = ftemp2 * 100;
		}
		else
		{
			ftemp2 = (~wert[0])+1;
			ftemp2 = (ftemp2*(-1))/2;
		}
		//TODO: Look for another convertion!!
		dtostrf( ftemp2, 4, 2, neutemp2 );
		//USART_print("\n");
		//uint8_t zahl = 25;
		//neutemp2 = itoa(zahl);
		//neuitoa(zahl, neutemp2);
		//itoa(zahl,neutemp2);
		//uart_putf(ftemp2);
		DS18S20_LED_LOW;
		//return neutemp2;
	}
	else
	{
		DS18S20_LED_HI;
		//neutemp2[0] = "0";
		//return neutemp2;
	}
}