/*
 * CMTwi.h
 *
 * Created: 16.03.2015 21:10:18
 *  Author: PC
 */ 


#ifndef CMTWI_H_
#define CMTWI_H_

void i2cInit(void);
unsigned char i2cStart(unsigned address);
void i2cStop(void);
unsigned char i2cSend(uint8_t dataByte);
uint8_t i2cReceive (uint8_t ack);
unsigned char i2c_start(unsigned char address);
//unsigned char i2c_readAck(void);
//unsigned char i2c_readNak(void);
unsigned char i2c_write( unsigned char data );
unsigned char i2cReceiveNack (void);
unsigned char i2cReceiveAck (void);
void i2c_start_wait(unsigned char address);
int i2cReadInt(char icAddress, char rom);
char i2cReadChar(char icAddress, char rom);

#endif /* CMTWI_H_ */