/*
 * CMLm75.h
 *
 * Created: 22.03.2015 15:39:50
 *  Author: PC
 */ 


#ifndef CMLM75_H_
#define CMLM75_H_

float readLM75TemparaturSensor(void);
void lm75_init (void);

unsigned char lm75_send_start (void);
void lm75_send_stop (void);
unsigned char lm75_send_add_rw (unsigned char address, unsigned char rw);
unsigned char lm75_send_byte (unsigned char byte);
unsigned char lm75_read_byte (void);
extern unsigned short lm75_get_temperature (unsigned char address);

#endif /* CMLM75_H_ */