/*
 * CMLm75.c
 *
 * Created: 22.03.2015 15:33:00
 *  Author: PC
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "CMLm75.h"
#include "../CMTwi/CMTwi.h"
#include "../CMGlobal.h"
#include <util/delay.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#define twWrite 0
#define twRead 1

#define LM75Adress			0x90
#define LM75BinaryAddress	1001000

float readLM75TemparaturSensor(void)
{
	uint8_t check = 0;
	int8_t LM75ValueLSB = 0;
	int8_t LM75ValueMSB = 0;
	//int16_t LM75Value = 0;
	float LM75WORD = 0.0;
	
	i2cInit();
	i2cStart(LM75Adress | twWrite);
	i2cSend(0x00);
	i2cStop();
	i2cStart(LM75Adress | twRead);
	LM75ValueMSB = i2cReceiveAck();
	LM75ValueLSB = i2cReceiveNack();
	i2cStop();
	
	if (LM75ValueMSB & (1<<8) )
	{
		LM75ValueMSB = (LM75ValueMSB - 256);	
	}
	LM75WORD = LM75ValueMSB;
	
	if (LM75ValueLSB & (1<<7))
	{
		LM75WORD += 0.5;
	}
	
	return LM75WORD;
}