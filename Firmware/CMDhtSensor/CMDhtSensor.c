/*
 * CMDhtSensor.c
 *
 * Created: 29.09.2015 20:39:36
 *  Author: Christian Möllers
 */ 

#include "CMDhtSensor.h"

#if DHT_FLOAT == 1
	int8_t readDhtSensor(float *humidity, float *temperature) {
#elif DHT_FLOAT == 0
	int8_t readDhtSensor(float *humidity, float *temperature) {
#endif
	cli();
	uint8_t x=0, j=0;
	int8_t completeSignal[6];
	uint16_t crcCheck = 0;
		
	DHTSensorPinOutput;
	DHTSensorPinHigh;
	_delay_ms(100);
	DHTSensorPinLow;
	#if DHT_TYPE == DHT_DHT11
	_delay_ms(18);
	#elif DHT_TYPE == DHT_DHT22
	_delay_us(500);
	#endif
	DHTSensorPinInput;
	DHTSensorPinHigh;
	_delay_us(40);
		
	//check start condition 1
	if((DHTSensorPinPressed)) {
		return -1;
	}
	_delay_us(80);
	//check start condition 2
	if(!(DHTSensorPinPressed)) {
		return -1;
	}
	_delay_us(80);
		
	uint16_t timeoutcounter = 0;
	for (j=0; j<5; j++) { //read 5 byte
		uint8_t result=0;
		for(x=0; x<8; x++) {//read every bit
			timeoutcounter = 0;
			while(!(DHTSensorPinPressed)) { //wait for an high input (non blocking)
				timeoutcounter++;
				if(timeoutcounter > DHT_TIMEOUT) {
					return -1; //timeout
				}
			}
			_delay_us(30);
			if(DHTSensorPinPressed) //if input is high after 30 us, get result
			result |= (1<<(7-x));
			timeoutcounter = 0;
			while(DHTSensorPinPressed) { //wait until input get low (non blocking)
				timeoutcounter++;
				if(timeoutcounter > DHT_TIMEOUT) {
					return -1; //timeout
				}
			}
		}
		completeSignal[j] = result;
	}
		
	*humidity = completeSignal[0];
	//humidity = humidity + completeSignal[1];
		
	*temperature = completeSignal[2] -2;
	//temperature = temperature + completeSignal[3];
		
	sei();
	return 0;
}