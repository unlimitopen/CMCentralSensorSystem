/*
 * CMDhtSensor.h
 *
 * Created: 29.09.2015 20:39:57
 *  Author: PC
 */ 


#ifndef CMDHTSENSOR_H_
#define CMDHTSENSOR_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include "../CMGlobal.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#include <util/delay.h>

#define DHTSensorPin			PB1
#define DHTSensorPinRegister	DDRB
#define DHTSensorPinPort		PORTB
#define DHTSensorPinIORegister	PINB
#define DHTSensorPinOutput		DHTSensorPinRegister |= (1<<DHTSensorPin)
#define DHTSensorPinInput		DHTSensorPinRegister &=~ (1<<DHTSensorPin)
#define DHTSensorPinHigh		DHTSensorPinPort |= (1<<DHTSensorPin)
#define DHTSensorPinLow		    DHTSensorPinPort &=~ (1<<DHTSensorPin)
#define DHTSensorPinToggle		DHTSensorPinPort ^= (1<<DHTSensorPin)
#define DHTSensorPinPressed		(DHTSensorPinIORegister&(1<<DHTSensorPin))

//sensor type
#define DHT_DHT11	1
#define DHT_DHT22	2
#define DHT_TYPE	DHT_DHT11

// using float?
#define DHT_FLOAT	0

// define a timeout
#define DHT_TIMEOUT 200

#if DHT_FLOAT == 1
	int8_t readDhtSensor(float *humidity, float *temperature);
#elif DHT_FLOAT == 0
	int8_t readDhtSensor(float *humidity, float *temperature);
#endif

#endif /* CMDHTSENSOR_H_ */