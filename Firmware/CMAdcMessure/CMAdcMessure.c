
#include <avr/io.h>
#include <stdlib.h>

/**
	@brief	adc_messure
	@param 	none
	@return	none
*/
uint16_t adcMessure (uint8_t port)
{
    // a value to measuring the
    // adc pin, if the value is smaller
    // the measuring is noisly.
    uint8_t a=100;
    uint8_t i=a, j=a;
    long int analogwert=0, analogwert1=0, analogwert2=0;
    while(j)
    {
        while(i)
		{
            // activate the adc, with non prescale
			ADCSRA=(1<<ADEN);
			// define the adc port for measuring
			ADMUX=port;
			// use internal reference voltage
			ADMUX |= (1<<REFS1) | (1<<REFS0);
			// single conversion mode on
			ADCSRA |= (1<<ADSC) | (1<<ADPS2) | (1<<ADPS1);
            while (ADCSRA & (1<<ADSC)) {;};
            // wait for finishing for converts
            analogwert+=ADCW;
            i--;
        }
        analogwert1 = analogwert/a;
        analogwert2 += analogwert1;
        j--;
    }
    analogwert=(analogwert2/a);
    return (analogwert);
}

/**
	@brief	getRandomNumber
	@param 	none
	@return	none
*/
uint16_t getRandomNumber(uint8_t port) 
{
    // which channel is used
    ADMUX = port; 
    //Prescaler auf größtmöglichen Wert setzen (128)
    ADCSRA = (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0); 
    
     // activate adc 
    ADCSRA |= (1<<ADEN);

    // first messuring
    ADCSRA |= (1<<ADSC); 
   
    while (ADCSRA & (1<<ADSC)); 
    uint8_t byte1 = ADCL; 
    // second messuring
    ADCSRA |= (1<<ADSC); 
   
    while (ADCSRA & (1<<ADSC)); 
    uint8_t byte2 = ADCL; 
    uint16_t seed = byte1 << 8 | byte2; 

    // deactivate adc 
    ADCSRA &=~ (1<<ADEN);

    return seed; 
}


void setup_seed()
{
 unsigned char oldADMUX = ADMUX;
 ADMUX |=  _BV(MUX0); //choose ADC1 on PB2
 ADCSRA |= _BV(ADPS2) |_BV(ADPS1) |_BV(ADPS0); //set prescaler to max value, 128
 
 ADCSRA |= _BV(ADEN); //enable the ADC
 ADCSRA |= _BV(ADSC);//start conversion
 
 while (ADCSRA & _BV(ADSC)); //wait until the hardware clears the flag. Note semicolon!
 
 unsigned char byte1 = ADCL;
 
 ADCSRA |= _BV(ADSC);//start conversion
 
 while (ADCSRA & _BV(ADSC)); //wait again note semicolon!
 
 unsigned char byte2 = ADCL;
 
 unsigned int seed = byte1 << 8 | byte2;
 
 srand(seed);
 
 ADCSRA &= ~_BV(ADEN); //disable ADC

  ADMUX = oldADMUX;
}
