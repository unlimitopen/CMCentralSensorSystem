/*
 * CMAdcMessure.h
 *
 *  Created on: 24.11.2014
 *      Author: christian
 */

#ifndef CMADCMESSURE_H_
#define CMADCMESSURE_H_

int adcMessure (uint8_t port);
uint16_t getRandomNumber(uint8_t port);

#endif /* CMADCMESSURE_H_ */
