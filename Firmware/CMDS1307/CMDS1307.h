﻿/*
 * CMDS1307.h
 *
 * Created: 04.09.2016 09:16:47
 *  Author: Christian Möllers
 *
 *	Version	maintainer		Release Notes
 *	0.0.1		cm			Initial Release		
 *	0.0.2		cm			insert some function to work with the DS1307 ic
 *						the special is, the ic will not work directly without changing 
 *						the bit 8 of second address 0x00.
 *	0.0.3		cm			insert a date and time function without work
 *	0.0.4		cm			insert clear function for all register and specials to bring
 *						the ic in default mode.
 *	0.0.5		cm			insert the options for the initialization, 12 or 24 hours
 *	0.0.6		cm			define a init step for am and pm definition.
 *	0.0.7		cm			make some corrections for settings oscillator and 12 or 24 hour 
 *						definition.
 */ 

#ifndef CMDS1307_H_
#define CMDS1307_H_

#include "../CMGlobal.h"

#define DS1307Address		0xD0
#define DS1307BinaryAddress	1101000

#define twWrite				0
#define twRead				1

// define the oscillator : active or inactive
#define oscillatoractive    0x80
#define oscillatorinactive  0x80

// define the hour modes : 12 and 24 hours
#define hourMode12          0x40
#define hourMode24			0x40
#define hour24				0
#define hour12				1
#define hourMode			hourMode24

// which status has the clock AM / PM ?
#define hourModePM          0x20
#define PM					1
#define AM					0

/// 00:00 – 12:00 Uhr = AM
/// 12:00 – 00:00 Uhr = PM

// define addresses for the ds1307 ic
#define secondAddress		0x00
#define minuteAddress		0x01
#define hourAddress			0x02
#define weekdayAddress		0x03
#define dateAddress			0x04
#define monthAddress		0x05
#define yearAddress			0x06
#define controlAddress		0x07
#define ramAddress			0x08

//  functions
void initDS1307(void);
void setOscillatorDS1307(uint8_t set);
void setHourModeDS1307(uint8_t set);

void setSecondDS1307(uint8_t second);
uint8_t getSecondDS1307(void);
void clearSecondDS1307(void);

void setMinuteDS1307(uint8_t minute);
uint8_t getMinuteDS1307(void);
void clearMinuteDS1307(void);

void setHourDS1307(uint8_t hour);
uint8_t getHourDS1307(void);
void clearHourDS1307(void);

void setWeekdayDS1307(uint8_t hour);
uint8_t getWeekdayDS1307(void);
void clearWeekdayDS1307(void);

void setMonthDS1307(uint8_t hour);
uint8_t getMonthDS1307(void);
void clearMonthDS1307(void);

void setyearDS1307(uint8_t year);
uint8_t getyearDS1307(void);
void clearYearDS1307(void);

void setdateDS1307(uint8_t date);
uint8_t getDateDS1307(void);
void clearDateDS1307(void);

uint8_t getTimeDS1307(void);
void clearTimeDS1307(void);

void getTimeVariablesFromDS1307(void);
void setTimeVariablesFromDS1307(void);

uint8_t bcdToDecimalOutput(uint8_t zahl);
uint8_t decimalTobcdOutput(uint8_t bcd);

#endif /* CMDS1307_H_ */