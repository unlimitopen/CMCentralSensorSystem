/*
 * CMMasterNode.h
 *
 * Created: 18.04.2015 14:12:50
 *  Author: c.moellers
 */ 

#ifndef CMMASTERNODE_H_
#define CMMASTERNODE_H_


void initNodeProtocol(void);
void sensorQuery(void);
void viewNodeAdress(void);
void defineNodeAddress(char *newNode, char *nodeSerial);
void defineMasterNodeAddress(char *newNode, char *nodeSerial);
//uint16_t makeCrcCheckSum(char *data);
void nodeAttach(void);
uint8_t checkNodeSerialAddress(char *nodeSerialAddress);
void transmitNode(unsigned char *sensorNodeAddress, unsigned char *command);
uint8_t receiveNode(unsigned char *sensorNodeAddress);
void defineOneNode(void);
uint8_t findNodeAdress(char *nodeaddress);
void deleteAllNodeAdress(void);

#endif /* CMMASTERNODE_H_ */