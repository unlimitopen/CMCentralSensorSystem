/*
 * CMMasterNode.c
 *
 * Created: 18.04.2015 14:12:37
 *  Author: christian M�llers
 */
#include <avr/io.h>
#include "CMMasterNode.h"
#include "../CMNodeDataHandle/CMNodeDataHandle.h"
#include "../CMRfm12/RFM12.h"
#include "../CMGlobal.h"
#include "../CMUart/CMUart0.h"
#include "../CMRsAddresses/CMRsAddresses.h"
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/eeprom.h>

extern unsigned char masterNodeAddress[5];
extern unsigned char falseNodeNumber[5];
extern unsigned char usegatewayNumber[5];
extern unsigned char nodeNumber[5];
extern unsigned char nodeBroadcast[5];
extern unsigned char nodeSignIn[5];
extern unsigned char nodeSignOut[5];
extern unsigned char nodeError[5];
extern unsigned char nodeSignOk[5];
extern unsigned char sensorInput[5];
extern unsigned char nodeHello[5];
extern unsigned char nodeInfo[5];
extern unsigned char nodeRestart[5];
extern unsigned char nodeSettings[5];
extern unsigned char nodeAddressNotDefined[5];
extern unsigned char nodeSerialAddress[5];
extern unsigned char nodeTimeSetup[5];
extern unsigned char masterNodeRestart[5];
extern unsigned char masterRsDataOK[5];
extern unsigned char masterRsError[5];

extern unsigned char nodeUpdate[5];

// define the crypt states
extern unsigned char changeCryptKey[5];
extern unsigned char changeCryptKeyCycles[5];
extern unsigned char changeCryptDelta[5];

// define the sensor state "on or off" or more
extern unsigned char sensorNodeShutdownInt[5];
extern unsigned char windowLevelSenorInt[5];
extern unsigned char audioSensorInt[5];
extern unsigned char gasSensorInt[5];
extern unsigned char carbonMonoxideSensorInt[5];
extern unsigned char pirSensorInt[5];
extern unsigned char magentSensorInt[5];


struct nodeProtocolChar
{
	unsigned char masterNodeAddress[5];
	unsigned char falseNodeNumber[5];
	unsigned char usegatewayNumber[5];
	unsigned char nodeNumber[5];
	unsigned char nodeBroadcast[5];
	unsigned char nodeSignIn[5];
	unsigned char nodeSignOut[5];
	unsigned char nodeError[5];
	unsigned char nodeSignOk[5];
	unsigned char sensorInput[5];
	unsigned char nodeHello[5];
	unsigned char nodeInfo[5];
	unsigned char nodeRestart[5];
	unsigned char nodeSettings[5];
	unsigned char nodeAddressNotDefined[5];
	unsigned char nodeSerialAddress[5];

	// define the sensor state "on or off" or more
	unsigned char sensorNodeShutdown[5];
	unsigned char windowLevelSenor[5];
	unsigned char audioSensor[5];
	unsigned char gasSensor[5];
	unsigned char carbonMonoxideSensor[5];
	unsigned char pirSensor[5];
	unsigned char magentSensor[5];
}nodeHeaderChar;

struct nodeProtocol
{
	uint16_t masterNodeAddress;
	uint16_t falseNodeNumber;
	uint16_t usegatewayNumber;
	uint16_t nodeNumber;
	uint16_t nodeBroadcast;
	uint16_t nodeSignIn;
	uint16_t nodeSignOut;
	uint16_t nodeError;
	uint16_t nodeSignOk;
	uint16_t sensorInput;
	uint16_t nodeHello;
	uint16_t nodeInfo;
	uint16_t nodeRestart;
	uint16_t nodeSettings;
	uint16_t nodeAddressNotDefined;
	uint16_t nodeSerialAddress;

	// define the sensor state "on or off" or more
	uint16_t sensorNodeShutdown;
	uint16_t windowLevelSenor;
	uint16_t audioSensor;
	uint16_t gasSensor;
	uint16_t carbonMonoxideSensor;
	uint16_t pirSensor;
	uint16_t magentSensor;
}nodeHeaderInt;

uint8_t	nodeType = 1;

// External variables
extern struct nodeTable node[MAX];
extern struct __eeprom_nodeData node_EEPROM[MAX];

extern volatile uint8_t sec;
extern volatile uint8_t hour;
extern volatile uint8_t min;
extern volatile uint8_t day;
extern volatile uint8_t weekDay;
extern volatile uint8_t month;
extern volatile uint8_t year;
extern volatile uint8_t tick;
extern volatile uint8_t high;
extern volatile uint8_t lowTick;
extern volatile uint8_t low;

unsigned char nodeStatement[8];
//volatile unsigned char nodeData[32];

//volatile unsigned char nodeinfo[1];
//volatile uint8_t jobRequest = FALSE;
//volatile uint8_t security = FALSE;

// is the incomming syncrobytes a internal node?
//uint8_t sensorNodeQuery = FALSE;

// nodeaddress = 22 and new address 23
//TODO: Write nodeaddress into EEprom!!
volatile uint16_t queryCounting = 0;
volatile uint8_t queryChange = 0;

// Where beginn the node addresses
volatile int nodeAddress = 9950;

/**
@brief	none
@param 	none
@return	none
**/
void initNodeProtocol(void)
{
	unsigned char copyChar[5];
	/*
	nodeHeaderInt.masterNodeAddress = masterNodeAddressInt;
	nodeHeaderInt.falseNodeNumber = falseNodeNumberInt;
	nodeHeaderInt.usegatewayNumber = usegatewayNumberInt;
	nodeHeaderInt.nodeNumber = nodeNumberInt;
	nodeHeaderInt.nodeBroadcast = nodeBroadcastInt;
	nodeHeaderInt.nodeSignIn = nodeSignInInt;
	nodeHeaderInt.nodeSignOut = nodeSignOutInt;
	nodeHeaderInt.nodeError = nodeErrorInt;
	nodeHeaderInt.nodeSignOk = nodeSignOkInt;
	nodeHeaderInt.sensorInput = sensorInputInt;
	nodeHeaderInt.nodeHello = nodeHelloInt;
	nodeHeaderInt.nodeInfo = nodeInfoInt;
	nodeHeaderInt.nodeRestart = nodeRestartInt;
	nodeHeaderInt.nodeSettings = nodeSettingsInt;
	nodeHeaderInt.nodeAddressNotDefined = nodeAddressNotDefinedInt;
	nodeHeaderInt.nodeSerialAddress = nodeSerialAddressInt;

	// define the sensor state "on or off" or more
	nodeHeaderInt.sensorNodeShutdown = sensorNodeShutdownInt;
	nodeHeaderInt.windowLevelSenor = windowLevelSenorInt;
	nodeHeaderInt.audioSensor = audioSensorInt;
	nodeHeaderInt.gasSensor = gasSensorInt;
	nodeHeaderInt.carbonMonoxideSensor = carbonMonoxideSensorInt;
	nodeHeaderInt.pirSensor = pirSensorInt;
	nodeHeaderInt.magentSensor = magentSensorInt;
	
	itoa(masterNodeAddressInt, nodeHeaderChar.masterNodeAddress, 10);
	itoa(falseNodeNumberInt ,nodeHeaderChar.falseNodeNumber, 10);
	itoa(usegatewayNumberInt ,nodeHeaderChar.usegatewayNumber, 10);
	itoa(nodeNumberInt ,nodeHeaderChar.nodeNumber, 10);
	itoa(nodeBroadcastInt ,nodeHeaderChar.nodeBroadcast, 10);
	itoa(nodeSignInInt ,nodeHeaderChar.nodeSignIn, 10);
	itoa(nodeSignOutInt ,nodeHeaderChar.nodeSignOut, 10);
	itoa(nodeErrorInt ,nodeHeaderChar.nodeError, 10);
	itoa(nodeSignOkInt ,nodeHeaderChar.nodeSignOk, 10);
	itoa(sensorInputInt ,nodeHeaderChar.sensorInput, 10);
	itoa(nodeHelloInt ,nodeHeaderChar.nodeHello, 10);
	itoa(nodeInfoInt ,nodeHeaderChar.nodeInfo, 10);
	itoa(nodeRestartInt ,nodeHeaderChar.nodeRestart, 10);
	itoa(nodeSettingsInt ,nodeHeaderChar.nodeSettings, 10);
	itoa(nodeAddressNotDefinedInt ,nodeHeaderChar.nodeAddressNotDefined, 10);
	itoa(nodeSerialAddressInt ,nodeHeaderChar.nodeSerialAddress, 10);
	
	// define the sensor state "on or off" or more
	itoa(sensorNodeShutdownInt ,nodeHeaderChar.sensorNodeShutdown, 10);
	itoa(windowLevelSenorInt ,nodeHeaderChar.windowLevelSenor, 10);
	itoa(audioSensorInt ,nodeHeaderChar.audioSensor, 10);
	itoa(gasSensorInt ,nodeHeaderChar.gasSensor, 10);
	itoa(carbonMonoxideSensorInt ,nodeHeaderChar.carbonMonoxideSensor, 10);
	itoa(pirSensorInt ,nodeHeaderChar.pirSensor, 10);
	itoa(magentSensorInt ,nodeHeaderChar.magentSensor, 10);
	*/
}


/**
@brief	none
@param 	none
@return	none
**/
void deleteOneNodeAdress(char *nodeAddress)
{
	uint8_t i = 0;
	for (i=0; i <= MAX; i++)
	{
		strcpy(node[i].address, "0");
	}
}

/**
@brief	none
@param 	none
@return	none
**/
uint16_t findNodeEmptyAdress(void)
{
	uint8_t i = 0;
	uint8_t success = 255;

	for (i=0; i <= MAX; i++)
	{
		if ( (strcmp (node[i].nodeSerial, "0") == 0) && (node[i].count == 999 ) && (node[i].activ == 0) )
		{
			success = i;
			break;
		}
	}
	return success;
}

/**
@brief	none
@param 	none
@return	none
**/
uint8_t findNodeSerialAdress(char *nodeSerialAddress)
{
	uint8_t i = 0;
	uint8_t success = 255;
	char nodeSerialEEprom[10];

	for (i=0; i <= MAX; i++)
	{
		eeprom_read_block(nodeSerialEEprom, node_EEPROM[i].nodeSerial_EEPROM, sizeof(nodeSerialEEprom) );
		
		if ( (strcmp (nodeSerialEEprom, nodeSerialAddress) == FALSE ) )
		{
			strcpy(node[i].nodeSerial, nodeSerialEEprom);
			success = i;
			break;
		}
	}
	return success;
}

/**
@brief	none
@param 	none
@return	none
**/
void deleteAllNodeAdress(void)
{
	uint8_t i = 0;
	char nodeSerialTmp[10];
	char nodeAddressTmp[5];
	
	for (i=0;i <= MAX; i++)
	{
		nodeAddress++;
		eeprom_read_block(nodeSerialTmp, node_EEPROM[i].nodeSerial_EEPROM, sizeof(nodeSerialTmp));
		
		if (checkNodeSerialAddress(nodeSerialTmp) != TRUE)
		{	
			strcpy(node[i].nodeSerial, "0"); // this is the node version :-)
		}
		itoa(nodeAddress, nodeAddressTmp, 10);
		strcpy(node[i].address, nodeAddressTmp); // this is the nodeaddress for node :-)
		strcpy(node[i].nodeType, "0"); // this is the node version :-)	
		strcpy(node[i].nodeVersion, "0"); // this is the node version :-)	
		strcpy(node[i].securityState, "0"); // this is the node version :-)	
		strcpy(node[i].gatewayAddress, "0"); // if node need a node gateway 
		strcpy(node[i].temperatur, "0"); // temperatur from lm75 or anything else
		strcpy(node[i].temperatur2, "0"); // temperatur from ds18s20 or anything else
		strcpy(node[i].temperatur3, "0"); // temperatur from ds18s20 or anything else
		strcpy(node[i].airpressure, "0"); // pressure in hPa
		strcpy(node[i].humidity, "0"); // humidity in %
		strcpy(node[i].battery, "0"); // battery volt
		strcpy(node[i].lightIntense, "0"); // adc measure
		strcpy(node[i].rfidKey, "0"); //  RFID key
		strcpy(node[i].rfidStatus, "0"); // 0 or 1
		strcpy(node[i].pirSensor, "0"); // active or not active
		strcpy(node[i].doorBell, "0"); // active or not active
		strcpy(node[i].audioSensor, "0"); // active or not active
		strcpy(node[i].gasSensor, "0"); // active or not active
		strcpy(node[i].carbonMonoxideSensor, "0"); // active or not active
		strcpy(node[i].radiationSensorCPS, "0"); // Counts per Second
		strcpy(node[i].radiationSensorCPM, "0"); // Counts per Minute
		strcpy(node[i].radiationSensorSV, "0"); // � Sievert
		strcpy(node[i].windowLevelDirectionSensor, "0"); // direction from window level = OPEN, CLOSE, ON TILT,
		strcpy(node[i].nodeVersion, "0"); // if node need a node gateway 
		strcpy(node[i].alarm, "0");
		node[i].activ = 0;
		node[i].count = 999;
	}
}

/**
@brief	none
@param 	none
@return	none
**/
void viewNodeAdress(void)
{
	uint8_t i = 0;
	char data[5];
	char nodeSerialTmp[10];

	for (i=0; i <= MAX; i++)
	{
		eeprom_read_block(nodeSerialTmp, node_EEPROM[i].nodeSerial_EEPROM, sizeof(nodeSerialTmp));
		//TODO: make correct informations
		if ( ( strlen(node[i].address) ) >= 3  )
		{
			if (checkNodeSerialAddress(nodeSerialTmp) == TRUE)
			{
				strcpy(node[i].nodeSerial, nodeSerialTmp); // this is the nodeaddress for node :-)
			}
			USART_print(node[i].address);
			USART_print(",");
			USART_print(node[i].nodeSerial);
			USART_print(",");
			USART_print(node[i].nodeType);
			USART_print(",");
			USART_print(node[i].securityState);
			USART_print(",");
			USART_print(node[i].nodeVersion);
			USART_print(",");
			USART_print(node[i].temperatur);
			USART_print(",");
			USART_print(node[i].temperatur2);
			USART_print(",");
			USART_print(node[i].temperatur3);
			USART_print(",");
			USART_print(node[i].airpressure);
			USART_print(",");
			USART_print(node[i].humidity);
			USART_print(",");
			itoa(node[i].activ, data, 10);
			USART_print(data);
			USART_print(",");
			itoa(node[i].count, data, 10);
			USART_print(data);
			USART_print(",");;
			USART_print(node[i].alarm);
			USART_print(",");
			itoa(i, data, 10);
			USART_print(data);
			USART_print("\n");
		}
	}
}


/**
@brief	none
@param 	none
@return	none
**/
uint8_t findNodeAdress(char *nodeaddress)
{
	uint8_t i = 0;
	uint8_t success = FALSE;

	for (i=0; i <= MAX; i++)
	{
		if ( (strcmp (node[i].address, nodeaddress) ) == 0 )
		{
			success = TRUE;
			break;
		}
	}
	return success;
}

/**
@brief	define a node address for the network node.
@param 	none
@return	none
*/
void defineMasterNodeAddress(char *newNode, char *nodeSerial)
{
	uint8_t i = 0, activ = 1;
	strcpy(node[i].address, newNode);
	eeprom_update_block( nodeSerial, node_EEPROM[i].nodeSerial_EEPROM, 10 );
	node[i].activ = activ;
	node[i].count = queryCounting;
}


void defineNodeAddress(char *newNode, char *nodeSerial)
{
	uint8_t activ = 1, count = 0;
	char infoptr[10];

	if ( (checkNodeSerialAddress(nodeSerial) == TRUE ) )
	{
		count = findNodeSerialAdress(nodeSerial);
		if (count <= MAX)
		{
			strcpy(newNode, node[count].address);
			strcpy(node[count].nodeSerial, nodeSerial);
			node[count].activ = activ;
			node[count].count = queryCounting;
		}
		else
		{
			count = findNodeEmptyAdress();
			if ( count <= MAX )
			{
				strcpy(newNode, node[count].address);
				strcpy(node[count].nodeSerial, nodeSerial);
				eeprom_update_block(node[count].nodeSerial, node_EEPROM[count].nodeSerial_EEPROM, sizeof(node[count].nodeSerial));
				node[count].activ = activ;
				node[count].count = queryCounting;
			}
		}
	}
}

/**
@brief	This function is used to speek with a sensor node that have not reported 
		in a time window or we want information about the node config or anything else.
@param 	none
@return	none
**/
void transmitNode(unsigned char *sensorNodeAddress, unsigned char *command)
{
	uint8_t success = FALSE;
	//unsigned char nodeStatement[8];
	// 
	rfm12TransmitData(nodeHello,8);
	rfm12TransmitData(sensorNodeAddress,8);
	rfm12ReceiveData(nodeStatement,8);
	if (strcmp( nodeStatement, nodeHello) == 0 )
	{
		// In this case we can only send to all nodes
		// but we want to send only to the central system or
		// to one node... we need to think about broadcast networking.
		// in the first step, all nodes become a hello, in the second step
		// the nodes become a node address, that is the point, that the nodes
		// can decisive is that information for me or for other node.
	}
}

/**
@brief	none
@param 	none
@return	none
**/
uint8_t receiveNode(unsigned char *sensorNodeAddress)
{
	uint8_t crcCheck = FALSE;
	uint8_t nodeAddressCheck = FALSE;
	char data[DataSize];
	uint16_t counterCheck = 0;
	char nodeSerialAddressTmp[10];
	uint8_t success = FALSE;
	uint8_t sensorNodeQuery = FALSE;

	rfm12ReceiveDataISR(nodeStatement,8);
	//rfm12ReceiveData(nodeStatement, 5);
	#if (printDebugging == TRUE)
		printf(">>XTEA: %s\n", nodeStatement);
	#endif
	
	if (strcmp(nodeStatement, nodeHello) == 0)
	{
		//USART_print("NODESIGNIN \n");
		// In this case we can only send to all nodes
		// but we want to send only to the central system or
		// to one node... we need to think about broadcast networking.
		// in the first step, all nodes become a hello, in the second step
		// the nodes become a node address, that is the point, that the nodes
		// can decisive is that information for me or for other node.

		// FIXMEE: Please query the node address we can need that for security!
		//getNewNodeSessionID(nodeStatement);
		//rfm12TransmitData(nodeStatement, 8);
		
		rfm12TransmitData(nodeSignOk, 8);
		rfm12ReceiveData(nodeStatement, 8);
		
		// check nodesessionid
		// if (nodeStatement )

		// FIXMEE: Please query the nodeaddresss we can need that for security!
		// if ( nodeIsBlocking = checkNodeBlocking(nodestatement)
		//{
		if (strcmp (nodeStatement, masterNodeAddress) == 0 )
		{
			rfm12TransmitData(nodeSignOk, 8);
			rfm12ReceiveData(nodeStatement, 8);
			
			if (strcmp(nodeStatement, nodeSerialAddress) == 0)
			{
				rfm12TransmitData(nodeSignOk, 8);
				rfm12ReceiveData(nodeSerialAddressTmp, 10);
				
				success = checkNodeSerialAddress(nodeSerialAddressTmp);
				
				if (success == TRUE)
				{
					rfm12TransmitData(nodeSignOk, 8);
					rfm12ReceiveData(nodeStatement, 8);
				
					if (strcmp (nodeStatement, nodeSignIn) == 0)
					{
						rfm12TransmitData(nodeSignOk, 8);
						rfm12ReceiveData(nodeStatement, 8);
						if (strcmp(nodeStatement, nodeNumber) == 0)
						{
							defineNodeAddress(nodeStatement, nodeSerialAddressTmp);
							rfm12TransmitData(nodeStatement,8);
							strncpy(sensorNodeAddress, nodeStatement, 5);
						}
						// we have a internal node query!
						sensorNodeQuery = TRUE;
					}
				}
			}
			else if (strcmp (nodeStatement, sensorInput) == 0)
			{
				rfm12TransmitData(nodeSignOk, 8);
				rfm12ReceiveData(nodeStatement, 8);
				nodeAddressCheck = findNodeAdress(nodeStatement);
				strncpy(sensorNodeAddress, nodeStatement, 5);
				if (nodeAddressCheck == TRUE)
				{
					rfm12TransmitData(nodeSignOk, 8);
					rfm12ReceiveData(data, DataSize);
					crcCheck = makeDataUpdate(data);
					if (crcCheck == TRUE)
					{
						rfm12TransmitData(nodeSignOk, 8);
						sensorNodeQuery = TRUE;
					}
					else
					{
						rfm12TransmitData(nodeError,8);
						sensorNodeQuery = FALSE;
						// FIXMEE: define the session id and node as blocked!!
					}
					data[0] = '\0';
				}
				else
				{
					rfm12TransmitData(nodeAddressNotDefined, 8);
					sensorNodeQuery = FALSE;
				}
			}
			else if (strcmp (nodeStatement, nodeTimeSetup) == 0)
			{
				// todo give the node the correct time.
			}
		}
	}
	else
	{
		nodeStatement[0] = '\0';
		sensorNodeQuery = FALSE;
	}
	// Very important, reacativation after the steps to define the interrupt rfm12 mode
	rfm12CommunicationDirection(RXIntMode);
	
	return sensorNodeQuery;
}