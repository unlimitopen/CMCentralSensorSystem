/*
 * CMUart1.h
 *
 *  Created on: 24.11.2014
 *      Author: c.moellers
 */

#ifndef CMUART1_H_
#define CMUART1_H_

#define USART_BAUD_RATE_1     19200UL
#define USART_BAUD_CALC_1     (F_CPU/(USART_BAUD_RATE_1*16L)-1)

// Lowbyte
#define LOW_1(x)              ((x) & 0xFF)
#define HIGH_1(x)             (((x) >> 8) & 0xFF)

#ifndef DataSize
#define DataSize 			50
#endif

#ifndef MAX
#define MAX		 			10
#endif

void USART1_init(void);
void USART1_transmit(const uint8_t data);
void USART1_print(char *data);

#endif /* CMUART1_H_ */
