/*
 * CMUart.h
 *
 *  Created on: 24.11.2014
 *      Author: c.moellers
 */

#ifndef CMUART_H_
#define CMUART_H_

#include <stdio.h>

#define USART_BAUD_RATE     19200UL
#define USART_BAUD_CALC     (F_CPU/(USART_BAUD_RATE*16L)-1)

// Lowbyte
#define LOW(x)              ((x) & 0xFF)
#define HIGH(x)             (((x) >> 8) & 0xFF)

#ifndef DataSize
#define DataSize 	50
#endif

#ifndef MAX
#define MAX		 	10
#endif

#ifndef TRUE
#define TRUE		1
#endif

#ifndef FALSE
#define FALSE		0
#endif

#ifndef RXMode
#define RXMode		1
#endif

#ifdef TXMode
#define TXMode		0
#endif

#ifndef PRINTDEBUG
	#define PRINTDEBUG	1
#endif

void USART_init(void);
void USART_transmit(const uint8_t data);
void USART_print(unsigned char *data);
uint8_t getUARTString( unsigned char *string);
uint8_t usart_getc_intr(void);
uint8_t usart_gets_intr(unsigned char *newPuffer);
void USARTFlush(void);
uint8_t ifrxBufferStringAviable(unsigned char *string , unsigned char *string_ret);
char *parsingStringElement(unsigned char *deleteElement, unsigned char *string, unsigned char *stringBack);
void uart_putchar(unsigned char c, FILE *stream);
void delLFCRString(unsigned char *string, unsigned char *string_ret);
uint8_t usart1_string_avail_intr(void);

#endif /* CMUART_H_ */
