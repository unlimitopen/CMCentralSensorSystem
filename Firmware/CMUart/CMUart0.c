/*
 * CMUart.c
 *
 *  Created on: 24.11.2014
 *      Author: c.moellers
 */

#include <avr/io.h>
#include "CMUart0.h"
#include "../CMGlobal.h"
#include <util/delay.h>
#include <stdio.h>

#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

volatile unsigned char rxBuffer[100];
volatile unsigned char rxBuffer2[DataSize];
volatile uint8_t rxWritePosition = 0;
volatile uint8_t rxReadPosition = 0;
unsigned char *rxBufferPtr;
unsigned char *rxBuffer2Ptr;
volatile unsigned char *lesezeiger = 0;
volatile unsigned char *schreibzeiger = 0;
volatile uint8_t rsUart0Success = 0;

#ifdef PRINTDEBUG
	static FILE printOutput = FDEV_SETUP_STREAM (uart_putchar, NULL, _FDEV_SETUP_WRITE);
#endif


/**
	@brief	Print the data to USART
	@param 	data
	@return	none
*/
ISR(USART0_RX_vect)
{
    *schreibzeiger=UDR0;
    schreibzeiger++;
    if(schreibzeiger==rxBuffer+100)
    {
		schreibzeiger=rxBuffer;
	}
	
	rsUart0Success = TRUE;
}

/**
	@brief	Initialize the UART port
	@param 	none
	@return	none
*/
void USART_init(void)
{
	//#ifdef PRINTDEBUG
	// very important for streaming into STDOUT
		stdout = &printOutput;
	//#endif
	
	lesezeiger=rxBuffer;
	schreibzeiger=rxBuffer;
	
    //UBRRH = HIGH(USART_BAUD_CALC);
    UBRR0H = HIGH(USART_BAUD_CALC);
    UBRR0L = LOW(USART_BAUD_CALC);

    UCSR0B = (1<<RXEN0) | (1<<TXEN0) | (1<<RXCIE0);
    //UCSRC = (1<<UCSZ0) | (1<<UCSZ0);
    UCSR0C = (1<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00);
    //UCSRC = (1<<USBS)|(1<<UCSZ1)|(1<<UCSZ0);

	USARTFlush();
}



/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart_byte_avail_intr(void)
{
	if(schreibzeiger!=lesezeiger)
	{
		return 1;		
	}
	else
	{
		return 0;
	}
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart_string_avail_intr(void)
{
	uint8_t avail=0;
	uint8_t *r=lesezeiger;
		
	while(r!=schreibzeiger)
	{
		if(*r == '\r') 
		{
			avail = 1;
		}
		r++; 
		if(r==rxBuffer+100) 
		{
			r=rxBuffer;
		}
	}
	return avail;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart_getc_intr(void)
{
	uint8_t datenbyte=0;

	if(schreibzeiger!=lesezeiger)
	{
		datenbyte=*lesezeiger;
		lesezeiger++;
		if(lesezeiger==rxBuffer+100) 
		{
			lesezeiger=rxBuffer;
		}	
	}
	return datenbyte;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart_gets_intr(unsigned char *newPuffer)
{
	unsigned char sData[100];
	unsigned char *s = sData;
	unsigned char c;
	uint8_t success = 0;

	do 
	{
		if(usart_string_avail_intr())
		{
			success = 1;
			do
			{
				c = usart_getc_intr();
				*s++=c;
			}while(c!=0);
		
			delLFCRString(sData, newPuffer);
		}
	}while((success != TRUE));

	return success;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart0_byte_avail(void)
{
	if (lesezeiger != schreibzeiger)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


/**
	@brief	Flush the receive udr register
	@param 	none
	@return	none
*/
void USARTFlush(void)
{
	uint8_t uc_Data;

	while(UCSR0A&(1<<RXC0))
	{
		uc_Data = UDR0;
	}
}

void uart_putchar(unsigned char c, FILE *stream)
{
	/*
	if (c == '\n')
	{
		uart_putchar('\r', stream);
	}
	*/
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
}

/**
	@brief	Transmit
	@param 	data
	@return	none
*/
void USART_transmit(const uint8_t data)
{
    // Warten bis Datenregister leer
    while (!(UCSR0A &(1<<UDRE0)));
    // sende 8 Bit daten
    UDR0 = data;
}

/**
	@brief	Receive data from USART
	@param 	data
	@return	none
*/
void USART_receive(unsigned char *data)
{
	// sende 8 Bit daten
	data = UDR0;
	// Warten bis Datenregister leer
	while (!(UCSR0A &(1<<UDRE0)));

}

/**
	@brief	Print the data to USART
	@param 	data
	@return	none
*/
void USART_print(unsigned char *data)
{
    // Ende String
    while (*data != '\0')
    {
        USART_transmit(*data++);
    }
}

// TODO We need a big change for AVR Controllers
void delLFCRString(unsigned char *string, unsigned char *string_ret)
{
	uint8_t e=0, i=0;
	for(;;)
	{
		if(string[i] != '\n' && string[i] != '\r' )
		{
			string_ret[e] = string[i];
			e++;
		}
		if(string[i] == '\0')
		{
			string_ret[e] = '\0';
			break;
		}
		i++;
	}
}


uint8_t changeLFCRString(unsigned char *string)
{
	uint8_t success = FALSE;
	uint8_t i = 0;
	uint8_t arraySize = 0;

	arraySize = sizeof(string);

	for ( i = 0; i <= arraySize; i++ )
	{
		if ( string[i] == '\n' || string[i] == '\r' )
		{
			string[i] = '\0';
			success = TRUE;
		}
	}
	return success;
}


/**
	@brief	Check if string
	@param 	data
	@return	none
*/
char *parsingStringElement(unsigned char *deleteElement, unsigned char *string, unsigned char *stringBack)
{
	uint8_t i = 0;
	uint8_t e = 0;
	uint8_t z = 0;

	for ( i = 0; i <= strlen(string); i++ )
	{
		if ( deleteElement[z] != string[i] && string[i] != 'O' && string[i] != 'K' )
		{
			stringBack[e++] = string[i];
		}
		else
		{
			z++;
		}
	}
	stringBack[e] = '\0';

	return stringBack;
}

/**
	@brief	Check if string
	@param 	data
	@return	none
*/
uint8_t ifrxBufferStringAviable(unsigned char *string, unsigned char *string_ret)
{
	uint8_t success = FALSE;
	uint8_t i = 0;
	uint8_t e = 0;

	if (rxReadPosition != rxWritePosition)
	{
		for ( i = 0; i <= strlen(string); i++ )
		{
			if ( string[i] != '\n' && string[i] != '\r')
			{
				string_ret[e++] = string[i];
			}
		}
	}

	if (i >= strlen(string))
	{
		string_ret[e] = '\0';
		success = TRUE;
	}
	return success;
}

/**
	@brief	Print the data to USART
	@param 	data
	@return	none
*/
uint8_t getUARTString(unsigned char *string){

	uint8_t stringBuffer = FALSE;
	uint8_t success = FALSE;
	//char data[5];
	stringBuffer = ifrxBufferStringAviable(rxBuffer, rxBuffer2);
	if( stringBuffer == TRUE)
	{
		while( rxReadPosition != rxWritePosition)
		{
			string[rxReadPosition] = rxBuffer2[rxReadPosition];
			rxReadPosition++;
			success = TRUE;
		}

		if (success == TRUE)
		{
			rxWritePosition = 0;
			rxReadPosition = 0;
		}
	}
	return success;
}