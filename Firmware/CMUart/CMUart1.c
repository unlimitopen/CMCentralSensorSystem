/*
 * CMUart1.c
 *
 *  Created on: 24.11.2014
 *      Author: c.moellers
 */

#include <avr/io.h>
#include "CMUart1.h"
#include "../CMGlobal.h"
#include <util/delay.h>

const char ringBuffer;
unsigned char puffer[DataSize];
char puffer2[DataSize];
char *information;
unsigned char *lesezeiger;
unsigned char *schreibzeiger;

/**
	@brief	Initialize the UART port
	@param 	none
	@return	none
*/
void USART1_init(void)
{
	uint8_t uc_Data;
    //UBRRH = HIGH(USART_BAUD_CALC);
    UBRR1H = HIGH_1(USART_BAUD_CALC_1);
    UBRR1L = LOW_1(USART_BAUD_CALC_1);

    UCSR1B = (1<<RXEN1) | (1<<TXEN1) | (1<<RXCIE1);
    //UCSRC = (1<<UCSZ0) | (1<<UCSZ0);
    UCSR1C = (1<<USBS1)|(1<<UCSZ11)|(1<<UCSZ10);
    //UCSRC = (1<<USBS)|(1<<UCSZ1)|(1<<UCSZ0);

	while(UCSR1A&(1<<RXC1))
	{
		uc_Data = UDR1;
	}
}

/**
	@brief	Transmit
	@param 	data
	@return	none
*/
void USART1_transmit(const uint8_t data)
{
    // Warten bis Datenregister leer
    while (!(UCSR1A &(1<<UDRE1)));
    // sende 8 Bit daten
    UDR1 = data;
}

/**
	@brief	Receive data from USART
	@param 	data
	@return	none
*/
void USART1_receive(const uint8_t data)
{

}

/**
	@brief	Print the data to USART
	@param 	data
	@return	none
*/
void USART1_print(char *data)
{
    // Ende String
    while (*data != '\0')
    {
        USART1_transmit(*data++);
    }
}

/**
	@brief	Print the data to USART
	@param 	data
	@return	none
*/

/*
ISR(USART1_RXC_vect)
{
    *schreibzeiger=UDR0;
    schreibzeiger++;
    if(schreibzeiger==puffer+DataSize)
    {
		schreibzeiger=puffer;
	}
}
*/

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart1_string_avail_intr(void)
{
	//Hilfsfunktion f�r die Funktion usart_gets_intr
	//Pr�fen, ob ein kompletter String im Puffer verf�gbar ist.
	//R�ckgabewert 1 = verf�gbar, 0 = nicht verf�gbar
	uint8_t avail=0;
	uint8_t *r=lesezeiger;
	while(r!=schreibzeiger)
	{
		if(*r=='\r')
		//if(*r==puffer[MAX])
		{
			avail=1;
		}
		r++;
		if(r==puffer+DataSize)
		{
			r=puffer;
		}
	}
	return avail;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart1_byte_avail(void)
{
	_delay_ms(10);
	if (lesezeiger != schreibzeiger)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void usart1_putc(uint8_t byte)
{
	//warten auf Datenregister empty
	while(!(UCSR1A&(1<<UDRE1)));
	//Ein Byte senden
	UDR1=byte;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void usart1_puts(char *s)
//Einen String mit Endmarke 0 senden
{
    while (*s!= '\0')
	{
        usart1_putc(*s);
		s++;
    }
    //usart_putc(0); //Endmarke 0 wird �bertragen!
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart1_getc_intr(void)
{
	//Liest ein Byte aus dem Puffer
	//Nur aufrufen, wenn usart_byte_avail_intr() vorher aufgerufen wurde und
	//eine 1 lieferte.
	uint8_t datenbyte=0;

	if(schreibzeiger!=lesezeiger)
	{
		datenbyte=*lesezeiger;
		lesezeiger++;
		if(lesezeiger==puffer+DataSize)
		{
			lesezeiger=puffer;
		}
	}
	return datenbyte;
}



/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t usart1_gets_intr(char *s)
{
	//Liest  eine Zeichenkette aus dem Puffer
	//einschlie�lich Endmarke 0 in die Stringvariable s
	//Wenn der R�ckgabewert ok = 1 ist, war das Lesen erfolgreich,
	//bei ok = 0 nicht erfolgreich.
	char c;
	uint8_t ok=0;
	//uint8_t i=0;

	if(usart_string_avail_intr())
	{
		ok=1;
		do
		{
			c = usart1_getc_intr();
			*s=c;
			s++;
		}while(c!=0);
		//}while(c!=puffer[max-1]);
	}
	// TODO Check if we have a character

	if ( ok == 1)
	{
		//LED_TWO_HI;
		usart1_puts(information);
		information = puffer2;
		// Setting up the pointer back
		// to starting point
		//lesezeiger = puffer;
		//schreibzeiger = puffer;
		//_delay_ms(800);
	}
	else
	{
		// TODO give us an return code

	}
	return ok;
}
