﻿/*
 * CMNodeDefault.c
 *
 * Created: 10.04.2017 22:56:22
 *  Author: Christian Möllers
 */ 
#include <avr/io.h>
#include "../CMGlobal.h"
#include "CMNodeDefaults.h"

// Menustructure
typedef const struct nodestructure
{
	const __flash char *nodeName;
	const __flash char *nodeDescription;
	uint8_t nodeType;
	
	int8_t airpressureAlarm;
	int8_t airpressureSensor;	
	int airpressureWatermark;
	float airpressureover;
	float airpressureunder;	
	
	int8_t audioSensor;
	int8_t audioSensorAlarm;
	uint8_t audioSensorWatermark;
	uint8_t audioSensorover;
	uint8_t audioSensorunder;
	
	int8_t batterySensor;
	int8_t batterySensorAlarm;
	uint8_t batterySensorWatermark;
	uint8_t batteryover;
	uint8_t batteryunder;
	
	int8_t carbonMonoxideAlarm;
	int8_t carbonMonoxideSensor;
	unsigned int carbonMonoxideSensorWatermark;
	unsigned int carbonMonoxideover;
	unsigned int carbonMonoxideunder;
	
	int8_t gasSensor;
	int8_t gasSensorAlarm;
	unsigned int gasSensorWatermark;
	unsigned int gasover;
	unsigned int gasunder;
	
	int8_t humidityAlarm;
	int8_t humiditySensor;
	uint8_t humidityWatermark;
	uint8_t humidityover;
	uint8_t humidityunder;
	
	int8_t ledSetup;
	
	int8_t lightintenseSensor;
	int8_t lightsenseAlarm;
	unsigned int lightsenseWatermark;
	unsigned int lightsenseover;
	unsigned int lightsenseunder;
	
	int8_t pirSensor;
	int8_t pirSensorAlarm;
	uint8_t pirSensorWatermark;
	uint8_t pirSensorover;
	uint8_t pirSensorunder;
	
	int8_t radiationSensorCPM;
	int8_t radiationSensorCPMAlarm;
	uint8_t radiationSensorCPMWatermark;
	uint8_t radiationSensorCPMover;
	uint8_t radiationSensorCPMunder;
	
	int8_t radiationSensorCPS;
	int8_t radiationSensorCPSAlarm;
	uint8_t radiationSensorCPSWatermark;
	uint8_t radiationSensorCPSover;
	uint8_t radiationSensorCPSunder;
	
	int8_t radiationSensorSV;
	int8_t radiationSensorSVAlarm;
	uint8_t radiationSensorSVWatermark;
	uint8_t radiationSensorSVover;
	uint8_t radiationSensorSVunder;
	
	int8_t rfidkeyAlarm;
	int8_t rfidkeySensor;
	uint8_t rfidkeyWatermark;
	uint8_t rfidkeyover;
	uint8_t rfidkeyunder;
	
	int8_t tempSensor1;
	int8_t tempSensor2;
	uint8_t tempSensor3;
	int8_t temparaturAlarm1;
	int8_t temparaturAlarm2;
	uint8_t temparaturAlarm3;
	uint8_t temparaturWatermark1;
	uint8_t temparaturWatermark2;
	uint8_t temparaturWatermark3;
	uint8_t temperaturover1;
	uint8_t temperaturover2;
	uint8_t temperaturover3;
	uint8_t temperaturunder1;
	uint8_t temperaturunder2;
	uint8_t temperaturunder3;
	
	int8_t doorBellSensor;
	int8_t doorBellSensorAlarm;
	uint8_t doorBellSensorWatermark;
	uint8_t doorBellSensorover;
	uint8_t doorBellSensorunder;
	
	int8_t windowLevelDirectionSensor;
	int8_t windowLevelDirectionSensorAlarm;
	uint8_t windowLevelDirectionSensorWatermark;
	uint8_t windowLevelDirectionover;
	uint8_t windowLevelDirectionunder;
	
	int8_t nodeactive;
}nodeDefinition;

/**
	@brief	none
	@param 	none
	@return	none
*/
const __flash char node_000[]  = "Central";				// 0
const __flash char node_001[]  = "SensorNode";  		// 1
const __flash char node_002[]  = "AlarmClock";			// 2
const __flash char node_003[]  = "WindowLevelSensor";	// 3
const __flash char node_004[]  = "ShutterControl";		// 4
const __flash char node_005[]  = "CarAlarm";			// 5
const __flash char node_006[]  = "PowerStrip";			// 6
const __flash char node_007[]  = "RFID-DoorNode";		// 7
const __flash char node_008[]  = "Placeholder2";		// 8
const __flash char node_009[]  = "Placeholder3";		// 9
const __flash char node_010[]  = "Placeholder4";  		// 10
const __flash char node_011[]  = "Placeholder4";  		// 10

/**
	@brief	none
	@param 	none
	@return	none
*/
const __flash char nodeDescription_000[]  = "Central";			// 0
const __flash char nodeDescription_001[]  = "SensorNode";  		// 1
const __flash char nodeDescription_002[]  = "AlarmClock";		// 2
const __flash char nodeDescription_003[]  = "WindowLevelSensor";// 3
const __flash char nodeDescription_004[]  = "ShutterControl";	// 4
const __flash char nodeDescription_005[]  = "CarAlarm";			// 5
const __flash char nodeDescription_006[]  = "PowerStrip";		// 6
const __flash char nodeDescription_007[]  = "RFID-DoorNode";	// 7
const __flash char nodeDescription_008[]  = "Placeholder2";		// 8
const __flash char nodeDescription_009[]  = "Placeholder3";		// 9
const __flash char nodeDescription_010[]  = "Placeholder4";  	// 10
const __flash char nodeDescription_011[]  = "Placeholder4";  	// 10


/**
	@brief	none
	@param 	none
	@return	none
*/
const __flash nodeDefinition menu[] =
{
	{node_000, nodeDescription_000, 1, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 0
	{node_001, nodeDescription_001, 2, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 1
	{node_002, nodeDescription_002, 3, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 2
	{node_003, nodeDescription_003, 4, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 3
	{node_004, nodeDescription_004, 5, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 4
	{node_005, nodeDescription_005, 6, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 5
	{node_006, nodeDescription_006, 7, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 6
	{node_007, nodeDescription_007, 8, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 7
	{node_008, nodeDescription_008, 9, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 8
	{node_009, nodeDescription_009, 10, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 9
	{node_010, nodeDescription_010, 11, 1, 1, 255, 1200.0, 980.0, 1, 1, 1, 2, 0, 1, 1, 2, 6, 1, 1, 1, 250, 500, 0, 1, 1, 255, 800, 0, 1, 1, 20, 80, 40, 1, 1, 1, 350, 1240, 0, 1, 1, 1, 2, 0, -1, -1, 10, 20, 0, -1, -1, 10, 50, 0, -1, -1, 10, 50, 0, -1, -1, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 25, 25, 25, 5, 5, 5, -1, -1, 1, 1, 0, -1, -1, 1, 2, 0, 1 },   // 10
};




