﻿/*
 * CMNodeDataHandle.c
 *
 * Created: 16.01.2017 20:55:44
 *  Author: Christian Möllers
 */

#include <avr/io.h>
#include <avr/eeprom.h>
#include "CMNodeDataHandle.h"
#include "../CMEEprom/CMEEprom.h"
#include "../CMUart/CMUart0.h"
#include "../CMMasterNode/CMMasterNode.h"
#include "../CMDcf/CMDcf.h"
#include "../CMGlobal.h"
#include "../CMRsAddresses/CMRsAddresses.h"

extern unsigned char masterRsDataOK[5];
extern unsigned char masterRsError[5];
extern unsigned char masterRsGetNodeConfig[5];
extern unsigned char masterRsGetWatermarkConfig[8];
extern unsigned char masterRsGetHello[8];
extern unsigned char masterRsTimeSetup[5];

extern volatile int nodeAddress;
extern volatile uint16_t queryCounting;
extern volatile uint8_t queryChange;

extern volatile uint8_t sec;
extern volatile uint8_t hour;
extern volatile uint8_t min;
extern volatile uint8_t day;
extern volatile uint8_t weekDay;
extern volatile uint8_t month;
extern volatile uint8_t year;
extern volatile uint8_t tick;
extern volatile uint8_t high;
extern volatile uint8_t lowTick;
extern volatile uint8_t low;

// watermark flag variables
uint8_t nodeSerialWatermark = 1;
uint8_t nodeTypeWatermark = 1;
uint8_t securityStateWatermark = 1;
uint8_t nodeVersionWatermark = 1;
uint8_t temparaturWatermark = 2;
uint16_t lightsenseWatermark = 350;
uint8_t batteryWatermark = 2;
uint8_t alarmWatermark = 1;
uint8_t humidityWatermark = 20;
uint8_t airpressureWatermark = 255;
uint8_t pirSensorWatermark = 1; // active or not active
uint8_t doorBellWatermark = 1; // active or not active
uint8_t audioSensorWatermark = 1; // active or not active
uint8_t gasSensorWatermark = 255; // active or not active
uint8_t carbonMonoxideSensorWatermark = 255; // active or not active
uint8_t radiationSensorCPSWatermark = 10; // Counts per Second
uint8_t radiationSensorCPMWatermark = 10; // Counts per Minute
uint8_t radiationSensorSVWatermark = 15; // µ Sievert
uint8_t windowLevelDirectionSensorWatermark = 1; // direction from window level = OPEN = 1 , CLOSE = 0, ON TILT = 2,

/**
@brief	none
@param 	none
@return	none
**/
void initNodeDataHandler(void)
{
	
}

/**
@brief	none
@param 	none
@return	none
**/
void readNodeStructWatermarksFromEEPROM(void)
{
	uint8_t i = 0;
	// watermark flag variables
	updateEEpromByte(node_EEPROM[i].nodeSerialWatermark, nodeSerialWatermark);
	updateEEpromByte(node_EEPROM[i].nodeTypeWatermark, nodeTypeWatermark);
	updateEEpromByte(node_EEPROM[i].securityStateWatermark, securityStateWatermark);
	updateEEpromByte(node_EEPROM[i].nodeVersionWatermark, nodeVersionWatermark);
	updateEEpromByte(node_EEPROM[i].temparaturWatermark, temparaturWatermark);
	updateEEpromWord(node_EEPROM[i].lightsenseWatermark, lightsenseWatermark);
	updateEEpromByte(node_EEPROM[i].batteryWatermark, batteryWatermark);
	updateEEpromByte(node_EEPROM[i].alarmWatermark, alarmWatermark);
	updateEEpromByte(node_EEPROM[i].humidityWatermark, humidityWatermark);
	updateEEpromByte(node_EEPROM[i].airpressureWatermark, airpressureWatermark);
	updateEEpromByte(node_EEPROM[i].pirSensorWatermark, pirSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].doorBellWatermark, doorBellWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].audioSensorWatermark, audioSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].gasSensorWatermark, gasSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].carbonMonoxideSensorWatermark, carbonMonoxideSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].radiationSensorCPSWatermark, radiationSensorCPSWatermark); // Counts per Second
	updateEEpromByte(node_EEPROM[i].radiationSensorCPMWatermark, radiationSensorCPMWatermark); // Counts per Minute
	updateEEpromByte(node_EEPROM[i].radiationSensorSVWatermark, radiationSensorSVWatermark); // µ Sievert
	updateEEpromByte(node_EEPROM[i].windowLevelDirectionSensorWatermark, windowLevelDirectionSensorWatermark); // direction from window level = OPEN = 1 , CLOSE = 0, ON TILT = 2,
}

/**
@brief	none
@param 	none
@return	none
**/
void updateNodeStructWatermarksFromEEPROM()
{
	uint8_t i = 0;
	// watermark flag variables
	updateEEpromByte(node_EEPROM[i].nodeSerialWatermark, nodeSerialWatermark);
	updateEEpromByte(node_EEPROM[i].nodeTypeWatermark, nodeTypeWatermark);
	updateEEpromByte(node_EEPROM[i].securityStateWatermark, securityStateWatermark);
	updateEEpromByte(node_EEPROM[i].nodeVersionWatermark, nodeVersionWatermark);
	updateEEpromByte(node_EEPROM[i].temparaturWatermark, temparaturWatermark);
	updateEEpromWord(node_EEPROM[i].lightsenseWatermark, lightsenseWatermark);
	updateEEpromByte(node_EEPROM[i].batteryWatermark, batteryWatermark);
	updateEEpromByte(node_EEPROM[i].alarmWatermark, alarmWatermark);
	updateEEpromByte(node_EEPROM[i].humidityWatermark, humidityWatermark);
	updateEEpromByte(node_EEPROM[i].airpressureWatermark, airpressureWatermark);
	updateEEpromByte(node_EEPROM[i].pirSensorWatermark, pirSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].doorBellWatermark, doorBellWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].audioSensorWatermark, audioSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].gasSensorWatermark, gasSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].carbonMonoxideSensorWatermark, carbonMonoxideSensorWatermark); // active or not active
	updateEEpromByte(node_EEPROM[i].radiationSensorCPSWatermark, radiationSensorCPSWatermark); // Counts per Second
	updateEEpromByte(node_EEPROM[i].radiationSensorCPMWatermark, radiationSensorCPMWatermark); // Counts per Minute
	updateEEpromByte(node_EEPROM[i].radiationSensorSVWatermark, radiationSensorSVWatermark); // µ Sievert
	updateEEpromByte(node_EEPROM[i].windowLevelDirectionSensorWatermark, windowLevelDirectionSensorWatermark); // direction from window level = OPEN = 1 , CLOSE = 0, ON TILT = 2,
}
	

/**
@brief	none
@param 	none
@return	none
**/
int checkWatermarkInteger(char *oldValue, char *newValue, int waterMark)
{
	uint8_t success = FALSE;
	uint16_t newValueInteger = 0;
	uint16_t oldValueInteger = 0;
	uint16_t watermarkInteger = 0;

	oldValueInteger = atoi(oldValue);
	newValueInteger = atoi(newValue);

	watermarkInteger = abs(oldValueInteger - newValueInteger);

	if (watermarkInteger >= waterMark)
	{
		success = TRUE;
	}
	else
	{
		success = FALSE;
	}
	return success;
}


/**
@brief	none
@param 	none
@return	none
**/
uint8_t checkNodeSerialAddress(char *nodeSerialAddress)
{
	uint8_t success = FALSE;
	uint8_t nodeAddressDigit = 0;
	char digit[] = "0123456789ABCDEF";
	nodeAddressDigit = strlen(digit);
	uint8_t nodeAddressSize = strlen(nodeSerialAddress);
	uint8_t i = 0;
	uint8_t y = 0;
	uint8_t digitCounter = 0;

	if ( nodeAddressSize == 8 )
	{
		for (i=0; i<=nodeAddressSize -1; i++)
		{
			for (y=0; y<=nodeAddressDigit; y++)
			{
				if (nodeSerialAddress[i] == digit[y])
				{
					digitCounter++;
					continue;
				}
			}
		}
	}
	else
	{
		success = FALSE;
	}

	if ( (digitCounter == nodeAddressSize) && (nodeAddressSize != 0) )
	{
		success = TRUE;
	}

	return success;
}

/**
@brief	none
@param 	none
@return	none
**/
int checkWatermarkFloat(char *oldValue, char *newValue, float waterMark)
{
	uint8_t success = FALSE;
	float newValueInteger = 0.00;
	float oldValueInteger = 0.00;
	float watermarkInteger = 0.00;

	oldValueInteger = atof(oldValue);
	newValueInteger = atof(newValue);

	watermarkInteger = abs(oldValueInteger - newValueInteger);

	if (watermarkInteger >= waterMark)
	{
		success = TRUE;
	}
	else
	{
		success = FALSE;
	}
	return success;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void getNodeInformation(unsigned char *clockData, unsigned char *sensorNodeAddress)
{
	uint8_t i = 0;
	unsigned char nodeQueryCounting[4];
	char nodeSerialEEprom[8];
	
	for (i=0; i <= MAX; i++)
	{
		if ( (strcmp (node[i].address, sensorNodeAddress) ) == 0 )
		{
			strncat(clockData, node[i].address, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].nodeSerial, 10);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].nodeType, 2);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].securityState, 2);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].nodeVersion, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].temperatur, 6);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].temperatur2, 6);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].temperatur3, 6);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].airpressure, 8);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].humidity, 6);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].battery, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].lightIntense, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].rfidKey, 10);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].rfidStatus, 2);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].pirSensor, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].doorBell, 2);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].audioSensor, 8);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].gasSensor, 8);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].carbonMonoxideSensor, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].radiationSensorCPS, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].radiationSensorCPM, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].radiationSensorSV, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].windowLevelDirectionSensor, 5);
			strncat(clockData,";", 1);
			strncat(clockData, node[i].alarm, 2);
			strncat(clockData,";", 1);
			itoa(node[i].count, nodeQueryCounting,10);
			strncat(clockData, nodeQueryCounting, 4);
			break;
		}
	}
}

uint8_t getDateTimeFromString(unsigned char *data)
{
	uint8_t success = FALSE;
	unsigned char yearChar[4];
	unsigned char monthChar[2];
	unsigned char dayChar[2];
	unsigned char minChar[2];
	unsigned char hourChar[2];
	unsigned char secChar[2];
	
	unsigned char *yearPtr;
	unsigned char *monthPtr;
	unsigned char *dayPtr;
	unsigned char *hourPtr;
	unsigned char *minPtr;
	unsigned char *secPtr;
	unsigned char *ptr;
	
	yearPtr = yearChar;
	monthPtr = monthChar;
	dayPtr = dayChar;
	hourPtr = hourChar;
	minPtr = minChar;
	secPtr = secChar;
	
	ptr = strtok(data, "-:");
	yearPtr = ptr;
	ptr = strtok(NULL, "-:");
	monthPtr = ptr;
	ptr = strtok(NULL, "-:;");
	dayPtr = ptr;
	ptr = strtok(NULL, "-:");
	hourPtr = ptr;
	ptr = strtok(NULL, "-:");
	minPtr = ptr;
	ptr = strtok(NULL, "-:");
	secPtr = ptr;
	
	year = atoi(yearPtr);
	month = atoi(monthPtr);
	day = atoi(dayPtr);
	hour = atoi(hourPtr);
	min = atoi(minPtr);
	sec = atoi(secPtr);
	
	if (year != NULL && month != NULL && day != NULL )
	{
		success = TRUE;
	}
	
	return success;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void getRsDataConfig(unsigned char *rsData, unsigned char getRsSituation)
{
	uint8_t success = FALSE;
	uint8_t restart = TRUE;
	uint8_t counter = 0;
	unsigned char rsStatement[100];
	
	if (usart_gets_intr(rsStatement) == TRUE)
	{
		if (strcmp(rsStatement, masterRsGetHello) == 0 )
		{
			switch (getRsSituation)
			{
				case getRsDataGetTimeSetup:
				{
					USART_print(masterRsTimeSetup);
					USART_print("\r\n");
					
					memset((void *) rsStatement, 0, 100);
					//*rsStatement = 0;
		
					while(! usart_gets_intr(rsStatement) );
					
					if (strlen(rsStatement) == 18)
					{
						if (getDateTimeFromString(rsStatement))
						{
							USART_print(masterRsDataOK);
							USART_print("\r\n");
						}
						else
						{
							USART_print(masterRsError);
							USART_print("\r\n");
						}
					}
					else
					{
						USART_print(masterRsError);
						USART_print("\r\n");
					}
					break;
				}
				
				case getRsDataGetNodeConfig:
				{
					USART_print(masterRsGetNodeConfig);
					USART_print("\r\n");
		
					memset((void *) rsStatement, 0, 100);
					//*rsStatement = 0;
		
					while(! usart_gets_intr(rsStatement) );
		
					if (strlen(rsStatement) == 4)
					{
						if (findNodeAdress(rsStatement) == TRUE)
						{
							while ( ! usart_gets_intr(rsStatement) );
							printf("SCORE: %s \n", rsStatement);	
						}
						else
						{
							USART_print(masterRsError);
							USART_print("\r\n");
						}
					}
					else
					{
						USART_print(rsStatement);
						USART_print("\n");
						USART_print("NO \n");
					}
					memset((void *) rsStatement, 0, 100);
					break;
				}
				
				case 2:
				{
					
					break;
				}
			}
		}
	}	
	else
	{
		memset((void *) rsStatement, 0, 100);
	}
	//else if (strcmp(rsStatement, masterRsGetWatermarkConfig) == 0 )
	//{
	//	USART_print("WATERMARK \n");
	//}
}


/**
	@brief	none
	@param 	none
	@return	none
*/
void tailInformation(unsigned char *info, unsigned char *completeData, unsigned char *sensorNodeAddress)
{
	//unsigned char rsData[5];
	
	//if (strcmp(sensorNodeAddress, masterNodeAddress) != 0)
	//{
		//strncat(completeData,"SN;",4);
		getDatum(completeData);
		getNodeInformation(completeData, sensorNodeAddress);
		strncat (completeData, "\n", 3);
	//}
	//else
	//{
		//strncat(completeData,"CSS;",4);
	//	getDatum(completeData);
	//	getNodeInformation(completeData, sensorNodeAddress);
		//strncat (completeData, info, 30);
	//	strncat (completeData, "\n", 3);
	//}
	USART_print(completeData);
	
	completeData[0] = '\0';
}

int makeCrcCheckSum(char *data)
{
	int crcCheckSum = 0;
	return crcCheckSum = atoi(data);
}

/**
@brief	none
@param 	none
@return	none
**/
uint8_t makeDataUpdate(char *data)
{	
	uint8_t success = FALSE;
	int crcCheck = 0;
	int newCheck = 0;
	
	uint8_t nodeSerialAlarmFlag;
	uint8_t nodeTypeAlarmFlag;
	uint8_t securityStateAlarmFlag;
	uint8_t nodeVersionAlarmFlag;
	uint8_t temperaturAlarmFlag;
	uint8_t temperatur2AlarmFlag;
	uint8_t temperatur3AlarmFlag;
	uint8_t airpressureAlarmFlag;
	uint8_t humidityAlarmFlag;
	uint8_t batteryAlarmFlag;
	uint8_t lightIntenseAlarmFlag;
	uint8_t rfidStatusAlarmFlag; // 0 or 1
	uint8_t pirSensorAlarmFlag; // active or not active
	uint8_t doorBellAlarmFlag; // active or not active
	uint8_t audioSensorAlarmFlag; // active or not active
	uint8_t gasSensorAlarmFlag; // active or not active
	uint8_t carbonMonoxideSensorAlarmFlag; // active or not active
	uint8_t radiationSensorCPSAlarmFlag; // Counts per Second
	uint8_t radiationSensorCPMAlarmFlag; // Counts per Minute
	uint8_t radiationSensorSVAlarmFlag; // µ Sievert
	uint8_t windowLevelDirectionSensorAlarmFlag; // direction from window level = OPEN, CLOSE, ON TILT
	uint8_t alarmFlag;
	uint8_t counter = 1;
	uint8_t i = 0;
	
	char nodeaddress[5] = "0";
	char helpData[10];
	char nodeCrcCheck[10];
	char nodeSerial[10] = "0";
	char nodeType[2] = "0";
	char securityState[2] = "0";
	char nodeSerialTmp[10] = "0";
	char temperatur[8] = "0";
	char temperatur2[8] = "0";
	char temperatur3[8] = "0";
	char airpressure[8] = "0";
	char humidity[8] = "0";
	char battery[5] = "0";
	char lightIntense[6] = "0";
	char rfidKey[11] = "0"; //  RFID key
	char rfidStatus[2] = "0"; // 0 or 1
	char pirSensor[2] = "0"; // active or not active
	char doorBell[2] = "0"; // active or not active
	char audioSensor[6] = "0"; // active or not active
	char gasSensor[6] = "0"; // active or not active
	char carbonMonoxideSensor[6] = "0"; // active or not active
	char radiationSensorCPS[6] = "0"; // Counts per Second
	char radiationSensorCPM[6] = "0"; // Counts per Minute
	char radiationSensorSV[6] = "0"; // µ Sievert
	char windowLevelDirectionSensor[2] = "0"; // direction from window level = OPEN, CLOSE, ON TILT,
	char nodeVersion[6] = "0";
	char alarm[2] = "0";

	char *ptr;
	char *nodeaddressPtr;
	char *securityStatePtr;
	char *nodeTypePtr;
	char *nodeSerialPtr;
	char *nodeSerialTmpPtr;
	char *temperaturPtr;
	char *temperaturPtr2;
	char *temperaturPtr3;
	char *airpressurePtr;
	char *humidityPtr;
	char *batteryPtr;
	char *lightIntensePtr;
	char *rfidKeyPtr;
	char *rfidStatusPtr;
	char *pirSensorPtr;
	char *doorBellPtr;
	char *audioSensorPtr;
	char *gasSensorPtr;
	char *carbonMonoxideSensorPtr;
	char *radiationSensorCPSPtr;
	char *radiationSensorCPMPtr;
	char *radiationSensorSVPtr;
	char *windowLevelDirectionSensorPtr;
	char *nodeVersionPtr;
	char *alarmPtr;
	char *nodeCrcCheckPtr;

	nodeaddressPtr = nodeaddress;
	nodeSerialPtr = nodeSerial;
	nodeTypePtr = nodeType;
	securityStatePtr = securityState;
	nodeSerialTmpPtr = nodeSerialTmp;
	temperaturPtr = temperatur;
	temperaturPtr2 = temperatur2;
	temperaturPtr3 = temperatur3;
	airpressurePtr = airpressure;
	humidityPtr = humidity;
	batteryPtr = battery;
	lightIntensePtr = lightIntense;
	rfidKeyPtr = rfidKey;
	rfidStatusPtr = rfidStatus;
	pirSensorPtr = pirSensor;
	doorBellPtr = doorBell;
	audioSensorPtr = audioSensor;
	gasSensorPtr = gasSensor;
	carbonMonoxideSensorPtr = carbonMonoxideSensor;
	radiationSensorCPSPtr = radiationSensorCPS;
	radiationSensorCPMPtr = radiationSensorCPM;
	radiationSensorSVPtr = radiationSensorSV;
	windowLevelDirectionSensorPtr = windowLevelDirectionSensor;
	nodeVersionPtr = nodeVersion;
	alarmPtr = alarm;
	nodeCrcCheckPtr = nodeCrcCheck;

	ptr = strtok(data, ":;");

	while(ptr != NULL)
	{
		if (strcmp(ptr, "N#") == 0 )
		{
			ptr = strtok(NULL, ";");
			nodeaddressPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}
		
		if (strcmp(ptr, "S#") == 0 )
		{
			ptr = strtok(NULL, ";");
			nodeSerialPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}
		
		if (strcmp(ptr, "T#") == 0 )
		{
			ptr = strtok(NULL, ";");
			nodeTypePtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}
		
		if (strcmp(ptr, "I#") == 0 )
		{
			ptr = strtok(NULL, ";");
			securityStatePtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}
		
		if (strcmp(ptr, "V#") == 0 )
		{
			ptr = strtok(NULL, ";");
			nodeVersionPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "T1") == 0 )
		{
			ptr = strtok(NULL, ";");
			temperaturPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "T2") == 0 )
		{
			ptr = strtok(NULL, ";");
			temperaturPtr2 = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "T3") == 0 )
		{
			ptr = strtok(NULL, ";");
			temperaturPtr3 = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "H1") == 0 )
		{
			ptr = strtok(NULL, ";");
			humidityPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "B1") == 0 )
		{
			ptr = strtok(NULL, ";");
			batteryPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}
		
		if (strcmp(ptr, "D1") == 0 )
		{
			ptr = strtok(NULL, ";");
			doorBellPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "L1") == 0 )
		{
			ptr = strtok(NULL, ";");
			lightIntensePtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "R1") == 0 )
		{
			ptr = strtok(NULL, ";");
			rfidKeyPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "R2") == 0 )
		{
			ptr = strtok(NULL, ";");
			rfidStatusPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "G1") == 0 )
		{
			ptr = strtok(NULL, ";");
			gasSensorPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "A1") == 0 )
		{
			ptr = strtok(NULL, ";");
			airpressurePtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "A2") == 0 )
		{
			ptr = strtok(NULL, ";");
			audioSensorPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "C1") == 0 )
		{
			ptr = strtok(NULL, ";");
			carbonMonoxideSensorPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}
		
		if (strcmp(ptr, "S1") == 0 )
		{
			ptr = strtok(NULL, ";");
			radiationSensorCPSPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "S2") == 0 )
		{
			ptr = strtok(NULL, ";");
			radiationSensorCPMPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "S3") == 0 )
		{
			ptr = strtok(NULL, ";");
			radiationSensorSVPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "W1") == 0 )
		{
			ptr = strtok(NULL, ";");
			windowLevelDirectionSensorPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "M1") == 0 )
		{
			ptr = strtok(NULL, ";");
			pirSensorPtr = ptr;
			crcCheck += makeCrcCheckSum(ptr);
		}

		if (strcmp(ptr, "CR") == 0 )
		{
			ptr = strtok(NULL, ";");
			newCheck = atoi(ptr);
		}

		ptr = strtok(NULL, ":;");
	}
	
	#if (printDebugging == TRUE)
		USART_print("CRC ADDITION FROM DATA: ");
		itoa(crcCheck, helpData, 10);
		USART_print(helpData);
		USART_print("\n");
		
		USART_print("CRC READ FROM DATA: ");
		itoa(newCheck, helpData, 10);
		USART_print(helpData);
		USART_print("\n");	
	#endif
	
	// check the crc checksum bevor we can use the node information
	if ( crcCheck == newCheck )
	{
		for (i=0; i<=MAX;i++)
		{
			if ( strcmp(nodeaddressPtr, node[i].address) == 0 )
			{
				// Check watermarks ......
				// in this version we only test the values of whole numbers that is very easier on development.
				// all the flags can be used later in the program!
				
				// check if the serial number is not given from node!!
				eeprom_read_block(nodeSerialTmpPtr, node_EEPROM[i].nodeSerial_EEPROM, sizeof(nodeSerialTmp) );
				nodeSerialAlarmFlag = checkWatermarkInteger(nodeSerialPtr, nodeSerialTmpPtr, nodeSerialWatermark );
				if ( nodeSerialAlarmFlag == TRUE)
				{
					//itoa(nodeSerialAlarmFlag, infoPtr, 10);
					node[i].nodeSerialAlarmFlag = TRUE;
				}
				
				// all the flags can be used later in the program!
				nodeTypeAlarmFlag = checkWatermarkInteger(nodeTypePtr, node[i].nodeType, nodeTypeWatermark );
				if ( nodeTypeAlarmFlag == TRUE)
				{
					//itoa(nodeVersionAlarmFlag, infoPtr, 10);
					node[i].nodeTypeAlarmFlag = TRUE;
				}
				
				// all the flags can be used later in the program!
				securityStateAlarmFlag = checkWatermarkInteger(securityStatePtr, node[i].securityState, securityStateWatermark );
				if ( nodeTypeAlarmFlag == TRUE)
				{
					//itoa(nodeVersionAlarmFlag, infoPtr, 10);
					node[i].securityStateAlarmFlag = TRUE;
				}
				
				// all the flags can be used later in the program!
				nodeVersionAlarmFlag = checkWatermarkInteger(nodeVersionPtr, node[i].nodeVersion, nodeVersionWatermark );
				if ( nodeVersionAlarmFlag == TRUE)
				{
					//itoa(nodeVersionAlarmFlag, infoPtr, 10);
					node[i].nodeVersionAlarmFlag = TRUE;
				}
				
				// all the flags can be used later in the program!
				temperaturAlarmFlag = checkWatermarkInteger(temperaturPtr, node[i].temperatur, temparaturWatermark );
				if ( temperaturAlarmFlag == TRUE)
				{
					//itoa(temperaturAlarmFlag, infoPtr, 10);
					node[i].temperaturAlarmFlag = TRUE;
				}

				temperatur2AlarmFlag = checkWatermarkInteger(temperaturPtr2, node[i].temperatur2, temparaturWatermark);
				if ( temperatur2AlarmFlag == TRUE)
				{
					//itoa(temperatur2AlarmFlag, infoPtr, 10);
					node[i].temperatur2AlarmFlag = TRUE;
				}

				temperatur3AlarmFlag = checkWatermarkInteger(temperaturPtr3, node[i].temperatur3, temparaturWatermark);
				if ( temperatur3AlarmFlag == TRUE)
				{
					//itoa(temperatur2AlarmFlag, infoPtr, 10);
					node[i].temperatur3AlarmFlag = TRUE;
				}

				airpressureAlarmFlag = checkWatermarkInteger(airpressurePtr, node[i].airpressure, airpressureWatermark);
				if ( airpressureAlarmFlag == TRUE)
				{
					//itoa(airpressureAlarmFlag, infoPtr, 10);
					node[i].airpressureAlarmFlag = TRUE;
				}

				humidityAlarmFlag = checkWatermarkInteger(humidityPtr, node[i].humidity, humidityWatermark);
				if ( humidityAlarmFlag == TRUE)
				{
					//itoa(humidityAlarmFlag, infoPtr, 10);
					node[i].humidityAlarmFlag = TRUE;
				}

				batteryAlarmFlag = checkWatermarkInteger(batteryPtr, node[i].battery, batteryWatermark);
				if ( batteryAlarmFlag == TRUE)
				{
					//itoa(batteryAlarmFlag, infoPtr, 10);
					node[i].batteryAlarmFlag = TRUE;
				}

				lightIntenseAlarmFlag = checkWatermarkInteger(lightIntensePtr, node[i].lightIntense, lightsenseWatermark);
				if ( lightIntenseAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].lightIntenseAlarmFlag = TRUE;
				}

				pirSensorAlarmFlag = checkWatermarkInteger(pirSensorPtr, node[i].pirSensor, pirSensorWatermark);
				if ( pirSensorAlarmFlag == TRUE)
				{
					//itoa(pirSensorAlarmFlag, infoPtr, 10);
					node[i].pirSensorAlarmFlag = TRUE;
				}
				
				doorBellAlarmFlag = checkWatermarkInteger(doorBellPtr, node[i].doorBell, doorBellWatermark);
				if ( doorBellAlarmFlag == TRUE)
				{
					//itoa(pirSensorAlarmFlag, infoPtr, 10);
					node[i].doorBellAlarmFlag = TRUE;
				}
				
				audioSensorAlarmFlag = checkWatermarkInteger(audioSensorPtr, node[i].audioSensor, audioSensorWatermark);
				if ( audioSensorAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].audioSensorAlarmFlag = TRUE;
				}

				gasSensorAlarmFlag = checkWatermarkInteger(gasSensorPtr, node[i].gasSensor, gasSensorWatermark);
				if ( gasSensorAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].gasSensorAlarmFlag = TRUE;
				}

				carbonMonoxideSensorAlarmFlag = checkWatermarkInteger(carbonMonoxideSensorPtr, node[i].carbonMonoxideSensor, carbonMonoxideSensorWatermark);
				if ( carbonMonoxideSensorAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].carbonMonoxideSensorAlarmFlag = TRUE;
				}

				radiationSensorCPSAlarmFlag = checkWatermarkInteger(radiationSensorCPMPtr, node[i].radiationSensorCPS, radiationSensorCPSWatermark);
				if ( radiationSensorCPSAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].radiationSensorCPSAlarmFlag = TRUE;
				}

				radiationSensorCPMAlarmFlag = checkWatermarkInteger(radiationSensorCPMPtr, node[i].radiationSensorCPM, radiationSensorCPMWatermark);
				if ( radiationSensorCPMAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].radiationSensorCPMAlarmFlag = TRUE;
				}

				radiationSensorSVAlarmFlag = checkWatermarkInteger(radiationSensorSVPtr, node[i].radiationSensorSV, radiationSensorSVWatermark);
				if ( radiationSensorSVAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].radiationSensorSVAlarmFlag = TRUE;
				}

				windowLevelDirectionSensorAlarmFlag = checkWatermarkInteger(windowLevelDirectionSensorPtr, node[i].windowLevelDirectionSensor, windowLevelDirectionSensorWatermark);
				if ( windowLevelDirectionSensorAlarmFlag == TRUE)
				{
					//itoa(lightIntenseAlarmFlag, infoPtr, 10);
					node[i].windowLevelDirectionSensorAlarmFlag = TRUE;
				}

				alarmFlag = checkWatermarkInteger(alarmPtr, node[i].alarm, alarmWatermark);
				if ( alarmFlag == TRUE)
				{
					//itoa(alarmFlag, infoPtr, 10);
					node[i].alarmFlag = TRUE;
				}
				
				if ( securityState == TRUE )
				{
					strcpy(node[i].nodeSerial, nodeSerialPtr);
					strcpy(node[i].nodeType, nodeTypePtr);
					strcpy(node[i].securityState, securityStatePtr);
					strcpy(node[i].nodeVersion, nodeVersionPtr);
					strcpy(node[i].battery, batteryPtr); // Battery information
					strcpy(node[i].lightIntense, lightIntensePtr);
					strcpy(node[i].rfidKey, rfidKeyPtr); //  RFID key
					strcpy(node[i].rfidStatus, rfidStatusPtr); // 0 or 1
					strcpy(node[i].pirSensor, pirSensorPtr); // active or not active
					strcpy(node[i].doorBell, doorBellPtr); // active or not active
					strcpy(node[i].audioSensor, audioSensorPtr); // active or not active
					strcpy(node[i].gasSensor, gasSensorPtr); // active or not active
					strcpy(node[i].carbonMonoxideSensor, carbonMonoxideSensorPtr); // active or not active
					strcpy(node[i].radiationSensorCPS, radiationSensorCPSPtr); // Counts per Second
					strcpy(node[i].radiationSensorCPM, radiationSensorCPMPtr); // Counts per Minute
					strcpy(node[i].radiationSensorSV, radiationSensorSVPtr); // µSievert	
				}
				else
				{
					// Copy the new informations into a struct table "node"				
					strcpy(node[i].nodeSerial, nodeSerialPtr);
					strcpy(node[i].nodeType, nodeTypePtr);
					strcpy(node[i].securityState, securityStatePtr);
					strcpy(node[i].nodeVersion, nodeVersionPtr);
					strcpy(node[i].temperatur, temperaturPtr);
					strcpy(node[i].temperatur2, temperaturPtr2);
					strcpy(node[i].temperatur3, temperaturPtr3);
					strcpy(node[i].airpressure, airpressurePtr); // airpressure
					strcpy(node[i].humidity, humidityPtr); // Humidty data
					strcpy(node[i].battery, batteryPtr); // Battery information
					strcpy(node[i].lightIntense, lightIntensePtr);
					strcpy(node[i].rfidKey, rfidKeyPtr); //  RFID key
					strcpy(node[i].rfidStatus, rfidStatusPtr); // 0 or 1
					strcpy(node[i].pirSensor, pirSensorPtr); // active or not active
					strcpy(node[i].doorBell, doorBellPtr); // active or not active
					strcpy(node[i].audioSensor, audioSensorPtr); // active or not active
					strcpy(node[i].gasSensor, gasSensorPtr); // active or not active
					strcpy(node[i].carbonMonoxideSensor, carbonMonoxideSensorPtr); // active or not active
					strcpy(node[i].radiationSensorCPS, radiationSensorCPSPtr); // Counts per Second
					strcpy(node[i].radiationSensorCPM, radiationSensorCPMPtr); // Counts per Minute
					strcpy(node[i].radiationSensorSV, radiationSensorSVPtr); // µSievert
					strcpy(node[i].windowLevelDirectionSensor, windowLevelDirectionSensorPtr); // direction from window level = OPEN, CLOSE, ON TILT,
					strcpy(node[i].alarm, alarmPtr);
				}
				node[i].count = queryCounting;
				success = TRUE;
				break;
			}
		}
	}
	else
	{
		success = FALSE;
	}
	return success;
} 
