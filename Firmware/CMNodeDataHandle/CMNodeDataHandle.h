﻿/*
 * CMNodeDataHandle.h
 *
 * Created: 16.01.2017 20:56:21
 *  Author: Christian Möllers
 */ 


#ifndef CMNODEDATAHANDLE_H_
#define CMNODEDATAHANDLE_H_

#define getRsDataGetTimeSetup		0
#define getRsDataGetNodeConfig		1

#ifndef MAX
#define MAX		 	10
#endif

// eeprom struct table
struct __eeprom_nodeData
{
	char nodeAddress_EEPROM[5];
	char nodeSerial_EEPROM[10];
	uint8_t nodeSerialWatermark;
	uint8_t nodeTypeWatermark;
	uint8_t securityStateWatermark;
	uint8_t nodeVersionWatermark;
	uint8_t temparaturWatermark;
	uint16_t lightsenseWatermark;
	uint8_t batteryWatermark;
	uint8_t alarmWatermark;
	uint8_t humidityWatermark;
	uint8_t airpressureWatermark;
	uint8_t pirSensorWatermark; // active or not active
	uint8_t doorBellWatermark; // active or not active
	uint8_t audioSensorWatermark; // active or not active
	uint8_t gasSensorWatermark; // active or not active
	uint8_t carbonMonoxideSensorWatermark; // active or not active
	uint8_t radiationSensorCPSWatermark; // Counts per Second
	uint8_t radiationSensorCPMWatermark; // Counts per Minute
	uint8_t radiationSensorSVWatermark; // µ Sievert
	uint8_t windowLevelDirectionSensorWatermark; // direction from window level = OPEN = 1 , CLOSE = 0, ON TILT = 2,
}node_EEPROM[MAX];


// dynamic struct table
struct nodeTable
{
	char address[5]; // this is the node address for node :-)
	char nodeSerial[10]; // node serial, that is interesting if we have a problem with css
	char nodeType[2]; // this is the node type definition
	char securityState[2]; // this is the node type definition
	char gatewayAddress[6]; // if node need a node gateway
	char sessionId[5]; // session ID for the communication
	char temperatur[8]; // temperatur from lm75 or anything else
	char temperatur2[8]; // temperatur from ds18s20 or anything else
	char temperatur3[8]; // temperatur from airpressure or anything else
	char airpressure[8]; // pressure in hPa
	char humidity[8]; // humidity in %
	char battery[5]; // battery volt
	char lightIntense[5]; // adc measure
	char rfidKey[11]; //  RFID key
	char rfidStatus[2]; // 0 or 1
	char pirSensor[2]; // active or not active
	char doorBell[2]; // active or not active
	char audioSensor[6]; // active or not active
	char gasSensor[6]; // active or not active
	char carbonMonoxideSensor[6]; // active or not active
	char radiationSensorCPS[6]; // Counts per Second
	char radiationSensorCPM[6]; // Counts per Minute
	char radiationSensorSV[6]; // µ Sievert
	char windowLevelDirectionSensor[2]; // direction from window level = OPEN, CLOSE, ON TILT,
	char nodeVersion[6]; // node software version
	char alarm[2];
	
	uint8_t nodeSerialWatermark;
	uint8_t nodeTypeWatermark;
	uint8_t securityStateWatermark;
	uint8_t nodeVersionWatermark;
	uint8_t temparaturWatermark;
	uint16_t lightsenseWatermark;
	uint8_t batteryWatermark;
	uint8_t alarmWatermark;
	uint8_t humidityWatermark;
	uint8_t airpressureWatermark;
	uint8_t pirSensorWatermark; // active or not active
	uint8_t doorBellWatermark; // active or not active
	uint8_t audioSensorWatermark; // active or not active
	uint8_t gasSensorWatermark; // active or not active
	uint8_t carbonMonoxideSensorWatermark; // active or not active
	uint8_t radiationSensorCPSWatermark; // Counts per Second
	uint8_t radiationSensorCPMWatermark; // Counts per Minute
	uint8_t radiationSensorSVWatermark; // µ Sievert
	uint8_t windowLevelDirectionSensorWatermark; // direction from window level = OPEN = 1 , CLOSE = 0, ON TILT = 2,
	
	uint8_t nodeSerialAlarmFlag;
	uint8_t nodeTypeAlarmFlag;
	uint8_t securityStateAlarmFlag;
	uint8_t nodeVersionAlarmFlag;
	uint8_t blockingNodeAlarmFlag; // blocking flag for corrupt data.
	uint8_t temperaturAlarmFlag;
	uint8_t temperatur2AlarmFlag;
	uint8_t temperatur3AlarmFlag;
	uint8_t airpressureAlarmFlag;
	uint8_t humidityAlarmFlag;
	uint8_t batteryAlarmFlag;
	uint8_t lightIntenseAlarmFlag;
	uint8_t pirSensorAlarmFlag; // active or not active
	uint8_t doorBellAlarmFlag; // active or not active
	uint8_t audioSensorAlarmFlag; // active or not active
	uint8_t gasSensorAlarmFlag; // active or not active
	uint8_t carbonMonoxideSensorAlarmFlag; // active or not active
	uint8_t radiationSensorCPSAlarmFlag; // Counts per Second
	uint8_t radiationSensorCPMAlarmFlag; // Counts per Minute
	uint8_t radiationSensorSVAlarmFlag; // µ Sievert
	uint8_t windowLevelDirectionSensorAlarmFlag; // direction from window level = OPEN, CLOSE, ON TILT,
	uint8_t alarmFlag;
	uint8_t activ;
	uint16_t count;
	uint8_t wakeUpTime;
}node[MAX];

int checkWatermarkInteger(char *oldValue, char *newValue, int waterMark);
uint8_t checkNodeSerialAddress(char *nodeSerialAddress);
int checkWatermarkFloat(char *oldValue, char *newValue, float waterMark);
void getNodeInformation(unsigned char *clockData, unsigned char *sensorNodeAddress);
////void tailInformation(unsigned char *info, unsigned char *completeData, unsigned char *sensorNodeAddress);
int makeCrcCheckSum(char *data);
uint8_t makeDataUpdate(char *data);
void tailInformation(unsigned char *info, unsigned char *clockData, unsigned char *sensorNodeAddress);
void getRsDataConfig(unsigned char *rsData, unsigned char getRsSituation);

#endif /* CMNODEDATAHANDLE_H_ */