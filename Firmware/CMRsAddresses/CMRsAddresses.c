﻿/*
 * CMRsAddresses.c
 *
 * Created: 31.10.2018 17:04:22
 *  Author: Christian Möllers
 */ 
#include <avr/io.h>
#include "../CMGlobal.h"


unsigned char masterNodeAddress[5]			= "9900";
unsigned char falseNodeNumber[5]			= "0000";
unsigned char usegatewayNumber[5]			= "1211";
unsigned char nodeNumber[5]					= "1231";
unsigned char nodeBroadcast[5]				= "1233";
unsigned char nodeSignIn[5]					= "1234";
unsigned char nodeSignOut[5]				= "1235";
unsigned char nodeError[5]					= "1236";
unsigned char nodeSignOk[5]					= "1237";
unsigned char sensorInput[5]				= "1238";
unsigned char nodeHello[5]					= "1239";
unsigned char nodeInfo[5]					= "1240";
unsigned char nodeRestart[5]				= "1241";
unsigned char nodeSettings[5]				= "1242";
unsigned char nodeAddressNotDefined[5]		= "1243";
unsigned char nodeSerialAddress[5]			= "1244";
unsigned char nodeTimeSetup[5]				= "1245";
unsigned char masterNodeRestart[5]			= "1246";


unsigned char masterRsDataOK[5]				= "1245";
unsigned char masterRsError[5]				= "1246";
unsigned char masterRsGetNodeConfig[5]		= "1247";
unsigned char masterRsGetWatermarkConfig[8] = "1248";
unsigned char masterRsGetHello[8]			= "1249";
unsigned char masterRsTimeSetup[5]			= "1250";

unsigned char nodeUpdate[5]					= "1301";

// define the crypt states
unsigned char changeCryptKey[5]				= "1260";
unsigned char changeCryptKeyCycles[5]		= "1261";
unsigned char changeCryptDelta[5]			= "1262";

// define the sensor state "on or off" or more
unsigned char sensorNodeShutdownInt[5]		= "6666";
unsigned char windowLevelSenorInt[5]		= "1282";
unsigned char audioSensorInt[5]				= "1284";
unsigned char gasSensorInt[5]				= "1286";
unsigned char carbonMonoxideSensorInt[5]	= "1288";
unsigned char pirSensorInt[5]				= "1290";
unsigned char magentSensorInt[5]			= "1292";



