/*
 * CMneuitoa.c
 *
 *  Created on: 24.11.2014
 *      Author: christian
 */

#include <avr/io.h>

/**
	@brief	NEUITOA function
	@param 	none
	@return	none
*/
void neuitoa(uint8_t i, char *b)
{
    uint8_t c = 0;
    char digit[] = "0123456789";

    c = i;

    if(i<0){
        *b++ = '-';
        i = -1;
    }
    uint8_t shifter = i;

    do
    { //Move to where representation ends
        ++b;
        shifter = shifter/10;
    }
    while(shifter);
    *b = '\0';

    do
    { //Move back, inserting digits as u go
        *--b = digit[i%10];
        //*--b = digit[i%10];
        i = i/10;
    }
    while(i);

    if ( c <= 9 )
    {
        b[1] = b[0];
        b[0] = 0x30;
        b[2] = '\0';
    }
}

