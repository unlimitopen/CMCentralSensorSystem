﻿/*
 * CMPirSensor.h
 *
 * Created: 29.03.2017 21:48:59
 *  Author: Christian Möllers
 */ 


#ifndef CMPIRSENSOR_H_
#define CMPIRSENSOR_H_

#define pirActivePin			PC2
#define pirActivePinRegister	DDRC
#define pirActivePinPort		PORTC
#define pirActivePinIORegister	PINC
#define pirActivePinOutput		pirActivePinRegister |= (1<<pirActivePin)
#define pirActivePinInput		pirActivePinRegister &=~ (1<<pirActivePin)
#define pirActivePinHigh		pirActivePinPort |= (1<<pirActivePin)
#define pirActivePinLow		    pirActivePinPort &=~ (1<<pirActivePin)
#define pirActivePinToggle		pirActivePinPort ^= (1<<pirActivePin)
#define pirActivePinPressed		(pirActivePinIORegister&(1<<pirActivePin) ? 1 : 0)

#define pirSensorPin			PA4
#define pirSensorPinRegister	DDRA
#define pirSensorPinPort		PORTA
#define pirSensorPinIORegister	PINA
#define pirSensorPinOutput		pirSensorPinRegister |= (1<<pirSensorPin)
#define pirSensorPinInput		pirSensorPinRegister &=~ (1<<pirSensorPin)
#define pirSensorPinHigh		pirSensorPinPort |= (1<<pirSensorPin)
#define pirSensorPinLow		    pirSensorPinPort &=~ (1<<pirSensorPin)
#define pirSensorPinToggle		pirSensorPinPort ^= (1<<pirSensorPin)
#define pirSensorPinPressed		(pirSensorPinIORegister&(1<<pirSensorPin) ? 1 : 0)

void pirDirection(uint8_t direction);
uint8_t getPirMotionSensor(void);

#endif /* CMPIRSENSOR_H_ */