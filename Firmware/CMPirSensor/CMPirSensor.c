﻿/*
 * CMPirSensor.c
 *
 * Created: 29.03.2017 21:48:44
 *  Author: Christian Möllers
 */ 

#include <avr/io.h>
#include "CMPirSensor.h"
#include "../CMGlobal.h"

/**
	@brief	none
	@param 	none
	@return	none
*/
void pirDirection(uint8_t direction)
{
	if (direction == TRUE)
	{
		pirSensorPinInput;	
		pirSensorPinLow;
		pirActivePinOutput;
		pirActivePinHigh;	
	}
	else
	{
		pirSensorPinInput;	
		pirSensorPinLow;
		pirActivePinOutput;
		pirActivePinLow;
	}
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getPirMotionSensor(void)
{
	uint8_t success = FALSE;
	
	if ( ! pirSensorPinPressed == 0)
	{
		success = TRUE;
	}
	else
	{
		success = FALSE;
	}
	
	return success;
}