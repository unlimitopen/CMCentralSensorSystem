
#include <avr/io.h>
#include "../CMUart/CMUart0.h"

void bitDumpChar(const char *str)
{
    char bitchar[9] = "";

    printf ("BIN:\n");
    do
    {
        char *cp = bitchar;
        int i;

        for (i = 7; i >= 0; i--)
        {
            *cp++ = ((*str >> i) & 1 ? '1' : '0');
        }
        printf ("%s\n", bitchar);
        *cp++ = '\0';
    } while (*str++);
}

void bitDumpInt(int zahl)
{
	unsigned int bitchar[9];
	int i = 0;

	printf ("BIN:\n");

	for (i = 7; i >= 0; i--)
	{
		bitchar[i]=((zahl >> i) & 1 ? 1 : 0);
		printf ("%d", bitchar[i]);
	}
	printf ("\n");

}


