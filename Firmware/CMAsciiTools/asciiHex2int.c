
#include <avr/io.h>

void hexAscii2Int( unsigned char *string, unsigned char *newString )
{
    while(*string)
    {
        if ( *string >= '0' && *string <= '9')
        {
            *newString = *string - '0'; 
            //printf ("0-9: %c newString %02x \n", *string, *newString);
        }
        else
        {
           if ( *string >= 'A' && *string <= 'F' )
            {
                *newString = (10 + *string) - 'A';
                //printf ("A-F: %c newString %02x \n", *string, *newString);
            }
        }
       *newString++;
       *string++;
    }
    *newString++ = 0xFF;
    *newString = '\0';
}

void twoint2one (unsigned char *zahl, unsigned char *septs)
{
    while(*zahl != 0xFF)
    {
        *septs = *zahl++ << 4;
        *septs |= *zahl;
        //printf ("Int: %i  Hex: %02x \n", *septs, *septs);
        *septs++;
        *zahl++;
    }
    *septs = '\0';
}

int countdigits(unsigned int x) 
{ 
    unsigned count=1; 
    unsigned int value= 10; 
    while (x>=value) 
    { 
        value*=10; 
        count++; 
    } 
    return count; 
}

