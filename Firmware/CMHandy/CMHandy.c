/*
 * CMHandy.c
 *
 * Created: 19.04.2015 11:00:32
 *  Author: PC
 */

#include <avr/io.h>
#include "CMHandy.h"
#include "../CMNodeDataHandle/CMNodeDataHandle.h"
#include "../CMUart/CMUart0.h"
#include "../CMGlobal.h"
#include <avr/interrupt.h>
#include <string.h>

#include <util/delay.h>


// TODO: NEW UART for UART1 and UART0
//#define USART_BAUD_RATE_1     19200UL
//#define USART_BAUD_CALC_1     (F_CPU/(USART_BAUD_RATE_1*16L)-1)

// Lowbyte
//#define LOW_1(x)              ((x) & 0xFF)
//#define HIGH_1(x)             (((x) >> 8) & 0xFF)

extern uint8_t sensorNodeQuery;
extern volatile uint8_t queryChange;
extern volatile uint16_t queryCounting;
extern volatile uint8_t TimeTick;

uint8_t sendingStopCounter = 0;
uint8_t sendingQuery = 0;
uint8_t akkuValueWatermark = 10;
uint8_t akkuCharging = FALSE;
uint8_t akkuCharchingCount = 0;

//uint8_t state = 1;
uint8_t status = FALSE;
//USART1_init();

//uint8_t success = FALSE;

const char ringBuffer;
unsigned char puffer[MAX];
char puffer2[MAX];
char *information;

void func_read_sms_center(char *string, char *new_string)
{
	int i, a = 0;
	for (i=0; string[i] !='\0'; i++)
	{
		// SMS Center Number
		if (i >= 4 && i <= 15)
		{
			new_string[a] = string[i];
			a++;
		}
	}
	rev_string( new_string, new_string);
	//return new_string;
}

void func_read_sms_number( char *string, char *new_string)
{
	int i, b = 0;
	for( i = 0; string[i] != '\0'; i++)
	{
		// SMS Receive or Sender Number
		if (i >= 22 && i <=33)
		{
			new_string[b] = string[i];
			b++;
		}
	}

	rev_string( new_string, new_string);
	//return new_string;
}

void func_read_sms_text( char *string, char *new_string)
{
	int i = 0;
	while(*string != '\0')
	{
		// Read SMS Message
		if (i > 53 )
		{
			*new_string = *string;
			*new_string++;
		}
		i++;
		*string++;
	}
	*new_string = '\0';
}

void func_read_sms_date( char *string, char *new_string)
{
	int c=0, i;

	for ( i=0; string[i] != '\0'; i++)
	{
		// SMS Date
		if (i >= 38 && i <= 52 )
		{
			//pdu_sms_date[c] = string[i];
			new_string[c] = string[i];
			c++;
		}
	}

	rev_string( new_string, new_string);
	//return new_string;
}

// TODO We need a big change for AVR Controllers
void rev_string(char *string, char *new_string)
{
	int c = 0,s=0;
	char a, b;
	while (string[s] !='\0')
	{
		a = string[s];
		s++;
		b = string[s];
		s++;
		new_string[c] = b;
		c++;
		new_string[c] = a;
		c++;
	}
}


uint8_t lese_befehl ( char *string )
{
	uint8_t success = FALSE;

	success = getUARTString(string);
	return success;
}

uint8_t sende_befehl ( const char *string )
{
	uint8_t n = 0 ;
	//uint8_t str_bytes = 0;
	uint8_t success = FALSE;

	USART_print(string);
	USART_print("\r\n");

	success = TRUE;
	return success;
}

// TODO We need a big change for AVR Controllers
void ende_string(char *string, char *string_ret)
{
	uint8_t e=0, i=0;
	for(;;)
	{
		if(string[i] != '\n'  )
		{
			string_ret[e] = string[i];
			e++;
		}
		if(string[i] == '\0')
		{
			string_ret[e] = '\0';
			break;
		}
		i++;
	}
}

char *read_pdu_str(int pos, int n, char *string, char *string_ret)
{
	int e=0, i=0;
	for(;;)
	{
		if(string[i] != 'O' && string[i] != ',' && string[i] != 'K' && string[i] != ' ' && string[i] != '+' && string[i] != ':' )
		//        if( string[i] != 'O' && string[i] != 'K' &&   )
		{
			string_ret[e] = string[i];
			e++;
		}
		if(string[i] == '\0')
		{
			string_ret[e] = '\0';
			break;
		}
		i++;
	}

	USART_print ("PRINT \n");

	for (i = pos+n;string_ret[i] != '\0'; i++)
	{
		string_ret[i-n] = string_ret[i];

	}
	string_ret[i-n] = '\0';
	return string_ret;
}

uint8_t checkBatteryStatus(char *string, char *stringBack)
{
	uint8_t success = FALSE;
	uint8_t akkuValue = 0;
	sende_befehl (battery_status);

	lese_befehl(string);
	string = parsingStringElement("+CBC: 0,", string, stringBack);
	akkuValue = atoi(string);

	if (akkuValue >= akkuValueWatermark )
	{
		success = TRUE;
	}
	else
	{
		success = FALSE;
		akkuCharging = TRUE;
		akkuCharchingCount++;
	}
	memset(string, 0, 160);
	return success;
}

uint8_t checkHandyConnection(char *string)
{
	uint8_t success = FALSE;

	sende_befehl (check_handy_con);
	// TODO We need a big change for AVR Controllers
	lese_befehl(string);

	if (strcmp (string, "OK") == 0)
	{
		success = TRUE;
	}
	else
	{
		success = FALSE;
	}
	memset(string, 0, 160);
	return success;
}


uint8_t checkSCIDNumber(char *string, char *stringBack)
{
	uint8_t success = FALSE;
	uint8_t i= 0;
	sende_befehl (read_scid);

	lese_befehl(string);

	string = parsingStringElement("^SCID: ", string, stringBack);

	if (strcmp (string, SCID_NUMBER) == 0)
	{
		success = TRUE;
	}
	else
	{
		success = FALSE;
	}
	memset(string, 0, 160);
	memset(stringBack, 0, 160);

	return success;
}

uint8_t checkSmsMessages(char *string)
{
	uint8_t success = FALSE;

	sende_befehl (readAllSmsMessages);

	lese_befehl(string);

	if (strcmp (string, "OK") == 0)
	{
		success = TRUE;
	}
	else
	{
		success = FALSE;
	}
	memset(string, 0, 160);
	return success;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void readAlarmManagement(unsigned char *nodeaddress)
{
	uint8_t i = 0;
	uint8_t alarmActiv = FALSE;

	for (i=0; i<=MAX;i++)
	{
		if ( strcmp(nodeaddress, node[i].address) == 0 )
		{
			if (node[i].airpressureAlarmFlag)
			{
				USART_print("AIRPRESSURE ALARM!! \n");
				node[i].airpressureAlarmFlag = FALSE;
			}

			if (node[i].batteryAlarmFlag)
			{
				USART_print("BATTERY ALARM!! \n");
				node[i].batteryAlarmFlag = FALSE;
			}

			if (node[i].humidityAlarmFlag)
			{
				USART_print("HUMIDITY ALARM!! \n");
				node[i].humidityAlarmFlag = FALSE;
			}

			if (node[i].lightIntenseAlarmFlag)
			{
				USART_print("LIGHTINTENSE ALARM!! \n");
				node[i].lightIntenseAlarmFlag = FALSE;
			}

			if (node[i].temperaturAlarmFlag)
			{
				USART_print("TEMP1 ALARM!! \n");
				node[i].temperaturAlarmFlag = FALSE;
			}

			if (node[i].temperatur2AlarmFlag)
			{
				USART_print("TEMP2 ALARM!! \n");
				node[i].temperatur2AlarmFlag = FALSE;
			}
			break;
		}
	}
}


uint8_t handyCommunication(char *string, const char *define)
{
	uint8_t success = FALSE;

	if (sendingQuery == FALSE)
	{
		// send the information to the UART
		sende_befehl (define);
		sendingQuery = TRUE;
	}
	else
	{
		sendingStopCounter++;
	}

	if (sendingQuery == TRUE)
	{
		// read the information from the UART
		success = lese_befehl(string);
		//USART_print(string);
		// Check if string was OK
		if (strncmp (string, "OK", 2) == 0)
		{
			success = TRUE;

		}
		else
		{
			success = FALSE;
		}
	}

	if ( sendingStopCounter >= 5 )
	{
		sendingQuery = FALSE;
		success = FALSE;
		sendingStopCounter = 0;
	}
	memset(string, 0, 160);
	return success;
}

uint8_t initialisizeHandyConnection (void)
{
    char *puffer_ptr;
    char puffer[160];
    puffer_ptr = puffer;

	char *puffer2Ptr;
	char puffer2[160];
	puffer2Ptr = puffer2;

	uint8_t handyCheck = FALSE;
	uint8_t success = FALSE;

	uint8_t state = 1;

	while(handyCheck == FALSE)
	{
		switch (state)
		{
			case 1:
			{
				if (TimeTick == TRUE)
				{
					success = handyCommunication(puffer_ptr, disable_local_echo);
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 2:
			{
				if (TimeTick == TRUE)
				{
					success = handyCommunication(puffer_ptr, check_handy_con);
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 3:
			{
				if ( TimeTick == TRUE)
				{
					success = checkSCIDNumber(puffer_ptr,puffer2Ptr );
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 4:
			{
				if ( TimeTick == TRUE)
				{
					success = handyCommunication(puffer_ptr, info_sms_inc);
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 5:
			{
				if ( TimeTick == TRUE)
				{
					success = handyCommunication(puffer_ptr, check_handy_con);
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 6:
			{
				if ( TimeTick == TRUE)
				{
					success = handyCommunication(puffer_ptr, enable_clip);
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 7:
			{
				if ( TimeTick == TRUE)
				{
					success = handyCommunication(puffer_ptr,disable_ringtone);
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 8:
			{
				if ( TimeTick == TRUE)
				{
					success = handyCommunication(puffer_ptr,disable_vibration);
					TimeTick = FALSE;
					if (success == TRUE )
					{
						success = FALSE;
						state++;
					}
				}
				break;
			}

			case 9:
			{
				if ( TimeTick == TRUE)
				{
					success = checkBatteryStatus(puffer_ptr, puffer2Ptr);
					TimeTick = FALSE;
					if (success == TRUE)
					{
						handyCheck = TRUE;
						//success = FALSE;
					}
				}
				break;
			}
		}
	}
	return success;
}

uint8_t workWithHandy(void)
{
	char *puffer_neu_ptr;
	char *puffer_alt_ptr;

	char *sms_date_ptr;
	char *sms_text_ptr;
	char *sms_number_ptr;
	char *sms_center_ptr;
	char *sms_puffer_ptr;

	char sms_date[20];
	char sms_text[255];
	char sms_text_neu[255];
	char sms_number[20];
	char sms_center[20];

	char puffer_alt[160];
	char puffer_neu[160];

	sms_date_ptr = sms_date;
	sms_text_ptr = sms_text;
	sms_puffer_ptr = sms_text_neu;
	sms_number_ptr = sms_number;
	sms_center_ptr = sms_center;

	puffer_neu_ptr = puffer_neu;
	puffer_alt_ptr = puffer_alt;

	uint8_t success = FALSE;


	success = checkSmsMessages(puffer_neu_ptr);


	return success;

	//puffer_ptr = lese_befehl(puffer_alt_ptr);

	//if (puffer_ptr[0] != '\0')
	//{
		/* !!!! FIXME !!!!
		* If we have a call we want to answer on that
		* if we have a sms we need to read this sms.
		*
		*/
		/*
		USART_print ("puffer_ptr: \n");

		if (strncmp(puffer_ptr, "+CMTI:", 5) == 0)
		{
			USART_print("SMS incoming %s \n");
			sende_befehl(read_sms_new);
			puffer_ptr = lese_befehl(puffer_alt_ptr);

			puffer_ptr = read_pdu_str(0,8,puffer_ptr, puffer_ptr);
			USART_print("SMS PDU_TEXT %s \n");

			func_read_sms_date(puffer_ptr, sms_date_ptr);
			USART_print("SMS_Date %s \n");

			func_read_sms_number(puffer_ptr, sms_number_ptr);
			USART_print("SMS_Number %s \n");

			func_read_sms_text(puffer_ptr, sms_text_ptr);
			USART_print("SMS_Text %s \n");

			hexAscii2Int(sms_text_ptr, sms_puffer_ptr);

			twoint2one(sms_puffer_ptr, sms_text_ptr);

			pdu2ascii(sms_text_ptr, sms_puffer_ptr);
			USART_print("SMS_ASCII_Text: %s \n");

			sende_befehl (delete_sms_ind);
			puffer_ptr = lese_befehl(puffer_alt_ptr);
			if (strcmp (puffer_ptr, "ERROR") == 0)
			{
				puffer_ptr = lese_befehl(puffer_alt_ptr);
			}
			else
			{
				USART_print("delete sms status: %s\n");
				// TODO save into DB, over the first UART
			}

			if (strncmp(sms_puffer_ptr, "set secure", 10)==0)
			{
				USART_print("set secure information \n");
				sende_befehl (handy_off);
				puffer_ptr = lese_befehl(puffer_alt_ptr);
				if (strcmp (puffer_ptr, "ERROR") == 0)
				{
					puffer_ptr = lese_befehl(puffer_alt_ptr);
				}
				else
				{
					USART_print("set secure information will be setd \n");

					// !!!! FIXME !!!!
					// now we want to set the secure information,
					// it is possible to make this
					// into a function.
				}
			}

			if (strncmp(sms_puffer_ptr, "secure", 10)==0)
			{
				USART_print("secure information \n");
				sende_befehl (handy_off);
				puffer_ptr = lese_befehl(puffer_alt_ptr);
				if (strcmp (puffer_ptr, "ERROR") == 0)
				{
					puffer_ptr = lese_befehl(puffer_alt_ptr);
				}
				else
				{
					USART_print("secure information will be send \n");

					// !!!! FIXME !!!!
					// now we want to send the secure information,
					// it is possible to make this
					// into a function.
				}
			}
			if (strncmp(sms_puffer_ptr, "clima", 5)==0)
			{
				USART_print("klima information \n");
				sende_befehl (handy_off);
				puffer_ptr = lese_befehl(puffer_alt_ptr);
				if (strcmp (puffer_ptr, "ERROR") == 0)
				{
					puffer_ptr = lese_befehl(puffer_alt_ptr);
				}
				else
				{
					USART_print("climate information will be send \n");

					// !!!! FIXME !!!!
					// now we want to send the klima information,
					// it is possible to make this
					// into a function.
				}
			}

			if (strncmp(sms_puffer_ptr, "handy off", 10)==0)
			{
				USART_print("handy will be off \n");
				sende_befehl (handy_off);
				puffer_ptr = lese_befehl(puffer_alt_ptr);
				if (strcmp (puffer_ptr, "ERROR") == 0)
				{
					puffer_ptr = lese_befehl(puffer_alt_ptr);
				}
				else
				{
					USART_print("handy is now off \n");
					// !!!! FIXME !!!!
					// now we must restart the handy,
					// it is possible to make this
					// into a function.
				}
			}
		}

		if ( strncmp(puffer_ptr, "RING+CLIP:", 10) == 0)
		{
			USART_print("call incoming: \n");
			USART_print(puffer_ptr);
			USART_print("\n");
			// make a hangup if the number is not correct?
			// lets ring 3 times if number is correct and than hang up?
		}


		puffer_ptr[0] = '\0';
	}
	puffer_ptr[0] = '\0';
	sms_puffer_ptr[0] = '\0';
	sms_text_ptr[0] = '\0';
	*/
}

