/*
 * CMHandy.h
 *
 * Created: 19.04.2015 11:01:39
 *  Author: PC
 */


#ifndef CMHANDY_H_
#define CMHANDY_H_

//-----------------------------------------------------------------------------
#define BAUDRATE        19200
#define SCID_NUMBER     "00000000000000000000"
#define SIMCARDPIN      "at+CPIN=0000" // SIM CARD PIN

#define hsDisableLocalEcho	1

// TODO: all doings are endet with "\r\n"
#define check_handy_con		"at"			// ok
#define start_modem			"at???"			// we need the step
#define reset_modem			"atz0"			// OK -> after this do initialisize
#define enable_clip			"at+clip=1"		// ok
#define disable_local_echo	"ate0"			// ok
#define disable_vibration	"at+cvib=0"		// ok
#define battery_status		"at+cbc"		// +CBC: 0,100
#define read_manufacter		"at+cgmi"		// SIEMENS
#define read_sms_new		"at+cmgl=0"		// new message or ok
#define readAllSmsMessages	"at+cmgl=4"		// read all messages
#define read_funk_quality	"at+csqn"		// +CSQ: 19,99
#define info_sms_inc		"at+cnmi=1,1"	// ok # +CMTI: "SM",x
#define delete_sms_ind		"at+cmgd=1"		// ok
#define disable_ringtone	"at^SRTC=0"		// ok
#define read_scid			"at^scid"		// ^SCID: 000000000000000000000000
#define handy_off			"AT^SMSO"		// ^SMSO: MS OFF
#define pin_input			"at+CPIN=1568"  // Sim PIN

// now we read and save sms message
#define save_sms_string		"at+cmgw=22"
#define send_sms_string		"at+cmgs=22"
#define read_sms_string		"at+cmgl=0"		//new message or ok

void rev_string(char *string, char *new_string);
uint8_t startingHandyConnection (void);
void readAlarmManagement(unsigned char *nodeaddress);
uint8_t sende_befehl ( const char *string );
uint8_t lese_befehl ( char *string );
uint8_t handyCommunication(char *string, const char *define);

#endif /* CMHANDY_H_ */