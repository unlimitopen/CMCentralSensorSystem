﻿/*
 * CMXteaCrypt.c
 *
 * Created: 07.01.2017 16:43:20
 *  Author: Christian Möllers
 * This crypt or decrypt can be read by : https://de.wikipedia.org/wiki/Extended_Tiny_Encryption_Algorithm
 * 
 * This is a alpha version for xtea crypt and decrypt. 
 * Some operators must be checked out before we can use.
 * In this case you see, i have cast the operations to uint32_t that is needed by 8 bit controllers
 * because, the value size for uint32t in the compiler definition is 16 bit big.
 *
 * It is VERY important to remember that there is no such thing as
 * Simply using this cypher does not guarantee security.
 * Security is always a end-to-end concern and not only a part of encrypted transmission.
 * Please make sure that the end-to-end concern are respected!
 *
 * this crypt and decrypt version is not very good implemented. 
 * Please look at: https://de.wikipedia.org/wiki/Extended_Tiny_Encryption_Algorithm
 * 
 // Future Requests:
 //  
 //
 //
 // Bugs:
 // DONE: On a 8 bit Controller, this function dosen´t work, correct the shift value with a cast implemented
 //
 // Milestones:
 // Make a double crc value witch the encryption.
 // With the crc we have a really good chance to stop side effect: 
 //
 */ 

#include <avr/io.h>
//#include <stdint.h>
#include "../CMGlobal.h"
#include "CMXteaCrypt.h"
#include "../CMAdcMessure/CMAdcMessure.h"
					 
const uint32_t delta = 0x00000000;
static const uint8_t numCycles = 64;
static const uint32_t key[4] = {
								0b00000000,
								0b00000000,
								0b00000000,
								0b00000000,
  							   };
								 
struct xteaKeyTable 
{
	uint32_t delta;
	uint8_t numCycles;
	uint32_t key[4];

}keyTable[MAX];
								 
/**
@brief	define the numCycles
@param 	none
@return	none
**/
uint8_t setKeys(void)
{
	// INSERTING: define a new key.

	return 0;

}

/**
@brief	define the numCycles
@param 	none
@return	none
**/
void setNumCycles(void)
{
	// INSERTING: make change for the num cycles.

}

/**
@brief	define the delta key
@param 	none
@return	none
**/
uint32_t setDeltaKey(uint32_t delta)
{
	uint32_t newDelta = 0;
	uint8_t i = 0;
	
	static uint32_t delta1 = 0;
	static uint32_t delta2 = 0;
	static uint8_t sit = 0;
	static uint8_t init = 0;
	
	if (init == FALSE)
	{
		srand((uint32_t) getRandomNumber(6));
		init = TRUE;
	}
	
	while (i<5)
	{
		newDelta = ((uint32_t) (rand() % 9999999999 )<< 24);
		newDelta |= ((uint32_t) (rand() % 9999999999 )<< 16);
		newDelta |= ((uint32_t) (rand() % 9999999999 )<< 8);
		newDelta |= ((uint32_t) rand() );
		
		if ( (newDelta > 0x3B9ACA00) )
		{
			if (delta2 != newDelta)
			{
				if(sit == FALSE && newDelta < delta )
				{
					delta2 = newDelta;
					break;
				}
			}
			
			if (delta1 != newDelta)
			{
				if(sit == TRUE && newDelta > delta)
				{
					delta1 = newDelta;
					break;
				}
			}
		}
		else
		{
			i = 0;
		}
	}
	
	sit ^= TRUE;
	
	// insert the delta key into struct or into array??
	
	return newDelta;
}

/**
@brief	xtea enccryption this is a function to encrypt your data
@param 	none
@return	none
**/
void xteaEncipher(uint8_t *srcData, uint8_t *dstData, uint8_t dataSize)
{
	uint8_t i = 0, b = 0;
	uint32_t v0 = 0, v1 = 0, sum = 0;
	
	while ( b <= dataSize )
	{
		v0 = ((uint32_t) *srcData++ << 24);
		v0 |= ((uint32_t) *srcData++ << 16);
		v0 |= ((uint32_t) *srcData++ << 8);
		v0 |= ((uint32_t) *srcData++);

		v1 = ((uint32_t) *srcData++ << 24);
		v1 |= ((uint32_t) *srcData++ << 16);
		v1 |= ((uint32_t) *srcData++ << 8);
		v1 |= ((uint32_t) *srcData++);

		for (i=0; i < numCycles; i++)
		{
			v0 +=  (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + key[sum & 3]);
			sum += delta;
			v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + key[(sum>>11) & 3]);
		}

		v0 += v1;
		v1 += v0;
		
		*dstData++ = (v0 >> 24) & 0xFF;
		*dstData++ = (v0 >> 16) & 0xFF;
		*dstData++ = (v0 >> 8) & 0xFF;
		*dstData++ = v0 & 0xFF;

		*dstData++ = (v1 >> 24) & 0xFF;
		*dstData++ = (v1 >> 16) & 0xFF;
		*dstData++ = (v1 >> 8) & 0xFF;
		*dstData++ = v1 & 0xFF;

		v0 = 0;
		v1 = 0;
		sum = 0;
		b += 8;
	}
	*dstData = '\0';
}

/**
@brief	Xtea ´decryption, this is a function to decrypt your data
@param 	none
@return	none
**/
void xteaDecipher(uint8_t *srcData, uint8_t *dstData, uint8_t dataSize)
{
	uint8_t i = 0 , b = 0;
	uint32_t v0 = 0, v1 = 0, sum = 0;

	while ( b <= dataSize )
	{
		sum = delta * numCycles;
		
		v0 = ((uint32_t) *srcData++ << 24);
		v0 |= ((uint32_t) *srcData++ << 16);
		v0 |= ((uint32_t) *srcData++ << 8);
		v0 |= ((uint32_t) *srcData++);

		v1 = ((uint32_t) *srcData++ << 24);
		v1 |= ((uint32_t) *srcData++ << 16);
		v1 |= ((uint32_t) *srcData++ << 8);
		v1 |= ((uint32_t) *srcData++);
		
		v1 -= v0;
		v0 -= v1;
				
		for (i=0; i < numCycles; i++)
		{
			v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + key[(sum>>11) & 3]);
			sum -= delta;
			v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + key[sum & 3]);
		}

		*dstData++ = (v0 >> 24) & 0xFF;
		*dstData++ = (v0 >> 16) & 0xFF;
		*dstData++ = (v0 >> 8) & 0xFF;
		*dstData++ = v0 & 0xFF;

		*dstData++ = (v1 >> 24) & 0xFF;
		*dstData++ = (v1 >> 16) & 0xFF;
		*dstData++ = (v1 >> 8) & 0xFF;
		*dstData++ = v1 & 0xFF;

		v0 = 0;
		v1 = 0;
		b += 8;
	}
	*dstData = '\0';
}

