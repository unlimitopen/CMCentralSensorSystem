﻿/*
 * CMXteaCrypt.h
 *
 * Created: 07.01.2017 16:43:41
 *  Author: Christian Möllers
 */ 


#ifndef CMXTEACRYPT_H_
#define CMXTEACRYPT_H_

#define Decipher	0
#define Encipher	1

void xteaDeEncFunction( uint8_t *srcData, uint8_t *dstData, uint8_t number, uint8_t deOrEn);
void xteaDecipher (uint8_t *srcData, uint8_t *dstData, uint8_t dataSize);
void xteaEncipher(uint8_t *srcData, uint8_t *dstData, uint8_t dataSize);
uint32_t setDeltaKey(uint32_t delta);

#endif /* CMXTEACRYPT_H_ */