/*
 * CMEEprom.h
 *
 * Created: 18.10.2015 12:32:13
 *  Author: PC
 */ 

#ifndef CMEEPROM_H_
#define CMEEPROM_H_

#ifndef TRUE
#define TRUE		1
#endif

#ifndef FALSE
#define FALSE		0
#endif

#define CounterDef	11

uint8_t readEEprom(uint8_t *value);
uint8_t updateEEpromByte(uint8_t *pointer, uint8_t value);
uint8_t updateEEpromWord(uint16_t *pointer, uint16_t value);
uint8_t updateEEpromWord(uint16_t *pointer, uint16_t value);

#endif /* CMEEPROM_H_ */