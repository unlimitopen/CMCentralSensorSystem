﻿/*
 * CMML8511.c
 *
 * Created: 05.02.2017 14:10:28
 *  Author: Christian Möllers
 */ 

#include <avr/io.h>
#include "CMML8511.h"
#include "../CMAdcMessure/CMAdcMessure.h"
#include "../CMGlobal.h"



float getML8511UVSensor(void)
{
	//Hardware pin definitions
	int uvLevel = adcMessure(ML8511Sensor);
	int refLevel = adcMessure(ML8511SensorRef);

	//Use the 3.3V power pin as a reference to get a very accurate output value from sensor
	float outputVoltage = 3.3 / refLevel * uvLevel;

	//float uvIntensity = mapfloat(outputVoltage, 0.99, 2.8, 0.0, 15.0); //Convert the voltage to a UV intensity level
	
	return outputVoltage;
}

	