﻿/*
 * CMML8511.h
 *
 * Created: 05.02.2017 14:10:45
 *  Author: Christian Möllers
 */ 

#ifndef CMML8511_H_
#define CMML8511_H_

#define ML8511Sensor				5
#define ML8511SensorPin				PA5
#define ML8511SensorPinRegister		DDRA
#define ML8511SensorPinPort			PORTA
#define ML8511SensorPinIORegister	PINA
#define ML8511SensorPinOutput		ML8511SensorPinRegister |= (1<<ML8511SensorPin)
#define ML8511SensorPinInput		ML8511SensorPinRegister &=~ (1<<ML8511SensorPin)
#define ML8511SensorPinHigh			ML8511SensorPinPort |= (1<<ML8511SensorPin)
#define ML8511SensorPinLow		    ML8511SensorPinPort &=~ (1<<ML8511SensorPin)
#define ML8511SensorPinToggle		ML8511SensorPinPort ^= (1<<ML8511SensorPin)
#define ML8511SensorPinPressed		(ML8511SensorPinIORegister&(1<<ML8511SensorPin))

#define ML8511SensorRef				5
#define ML8511SensorRefPin				PA5
#define ML8511SensorRefPinRegister		DDRA
#define ML8511SensorRefPinPort			PORTA
#define ML8511SensorRefPinIORegister	PINA
#define ML8511SensorRefPinOutput		ML8511SensorRefPinRegister |= (1<<ML8511SensorRefPin)
#define ML8511SensorRefPinInput			ML8511SensorRefPinRegister &=~ (1<<ML8511SensorRefPin)
#define ML8511SensorRefPinHigh			ML8511SensorRefPinPort |= (1<<ML8511SensorRefPin)
#define ML8511SensorRefPinLow		    ML8511SensorRefPinPort &=~ (1<<ML8511SensorRefPin)
#define ML8511SensorRefPinToggle		ML8511SensorRefPinPort ^= (1<<ML8511SensorRefPin)
#define ML8511SensorRefPinPressed		(ML8511SensorRefPinIORegister&(1<<ML8511SensorRefPin))

float getML8511UVSensor(void);

#endif /* CMML8511_H_ */