/*
 * dcf.c
 *
 * Created: 27.12.2014 11:23:23
 *  Author: c.moellers
 */ 

#ifndef CMDCF_H_
#define CMDCF_H_

#define TRUE	1
#define FALSE	0

// if DCF modules - signals are inverted
#define INVERT	0

// define the port for activating
#define dcfReadDataRegister			PORTC
#define dcfReadDirectionRegister	DDRC
#define dcfReadInputRegister		PINC
#define dcfReadInputPin				PC7
#define dcfReadingSignal			(dcfReadInputRegister&(1<<dcfReadInputPin))

// activate and deactivate dcf module
#define dcfPortOn			PORTC
#define dcfRegisterOn		DDRC
#define dcfPinOn			PC6


void dcfInit(uint8_t status);
uint8_t dcfReadSignal();
void dcfConvert();
void getDatum (char *clockData);
uint8_t dcfSyncing(void);

#endif /* CMDCF_H_ */
