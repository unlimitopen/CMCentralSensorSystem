/*
 * dcf.c
 *
 * Created: 27.12.2014 11:23:23
 *  Author: c.moellers
 */

#include <avr/io.h>
#include <stdlib.h>
#include "CMDcf.h"
#include "../CMUart/CMUart0.h"
#include "../CMNeuItoa/CMNeuItoa.h"
#include "../CMGlobal.h"
#include <util/delay.h>
#include <string.h>

// DCF Frames
volatile uint8_t dcfFrame[60];

// DCF Signal
uint8_t dcfSignal			= FALSE;

// define the varialbles
volatile uint8_t sec		= 0;
volatile uint8_t hour		= 0;
volatile uint8_t min		= 0;
volatile uint8_t day		= 4;
volatile uint8_t weekday	= 1;
volatile uint8_t month		= 6;
volatile uint8_t year		= 15;
volatile uint8_t date		= 0;

volatile uint8_t flagCounter = FALSE;

volatile uint8_t tick		= FALSE;
volatile uint8_t lowTick	= FALSE;
volatile uint8_t dcfConvertFlag = FALSE;
volatile uint8_t low		= 0;
volatile uint8_t high		= 0;


/**
	@brief	activate DCF read pin and activate and deactivate DCF module
	@param 	none
	@return	none
*/
void dcfInit(uint8_t status)
{
	if (status == TRUE)
	{
		// activate the dcf module
		dcfRegisterOn |= (1<<dcfPinOn);
		dcfPortOn &=~ (1<<dcfPinOn);
		// Setup the pin definitions for dcf module
		//dcfReadPort &=~ (1<<dcfReadPin);
		dcfReadDataRegister &=~ (1<<dcfReadInputPin);
	}
	else
	{
		// deactivate the read pin
		dcfReadDataRegister |= (1<<dcfReadInputPin);
		dcfReadDirectionRegister &=~ (1<<dcfReadInputPin);
		// deactivate the dcf module
		dcfRegisterOn &=~ (1<<dcfPinOn);
		dcfPortOn |= (1<<dcfPinOn);
	}
}


/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t dcfReadSignal(void)
{
	uint8_t dcfCounterRef		= 2;
	uint8_t dcfCounter			= 0;
	uint8_t dcfCounterFalse		= 0;
	uint8_t dcfCounterFalseRef	= 2;
	uint8_t dcfGeneralCounter	= 0;
	uint8_t dcfGeneralCounterRef = 70;
	uint8_t i					= 0;
	uint8_t beginSecond			= FALSE;
	uint8_t success				= FALSE;

	// DCF Signal
	// a zero is 100ms long, per one second
	// a one is 200ms  long, per one second
	while (dcfCounter <= dcfCounterRef)
	{
		if (tick == FALSE && beginSecond == TRUE)
		{
			if (high >= 10 && high <= 14 )
			{
				dcfFrame[i] = 0;
				high = 0;
				i++;
			}

			if ( high >= 18 && high <= 24 )
			{
				dcfFrame[i] = 1;
				high = 0;
				i++;
			}
		}

		if (lowTick == FALSE)
		{
			if ( low >= 175 )
			{
				if ( ( i <= 38) && (beginSecond == TRUE) )
				{
					i = 0;
					dcfCounter = 0;
					beginSecond = FALSE;
					dcfCounterFalse++;
				}
				else
				{
					i=0;
					high = 0;
					beginSecond = TRUE;
					dcfCounter++;
					success = TRUE;
				}

			}
			//dcfGeneralCounter++;
			low = 0;

			// now we make a brake, if we have problem with
			// the dcf signal here!!
			if ( dcfCounterFalse >= dcfCounterFalseRef )
			{
				dcfCounter = 10;
				success = FALSE;
			}

			if ( dcfGeneralCounter >= dcfGeneralCounterRef)
			{
				success = FALSE;
				dcfCounter = 10;
			}
		}
	}
	return success;
}


/**
	@brief	none
	@param 	none
	@return	none
*/
void dcfConvert()
{
	// TODO: Make logical checks, because the date and time is not every time correct

	// Setup the summer or wintertime
	// Frame: if the frame 20 is 1 than we can calculate
	if (dcfFrame[20] == TRUE )
	{
		// minute:
		min = dcfFrame[21] + (dcfFrame[22] * 2) + (dcfFrame[23] * 4) + (dcfFrame[24] * 8 ) + (dcfFrame[25] * 10) + (dcfFrame[26] * 20) + (dcfFrame[27] * 40);

		// hour :
		hour = dcfFrame[29] + (dcfFrame[30] * 2) + (dcfFrame[31] * 4) + (dcfFrame[32] * 8) + (dcfFrame[33] * 10) + (dcfFrame[34] * 20);

		// day :
		day = dcfFrame[36] + (dcfFrame[37] * 2) + (dcfFrame[38] * 4) + (dcfFrame[39] * 8) + (dcfFrame[40] * 10) + (dcfFrame[41] * 20);

		// weekday :
		weekday = dcfFrame[42] + (dcfFrame[43] * 2) + (dcfFrame[44] * 4);

		// month
		month = dcfFrame[45] + (dcfFrame[46] * 2) + (dcfFrame[47] * 4) + (dcfFrame[48] * 8) + (dcfFrame[49] * 10);

		// year :
		year = dcfFrame[50] + (dcfFrame[51] * 2) + (dcfFrame[52] * 4) + (dcfFrame[53] * 8) + (dcfFrame[54] * 10) + (dcfFrame[55] * 20)  + (dcfFrame[56] * 40) + (dcfFrame[57] * 80);
		// TODO what can we do?
		//year += 2000

		// TODO: make a parity check!
		// INFO: parity check are 0
	}
}


uint8_t dcfModulExists(void)
{
	uint8_t success = FALSE;
	uint8_t counter = 0;
	
	while(counter < 50 && success == FALSE)
	{
		if (dcfReadingSignal)
		{
			success = TRUE;
			counter = 55;
		}	
		counter++;
	}
	
	if (counter > 50)
	{
		success = FALSE;
	}
	
	return success;
}


/**
	@brief	DCF Syncing for Script
	@param 	none
	@return	none
*/
uint8_t dcfSyncing(void)
{
	uint8_t returnCode = FALSE;
	uint8_t success = FALSE;
	dcfInit(TRUE);
	
	if (dcfModulExists())
	{
		returnCode = dcfReadSignal();

		if ( returnCode == TRUE )
		{
			dcfConvert();
			success = TRUE;
		}
		else
		{
			dcfInit(FALSE);
			success = FALSE;
		}	
	}
	else
	{
		dcfInit(FALSE);
		success = FALSE;
	}
	
	return success;
}

/**
	@brief	none
	@param 	none
	@return	Datum as a string
*/
void getDatum (char *clockData)
{
	unsigned char secChar[3];
	unsigned char minChar[3];
	unsigned char hourChar[3];
	unsigned char yearChar[3];
	unsigned char monthChar[3];
	unsigned char dayChar[3];
	unsigned char weekdayChar[3];

	neuitoa(sec, secChar);
	neuitoa(min, minChar);
	neuitoa(hour, hourChar);
	neuitoa(day, dayChar);
	neuitoa(weekday, weekdayChar);
	neuitoa(year, yearChar);
	neuitoa(month, monthChar);

	strncat(clockData, yearChar,3);
	strncat(clockData, "-", 1);
	strncat(clockData, monthChar,3);
	strncat(clockData, "-", 1);
	strncat(clockData, dayChar,3);
	strncat(clockData, ";", 1);
	strncat(clockData, hourChar,3);
	strncat(clockData, ":", 1);
	strncat(clockData, minChar,3);
	strncat(clockData, ":", 1);
	strncat(clockData, secChar,3);
	strncat(clockData, ";", 1);
}
