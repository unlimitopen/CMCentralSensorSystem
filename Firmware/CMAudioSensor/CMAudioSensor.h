﻿/*
 * CMAudioSensor.h
 *
 * Created: 01.04.2017 13:20:57
 *  Author: Christian Möllers
 */ 


#ifndef CMAUDIOSENSOR_H_
#define CMAUDIOSENSOR_H_

#define audioSensorInputPin			PA7
#define audioSensorInputRegister	DDRA
#define audioSensorInputPort		PORTA
#define audioSensorInputIORegister	PINA
#define audioSensorInputOutput		audioSensorInputRegister |= (1<<audioSensorInputPin)
#define audioSensorInputInput		audioSensorInputRegister &=~ (1<<audioSensorInputPin)
#define audioSensorInputHigh		audioSensorInputPort |= (1<<audioSensorInputPin)
#define audioSensorInputLow		    audioSensorInputPort &=~ (1<<audioSensorInputPin)
#define audioSensorInputToggle		audioSensorInputPort ^= (1<<audioSensorInputPin)
#define audioSensorInputPressed		(audioSensorInputIORegister&(1<<audioSensorInputPin))

#define audioSensorActivePin				PC3
#define audioSensorActivePinRegister		DDRC
#define audioSensorActivePinPort			PORTC
#define audioSensorActivePinIORegister	PINC
#define audioSensorActivePinOutput		audioSensorActivePinRegister |= (1<<audioSensorActivePin)
#define audioSensorActivePinInput		audioSensorActivePinRegister &=~ (1<<audioSensorActivePin)
#define audioSensorActivePinHigh		audioSensorActivePinPort |= (1<<audioSensorActivePin)
#define audioSensorActivePinLow		    audioSensorActivePinPort &=~ (1<<audioSensorActivePin)
#define audioSensorActivePinToggle		audioSensorActivePinPort ^= (1<<audioSensorActivePin)
#define audioSensorActivePinPressed		(audioSensorActivePinIORegister&(1<<audioSensorActivePin))

uint16_t getAudioNoise(uint8_t channel);
void audioSensorDirection(uint8_t direction);

#endif /* CMAUDIOSENSOR_H_ */