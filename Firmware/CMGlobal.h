﻿/*
 * CMGlobal.h
 *
 * Created: 13.08.2016 10:06:32
 *  Author: Christian Möllers
 */ 


#ifndef CMGLOBAL_H_
#define CMGLOBAL_H_

#include <avr/io.h>

//#define F_CPU 14475000UL
//#define F_CPU   12000000UL
//#define F_CPU   12000000UL
//#define F_CPU  7372800
//#define F_CPU    8000000
#define F_CPU 14745600UL
//#define F_CPU   20000000UL





#include <util/delay.h>
#include <stdlib.h>
#include <string.h>

// Selfmade programs
//#include "CMXteaCrypt/CMXteaCrypt.h"
//#include "CMRfm12/RFM12.h"
//#include "CMSpi/SPI.h"
//#include "CMTwi/CMTwi.h"
//#include "CMAdcMessure/CMAdcMessure.h"
//#include "CMUart/CMUart0.h"				// Uart Connection
//#include "CMPduConvert/pduConvert.h"     // Conversion for ASCII to PDU
//#include "CMAsciiTools/bitDump.h"        // To see in Binary what happend
//#include "CMAsciiTools/asciiHex2int.h"   // Conversion from HEX ARRAY to Char
//#include "CMRsAddresses/CMRsAddresses.h" // defined the communication numbers
//#include "CMMasterNode/CMMasterNode.h"
//#include "CMNodeDataHandle/CMNodeDataHandle.h" // Data handle for NodeSensor Network
//#include "CMEEprom/CMEEprom.h"
//#include "CMDcf/CMDcf.h"
//#include "CMNeuItoa/CMNeuItoa.h"
//#include "CMLm75/CMLm75.h"
//#include "CMDhtSensor/CMDhtSensor.h"
////#include "CMOneWire.h"
//#include "CMBMP180/CMBMP180.h"
//#include "CMMCP9808/CMMCP9808.h"
//#include "CMHandy/CMHandy.h"
//#include "CMLux/CMLux.h"
//#include "CMDS1307/CMDS1307.h"
//#include "CMPirSensor/CMPirSensor.h"
//#include "CMAudioSensor/CMAudioSensor.h"
//#include "CMNodeDefault/CMNodeDefaults.h"

#define printDebugging		0

#define TRUE		1
#define FALSE		0

#define RXMode		1
#define TXMode		0

#define RXIntMode		2
#define TXIntMode		3

#ifndef DataSize
#define DataSize 		200
#endif

#ifndef MAX
#define MAX		 	10
#endif

#define interrupt0			PD2
#define interrupt0Output	DDRD |= (1<<interrupt0)
#define interrupt0Input		DDRD &=~ (1<<interrupt0)
#define interrupt0High		PORTD |= (1<<interrupt0)
#define interrupt0Low		PORTD &=~ (1<<interrupt0)
#define interrupt0Toggle	PORTD ^= (1<<interrupt0)

#define interrupt1			PD3
#define interrupt1Output	DDRD |= (1<<interrupt1)
#define interrupt1Input		DDRD &=~ (1<<interrupt1)
#define interrupt1High		PORTD |= (1<<interrupt1)
#define interrupt1Low		PORTD &=~ (1<<interrupt1)
#define interrupt1Toggle	PORTD ^= (1<<interrupt1)

#define RedLED				PD7
#define RedLEDOutput		DDRD |= (1<<RedLED)
#define RedLEDInput			DDRD &=~ (1<<RedLED)
#define RedLEDHigh			PORTD |= (1<<RedLED)
#define RedLEDLow			PORTD &=~ (1<<RedLED)
#define RedLEDToggle		PORTD ^= (1<<RedLED)

#define GreenLED			PD6
#define GreenLEDOutput		DDRD |= (1<<GreenLED)
#define GreenLEDInput		DDRD &=~ (1<<GreenLED)
#define GreenLEDHigh		PORTD |= (1<<GreenLED)
#define GreenLEDLow			PORTD &=~ (1<<GreenLED)
#define GreenLEDToggle		PORTD ^= (1<<GreenLED)


#endif /* CMGLOBAL_H_ */