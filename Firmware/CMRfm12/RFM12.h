/*
 * IncFile1.h
 *
 * Created: 19.02.2015 00:11:58
 *  Author: c.moellers
 */ 

#ifndef RFM12_H_
#define RFM12_H_

#define RFM12Crypt				1
#define RFM12CryptDebug			0

#define RXMode					1
#define TXMode					0
#define RXIntMode				2
#define TXIntMode				3

#define TRUE					1
#define FALSE					0

#define rfm12PORT				PORTB
#define rfm12DDR				DDRB
#define rfm12PIN				PINB

#define rfm12CSPin				PB4
#define rfm12CSPinOutput		DDRB |= (1<<rfm12CSPin)
#define rfm12CSPinInput			DDRB &=~  (1<<rfm12CSPin)
#define rfm12CSPinHigh			PORTB |= (1<<rfm12CSPin)
#define rfm12CSPinLow			PORTB &=~ (1<<rfm12CSPin)
#define rfm12CSPinToggle		PORTB ^= (1<<rfm12CSPin)

#define rfm12NRESPin			PB3
#define rfm12NRESPinOutput		DDRB |= (1<<rfm12NRESPin)
#define rfm12NRESPinInput		DDRB &=~  (1<<rfm12NRESPin)
#define rfm12NRESPinHigh		PORTB |= (1<<rfm12NRESPin)
#define rfm12NRESPinLow			PORTB &=~ (1<<rfm12NRESPin)
#define rfm12NRESPinToggle		PORTB ^= (1<<rfm12NRESPin)

uint16_t rfm12WriteCmd(uint16_t data);
void rfm12Init();
void rfm12ChangeDirection(uint8_t direction);
void rfm12TransmitData(unsigned char *data, unsigned char number);
//void rfm12TransmitDataCrypt(unsigned char *data, unsigned char number);
void rfm12ReceiveData(unsigned char *data, uint8_t number);
//void rfm12ReceiveDataCrypt(unsigned char *data, unsigned char number);
void rfm12SetInterrupt(void);
void rfm12Wakeup(void);
void rfm12SetAfterInterrupt(void);
void rfm12CommunicationDirection(uint8_t direction);
void rfm12SetFIFOMode(void);
uint16_t rfm12Check(void);
void rfm12ReceiveISR(unsigned char *data, unsigned char number);
unsigned char rfRecv();
uint8_t rfm12PORCheckCheck(void);
int rfm12StatusCheck(void);
void rfm12ReceiveDataISR(unsigned char *data, unsigned char number);
unsigned short rf12_trans(unsigned short wert);

#endif /* RFM12_H_ */