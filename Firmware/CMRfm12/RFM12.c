/*
 * RFM12.c
 *
 * Created: 19.02.2015 00:11:30
 *  Author: PC
 */

#include <avr/io.h>
#include "RFM12.h"
#include "../CMSpi/SPI.h"
#include "../CMXteaCrypt/CMXteaCrypt.h"
#include "../CMGlobal.h"
#include <util/delay.h>

unsigned char helpVar[DataSize];
unsigned char *helpVarPtr = helpVar;
unsigned char *helpVarPtrStart  = helpVar;

uint16_t rfm12WriteCmd(uint16_t data);

/**
@brief	none
@param 	none
@return	none
**/
void rfm12Ready(void)
{
	//uint8_t secTmp = sec;
	rfm12CSPinLow;
	//TODO: We need a better timeout or interrupt
	while (!(rfm12PIN&(1<<MISO_PIN))); // wait until FIFO ready
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12Wakeup(void)
{
	rfm12WriteCmd(0x80D7);	//EL,EF,12.0pF
	rfm12WriteCmd(0x8208);	//!er,!ebb,!ET,ES,EX,!eb,!ew,!DC (in Bereitschaft gehen)
	rfm12StatusCheck();  // Statusregister auslesen;
	_delay_ms(5);
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12SetAfterInterrupt(void)
{
	rfm12WriteCmd(0x82C9);		// RX on
	//rfm12WriteCmd(0xCA81);	// set FIFO mode
	rfm12WriteCmd(0xCA81);	// set FIFO mode
	rfm12WriteCmd(0xCA83);	// set FIFO mode
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12ResetModule()
{
	rfm12NRESPinOutput;
	rfm12NRESPinLow;
	rfm12NRESPinHigh;
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12SetFIFOMode(void)
{
	rfm12WriteCmd(0xCA81);		// set FIFO mode
	rfm12WriteCmd(0xCAF3);		// enable FIFO
	//rfm12WriteCmd(0xCA83);	// enable FIFO
}

/**
@brief	none
@param 	none
@return	none
**/
//Exchange a word (two bytes, big-endian) with the module
uint16_t rfm12WriteCmd(uint16_t data)
{
	uint16_t received = 0;
	rfm12CSPinLow;
	spiTrasmitData((data >> 8) & 0xFF );
	received = SPDR;
	received <<= 8;	
	spiTrasmitData( 0xFF & data);
	received |= SPDR;
	rfm12CSPinHigh;
	return received;	
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12Init()
{
	// reset the module
	rfm12ResetModule();
	rfm12CSPinOutput;

	rfm12WriteCmd(0xFE00); //send the reset command

	for (unsigned char i=0; i<10; i++)
	{
		_delay_ms(15); // wait until POR done
	}
	rfm12WriteCmd(0x80E7);//EL,EF,868band,12.0pF
	//rfm12WriteCmd(0x8238);//!er !ebb !ET ES EX !eb !ew DC
	//rfm12WriteCmd(0x827D);
	rfm12WriteCmd(0x82C9);
	//rfm12WriteCmd(0x8219);//!er !ebb !ET ES EX !eb !ew !DC
	//rfm12WriteCmd(0x82D8); // !eb !ebb  Et Es Ex !eb !ew Dc
	//rfm12WriteCmd(0xA67C);//868MHz
	rfm12WriteCmd(0xA6F8);
	//rfm12WriteCmd(0xA5DC);
	//rfm12WriteCmd(0xC647);//4.8kbps
	rfm12WriteCmd(0xC611);//19.2kbps
	//rfm12WriteCmd(0x94A0);//VDI,FAST,134kHz,0dBm,-103dBm
	//rfm12WriteCmd(0x94A2);//VDI,FAST,134kHz,0dBm,-103dBm TODO!!!
	rfm12WriteCmd(0x948C);//VDI,FAST,134kHz,0dBm,-103dBm TODO!!!
	rfm12WriteCmd(0xC2AC);//AL,!ml,DIG,DQD4
	rfm12WriteCmd(0xC2AB);//AL,!ml,DIG,DQD4
	rfm12WriteCmd(0xCA81);//FIFO8,SYNC,!ff,DR
	rfm12WriteCmd(0xCED4);//SYNC=2DD4;
	//rfm12WriteCmd(0xC483);//@PWR,NO RSTRIC,!st,!fi,OE,EN
	//rfm12WriteCmd(0xC4F7);//@PWR,NO RSTRIC,!st,!fi,OE,EN TDOD!!!
	rfm12WriteCmd(0xC4A7);
	//rfm12WriteCmd(0x9850);//!mp,90kHz,MAX OUT
	rfm12WriteCmd(0x9870);//!mp,90kHz,MAX OUT
	rfm12WriteCmd(0xE000);//NOT USE
	rfm12WriteCmd(0xC800);//NOT USE
	//rfm12WriteCmd(0xC040);//1.66MHz,2.2V
	//rfm12WriteCmd(0xC060);//2 MHz , 2.2V
	//rfm12WriteCmd(0xC0C0);//5 MHz,2.2V
	rfm12WriteCmd(0xC0E0);//10 MHz,2.2V
	//rfm12WriteCmd(0x0000);
}

/**
@brief	none
@param 	none
@return	none
**/
int rfm12StatusCheck(void)
{
	return rfm12WriteCmd(0x0000);
}

/**
@brief	none
@param 	none
@return	none
**/
uint8_t rfm12PORCheckCheck(void)
{
	uint8_t success = FALSE;

	if (rfm12StatusCheck() & 0x4000)
	{
		success = TRUE;
	}
	return success;
}

/**
@brief	none
@param 	none
@return	none
**/
uint16_t rfm12Check(void)
{
	rfm12CSPinOutput;
	rfm12CSPinLow;
	return rfm12WriteCmd(0x0000);
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12TransmitData(unsigned char *data, unsigned char number)
{
	uint8_t i = 0;
	// TODO: Need correction
	//rfm12WriteCmd(0x0000);
	//rfm12Ready();
	
	//unsigned char helpVar[DataSize];
	//unsigned char *helpVarPtr = helpVar;
	
	helpVarPtr = helpVarPtrStart;
	
	#if (RFM12Crypt == TRUE)
		xteaEncipher(data, helpVar, (number/2) );
	#endif
	
	#if (RFM12CryptDebug == TRUE)
		printf(">>ENC: %i,%i,%i,%i,%i,%i,%i,%i,%i,%i \n", helpVar[0],helpVar[1],helpVar[2],helpVar[3],helpVar[4],helpVar[5],helpVar[6],helpVar[7],helpVar[8],helpVar[9]);
	#endif
	
	_delay_ms(50);
	rfm12WriteCmd(0x8239); // TX on
	rfm12Ready();
	rfm12WriteCmd(0xB8AA); // DUMMY Byte
	rfm12Ready();
	rfm12WriteCmd(0xB8AA); // DUMMY Byte
	rfm12Ready();
	rfm12WriteCmd(0xB8AA); // DUMMY Byte
	rfm12Ready();
	rfm12WriteCmd(0xB82D); // SyncronByte
	rfm12Ready();
	rfm12WriteCmd(0xB8D4); // SyncronByte
	rfm12Ready();
	for (i=0; i<number;i++)
	{
		rfm12Ready();
		rfm12WriteCmd(0xB800 | (*helpVarPtr++) );
	}
	rfm12Ready();
	rfm12WriteCmd(0xB8AA); // DUMMY Byte
	rfm12Ready();
	rfm12WriteCmd(0xB8AA); // DUMMY Byte
	rfm12Ready();
	rfm12WriteCmd(0xB209); // TX Off
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12ReceiveData(unsigned char *data, uint8_t number)
{
	uint8_t i = 0;
	//unsigned char helpVar[DataSize];
	//unsigned char *helpVarPtr = helpVar;
	
	helpVarPtr = helpVarPtrStart;
	
	rfm12WriteCmd(0x82C9);	// RX on
	rfm12WriteCmd(0xCA81);  // set FIFO mode
	rfm12WriteCmd(0xCA83);	// enable FIFO
	rfm12Ready();
	for (i=0; i<number; i++)
	{
		rfm12Ready();
		//*helpVarPtr++=rfm12WriteCmd(0xB000);
		//*helpVarPtr++=rfm12WriteCmd(0xB000) & 0x00FF;
		//*helpVarPtr++=(unsigned char) rfm12WriteCmd(0xB000) & 0x00FF;
		*helpVarPtr++=(unsigned char)rfm12WriteCmd(0xB000);
		rfm12Ready();
	}
	rfm12Ready();
	rfm12WriteCmd(0xCA81);		// set FIFO mode
	rfm12WriteCmd(0xCA83);		// enable FIFO
	
	#if (RFM12CryptDebug == TRUE)
		printf("<<ENC: %i,%i,%i,%i,%i,%i,%i,%i,%i,%i \n", helpVar[0],helpVar[1],helpVar[2],helpVar[3],helpVar[4],helpVar[5],helpVar[6],helpVar[7],helpVar[8],helpVar[9]);
	#endif

	#if (RFM12Crypt == TRUE)
		xteaDecipher(helpVar, data, (number/2) );
	#endif
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12ReceiveDataISR(unsigned char *data, unsigned char number)
{
	uint8_t i = 0;
	
	helpVarPtr = helpVarPtrStart;
	
	//rfm12WriteCmd(0x82C8);	// RX on
	//rfm12WriteCmd(0xCA81);  // set FIFO mode
	//rfm12WriteCmd(0xCA83);	// enable FIFO
	if (rfm12StatusCheck() & 0x8000)
	{
		for (i=0; i<number; i++)
		{
			rfm12Ready();
			//*helpVarPtr++=(unsigned char) (rfm12WriteCmd(0xB000) & 0x00FF);
			//*helpVarPtr++=rfm12WriteCmd(0xB000) & 0x00FF;
			*helpVarPtr++=rfm12WriteCmd(0xB000);
			//*helpVarPtr++=(unsigned char) (rfm12WriteCmd(0xB000) & 0x00FF);
			rfm12Ready();
		}
		rfm12Ready();
		rfm12WriteCmd(0xCA81);		// set FIFO mode
		rfm12WriteCmd(0xCA83);		// enable FIFO
		
		#if (RFM12CryptDebug == TRUE)
			printf("<<ENC: %i,%i,%i,%i,%i,%i,%i,%i,%i,%i \n", helpVar[0],helpVar[1],helpVar[2],helpVar[3],helpVar[4],helpVar[5],helpVar[6],helpVar[7],helpVar[8],helpVar[9]);
		#endif
		
		#if (RFM12Crypt == TRUE)
			xteaDecipher(helpVar, data, (number/2) );
		#endif
	}
}

/**
@brief	none
@param 	none
@return	none
**/
void rfm12CommunicationDirection(uint8_t direction)
{
	switch (direction)
	{
		case RXMode:
		{
			rfm12WriteCmd(0x82C9);		// RX on
			rfm12WriteCmd(0xCA81);
			//rfm12WriteCmd(0xCA83);	// enable FIFO
			//rfm12Ready();
			rfm12WriteCmd(0xCA23);	// enable FIFO
			rfm12WriteCmd(0x0000);
		}

		case TXMode:
		{
			//_delay_ms(50);
			rfm12WriteCmd(0x8239);		// TX on
		}

		case RXIntMode:
		{
			rfm12WriteCmd(0x82C9);	// RX on
			//rfm12WriteCmd(0xCA81);	//FIFO8,SYNC,!ff,DR
			rfm12WriteCmd(0xCA81);
			rfm12WriteCmd(0xCA83);	// enable FIFO
		}

		case TXIntMode:
		{

		}
	}
}
