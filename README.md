# CMCentralSensorSystem
Central Sensor System is a name that means we talk in this project about, SmartHome and HomeAutomation.
Now i think in this case we have a status to bring the Central Sensor System to gitlab, because we have stadium that works good for us, but really not perfectly!!

In this first step we have a lot work done and we have a lot of work infront of us, that is really frightening, because one of the thing is we are thinking about, we change the microcontroller, acutal it is a Atmel ATmega 1284 implemented, but with the lot of work infront of us we will thought about a another controller like STM32 microcontroller, with more power and more possibilities.
But it is not so, that we can change it with a "click", no!

At the beginning of this project i will document a many thing, i know that is many informations, i hope it will be understood.


----


#### Technical Informations
Follwing components are used
* Atmega 1284 Microcontroller
* EA DOG 160 XL Display
* HopeRF RFM12 Radio Modul
* DCF-77 Signal modul
* ~~Siemens S35 Handy communication~~ (deprecated)
* DS1307 RTC
* Serial Connection to a Raspberry PI Clone (BananaPI)

Follwing Sensors are used:
* MQ4
* MQ6
* LM75 Temperatur Sensor
* ML8511 Sensor
* HTUD21 Sensor
* BMP180 Sensor
* TEMT6000 Sensor
* DHT11 Sensor
* Audio with LM324 Sensor
* LDR07 Sensor
* DS18B20 Sensor
* MCP9808 Sensor
* PIR Sensor with BIS100008

Following Intel /  Raspberry PI (Clone) System are used:
* Telegram API Communication for Informations are used 
* SQLite3 or MySQL or PostgreSQL
* InfluxDB
* Grafana

----

#### How does it works
The CMCSS starts directly if they power up. They will initialisize the sensor on board and the components - rfm12, ds1307 and anything else.
In the initialisize section the CMCSS will communicate with the serial interface for the pc/arm system, the first initialisize command is the actual date and time to set this internal in the CMCSS. The serial communication is encrypted with XTEA-Crypt.
If all initialisize is ready, then the rfm12 will setup into the receiver status and the CMCSS will wait for a incoming interrupt from the rfm12, that signal means a sensor node will communicate with the CMCSS. The communication over the rfm12 modules are also encrypted with the XTEA-Crypt method. We know that is small encryption and not really heavy secure but it works and it is possible to bring it on a ATmega 328 controller. Please let me note the secure issue is the biggest thought why we want to use another controller like STM32. In this version 1007 we cannot support other smarthome / homeautomation tools like "HomeMatic, RWE Smarthome" or anything else.

![ This is a view of the architecture ](/doc/CentralSensorSystem.jpg)

#### Information about the sensors for CM CMCentralSensorSystem
Following node type in planning or also developed

| Node type | Node discription | Link / Information | developed / in planning |
| -------- | -------- | -------- | -------- |
| sensorNode | typical sensor node, with many sensors on board. | https://gitlab.com/unlimitopen/CMCSSNodes | developed |
| Alarmclock | A night table alarm clock, with many alarming times and possibility to a read a sd card for wav files. | | development has started yet |
| WindowLevelSensor | for window handle a sensor that informate about the opened window and man more. | | developed |
| CarAlarm | a car sensor to measure the situations in the car if they turned of and completed for the house door ;-). | | development has not started yet |
| ShutterControl | a window shutter control sensor | | development has not started yet |
| PowerStrip | a power control as a master slave power strip | | development has started yes |
| RFID-DoorAlarm | a sensor control that is used to tell the CMCSS who is in the house (:-)) | | developed |
| PlantwateringSensor | a sensor for your plant if you want to control the watering, also needed is the pumping system | | development has started |
| WaterpumpControl | a little water pump station for the plantwateringsensor, self-sufficient (solar powered) system | | development has started |


----

#### Version and History
| date | developer | version | answer / comment | actual version online |
| -------- | -------- | -------- | -------- | -------- |
| 10.01.2015 | c.möllers | 0001 | initial release |  |
| --.--.---- | c.möllers | ---- | --------------- |  |
| --.--.---- | c.möllers | ---- | --------------- |  |
| --.--.---- | c.möllers | ---- | --------------- |  |
| --.--.---- | c.möllers | ---- | --------------- |  |
| 27.12.2016 | c.möllers | 1007 | initial release | <<<<<<<<<  |

----

#### Actual Version
| Version | Build | developer |
| -------- | -------- | -------- |
| 1007 | 20161227 | c.möllers |
